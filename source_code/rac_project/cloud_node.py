import util
from math import sqrt
from edge_node import EdgeNode
from request import Request
from util import *

class CloudNode:
	DELAY_CLOUD_EN_DEFAULT = 3		#delay in seconds
	CPU_RATE = 3 * GB
	INST_DELAY 		= 0.100

	# Request state
	FAST_FULFILLED = "FAST_FULFILLED" # database has data and request time is in database
	SLOW_FULFILLED = "SLOW_FULFILLED" # request time is now
	NOT_FULFILLED = "NOT_FULFILLED"  # request time is not in database
	CLOUD_FULFILLED = "CLOUD_FULFILLED" # request was offloaded to database

	def __init__(self, env, interface):
		self.env = env
		self.interface = interface
		self.type = "CLOUD"
		self.neighbor_list = []
		self.psanList = []
		self.process = None
		self.allDtList = []
		self.edgeList = []

		# Generation request parameters
		self.avgCycle = 0
		self.minRAM = 0
		self.maxRAM = 0
		self.avgStorage = 0
		self.priceList = 0
		self.nSimultaneous = 1
		self.minItv = 1
		self.maxItv = int (SIMULATION_TIME / 2)

		self.tot_time = 0.

		self.active_cores = 0

		self.process_list = []

		self.queue = []

		self.results = {"Requests" : [], "Energy" : {}, "Network" : [], "tot_time": 0.}

		self.nAttend = 0

	def get_results (self):
		cloud_posx = 0
		cloud_posy = 0
		self.results["tot_time"] = self.tot_time
		for psan in self.psanList:
			distance = self.distance ([cloud_posx, cloud_posy], [psan.posX, psan.posY])
			self.results["Energy"][psan] = {"data_r" : psan.results["Energy"]["data_r"],
											"data_s" : psan.results["Energy"]["data_s"],
											"distance" : distance}
		return self.results

	def distance (self, c1, c2):
		return sqrt ((c1[0] - c2[0]) ** 2 + (c1[1] - c2[1]) ** 2)

	def add_neighbor (self, neighbor):
		if neighbor.type == "PSAN":
			self.psanList.append (neighbor)
		else:
			self.neighbor_list.append (neighbor)

	def set_all_data_type_list (self, allDtList):
		self.allDtList = allDtList

	def set_edge_node_parameters (self, masterPerc):
		self.masterPerc = masterPerc

	def generate_request (self):
		request = Request (	int (abs (rnd.gauss (self.avgCycle, self.avgCycle))) + 1,
												int (abs (rnd.randint (self.minRAM, self.maxRAM))),
												int (abs (rnd.randint (self.minStorage, self.maxStorage))),
												rnd.choice (self.allDtList),
												rnd.choice (self.priceList),
												True if rnd.randint (0, 1) == 1 else False)

		return request

	def compute_service_time (self, request):
		#print "service_time:", request.cycle, self.CPU_RATE, float (request.cycle) / self.CPU_RATE
		return float (request.cycle) / self.CPU_RATE

	def run (self):
		self.process = self.env.process (self.operation_thread ())

	def computing (self, request):
		
		computing_time = self.compute_service_time (request) + self.INST_DELAY
		# print "Cloud: Request " + str (request.id), computing_time , raw_input ()
		yield self.env.timeout (computing_time)
		
		util.FINISHED_REQ += 1
		self.tot_time = self.env.now
		self.active_cores -= 1

		request.critical_path.append ("Cloud")
		tStart = request.t_start
		tFinal = self.env.now 
		makespan = tFinal - tStart

		log2.log ( "F Request " + "{:02d}".format (request.id) + ":Cloud,Cloud<-     ,", self.env.now, ",", makespan, ",", self.active_cores, ",d=", self.env.now-request.t_start)

		self.results["Requests"].append ([tStart, tFinal, makespan, self.CLOUD_FULFILLED, 
											self.env.now, request.hop, request.hopedge, 
											request.hopcloud, request.price, 
											request.id,
											request.data_type,
											request.critical_path])

		while self.active_cores <= 64 and len (self.queue) > 0:
			request = self.queue.pop (0)
			self.active_cores += 1
			self.env.process (self.computing (request))

	def operation_thread (self):
		while True:
			#print "operation thread cloud"
			try:
				yield self.env.timeout (SIMULATION_TIME)

			except simpy.Interrupt as interrupt:
				event = interrupt.cause
				if event.type == event.ARRIVED_MSG or event.type == event.ARRIVED_MSG_CLOUD:
					request = event.obj

					if self.psanList:
						continue

					chosen_edges = []
					for edge in self.neighbor_list:
						if edge.check_data_type (request):
							chosen_edges.append (edge)

					edge = util.random.choice (chosen_edges)
					request.type = request.CLOUD_REQUEST

					log2.log ( "  Request " + "{:02d}".format (request.id) + ":Cloud,Cloud->Edge" + str (edge.id) + ",", self.env.now, ",d=", self.env.now-request.t_start)
					self.send_message (edge, request)

				if event.type == event.ARRIVED_DATA_CLOUD:
					event = interrupt.cause
					request = event.obj

					log2.log ( "D Request " + "{:02d}".format (request.id) + ":Cloud,Cloud<-     ,", self.env.now, len (self.queue), self.active_cores, ",d=", self.env.now-request.t_start)
					# print "teste2 ", self.env.now#, raw_input ()

					self.queue.append (request)

					while self.active_cores <= 64 and len (self.queue) > 0:
						request = self.queue.pop (0)
						self.active_cores += 1
						self.env.process (self.computing (request))
					
					log.log ("DATA ARRIVED (CloudNode): " + str (self.env.now) + " s")

	def send_message (self, edge_node, msg):
		msg.critical_path.append ("Cloud")
		msg.dst = edge_node
		ev = Event (Event.ARRIVED_MSG_CLOUD, msg)
		self.interface.send_message (ev, edge_node)

