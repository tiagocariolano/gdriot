import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import pickle
from resource_allocator_algorithm.utils import VERTICAL, ONLY_CLOUD, HIERARCHY, LINE, LINE_RANDOM


def distribution_function_averages(type_interval, links_type, all_results):
    for link_type in links_type:
        y_array = []
        for parameters_by_interval in all_results:
            x_values = []
            y_values = []
            parameters_array, results_array, _ = parameters_by_interval[link_type]

            for parameters, results in zip(parameters_array, results_array):
                average = 0
                for result in results["Requests"]:
                    average += result[2]

                x_values.append(parameters["mean_interval"])
                average /= len(results["Requests"])
                y_values.append(average)
            y_array.append(y_values)

        aux = reduce(lambda x, y: x + y, np.asarray(y_array))

        plt.plot(x_values, aux/len(all_results), label=link_type)

    plt.title(type_interval)
    plt.xlabel("Distribution Function Averages")
    plt.ylabel("Average of makespan")
    plt.legend(loc="upper right")
    plt.show()


def plot_avg_buffe_messages(type_interval, links_type, all_results):
    for link_type in links_type:
        y_array = []
        for parameters_by_interval in all_results:
            x_values = []
            y_values = []

            parameters_array, _, network_results_array = parameters_by_interval[link_type]

            for parameters, network_results in zip(parameters_array, network_results_array):
                x_values.append(parameters["mean_interval"])
                y_values.append(network_results["GeneralNetwork"]["avg"])
            y_array.append(y_values)

        aux = reduce(lambda x, y: x + y, np.asarray(y_array))

        plt.plot(x_values, aux/len(all_results), label=link_type)

    plt.title(type_interval)
    plt.xlabel("Distribution Function Averages")
    plt.ylabel("Average Number of Messages in the Queue")
    plt.legend(loc="upper right")
    plt.show()


def plot_time_x_buffer(type_interval, links_type, parameters_by_interval):
    for link_type in links_type:
        _, _, network_results_array = parameters_by_interval[link_type]

        network_results = network_results_array[0]
        x_values = network_results["GeneralNetwork"]["time"]
        y_values = network_results["GeneralNetwork"]["buffer"]

        plt.plot(x_values, y_values, label=link_type)

    plt.title(type_interval)
    plt.xlabel("Simulation Time")
    plt.ylabel("Number of Queue Messages")
    plt.legend(loc="upper right")
    plt.show()


def plot_line_per_edge_node(type_interval, parameters_by_interval):
    for link_type in parameters_by_interval:
        parameters_array, separete_results, _ = parameters_by_interval[link_type]

        for _, separete_results in zip(parameters_array, separete_results):
            for key in separete_results:
                x_values = []
                y_values = []
                for message_data in separete_results[key]["Requests"]:
                    x_values.append(message_data[0])
                    y_values.append(message_data[2])
                plt.plot(x_values, y_values, label=key)

        plt.title(type_interval)
        plt.xlabel("Message start")
        plt.ylabel("Makspan")
        plt.legend(loc='upper right')
        plt.show()


def plot_requests_status(links_type, all_results):
    for link_type in links_type:
        fast_fulfilled_array = []
        slow_fulfilled_array = []
        not_fulfilled_array = []

        for parameters_by_interval in all_results:
            x_values = []

            fast_fulfilled_values = []
            slow_fulfilled_values = []
            not_fulfilled_values = []
            parameters_array, results_array = parameters_by_interval[link_type]

            for parameters, results in zip(parameters_array, results_array):
                requests_fast_fulfilled = 0
                requests_slow_fulfilled = 0
                requests_not_fulfilled = 0

                for result in results["Requests"]:
                    if len(result) < 4:
                        requests_not_fulfilled += 1
                    elif result[3] == "FAST_FULFILLED":
                        requests_fast_fulfilled += 1
                    elif result[3] == "SLOW_FULFILLED":
                        requests_slow_fulfilled += 1
                    else:
                        requests_not_fulfilled += 1

                size = len(results["Requests"])

                if link_type == HIERARCHY:
                    x_values.append(parameters["levels"])
                else:
                    x_values.append(parameters["n_entities"])
                fast_fulfilled_values.append(requests_fast_fulfilled/float(size))
                slow_fulfilled_values.append(requests_slow_fulfilled/float(size))
                not_fulfilled_values.append(requests_not_fulfilled/float(size))

            fast_fulfilled_array.append(fast_fulfilled_values)
            slow_fulfilled_array.append(slow_fulfilled_values)
            not_fulfilled_array.append(not_fulfilled_values)

        aux_1 = reduce(lambda x, y: x + y, np.asarray(fast_fulfilled_array))
        aux_2 = reduce(lambda x, y: x + y, np.asarray(slow_fulfilled_array))
        aux_3 = reduce(lambda x, y: x + y, np.asarray(not_fulfilled_array))

        plt.plot(x_values, aux_1/len(all_results), label="fast fulfilled")
        plt.plot(x_values, aux_2/len(all_results), label="slow fulfilled")
        plt.plot(x_values, aux_3/len(all_results), label="not fulfilled")

        plt.title(link_type)
        plt.xlabel("Number of Edge Nodes")
        plt.ylabel("")
        plt.legend(loc="center right")
        plt.show()
