from packet import Packet

class Data(Packet):

    def __init__ (self, type, inst, bytes, timestamp):
        self.type = type
        self.inst = inst
        self.bytes = bytes
        self.timestamp = timestamp