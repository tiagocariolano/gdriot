import numpy as np
import util
from request import Request
from data_type import DataType
from data import Data
from util import *

class PSAN:
    PSAN = "PSAN"
    DELAY = 0.1

    def __init__(self, env, interface, id, x, y, data_type, delay=DELAY):
        self.id = id
        self.env = env
        self.interface = interface
        self.x = x
        self.y = y
        self.data_type = data_type
        self.delay = delay

        self.type = self.PSAN   

        self.neighbor_list = []
        self.process = None

        self.tot_msg = 0
        self.results = {"Messages" : 0, "Energy" : {"data_r": 0, "data_s": 0, "delay": .0}, "Network" : []}

    def add_neighbor (self, neighbor):
        self.neighbor_list.append (neighbor)

    def generate_delay (self):

        switcher = {
            "constant":     self.interval_mean,
            "normal":       abs (util.np.random.normal (self.interval_mean, self.interval_std)),
            "exponential":  abs (util.np.random.exponential (self.interval_mean)),
            "uniform":      abs (util.np.random.uniform (0, 2 * self.interval_mean))
        }
        return switcher.get (self.interval_type, "Invalid interval_type for psan.")

    def set_delay (self, delay):
        self.delay = delay

    def get_data (self, request):
        data = Data (request.type, request.cycle, request.mem, self.env.now)

        self.results["Messages"] += 1
        # self.results["Energy"]["data_r"] += data.bytes
        self.results["Energy"]["data_r"] += request.DEFAULT_BYTES
        self.results["Energy"]["data_s"] += data.bytes
        self.results["Energy"]["delay"] += self.delay
        return data

    def run (self):
        self.process = self.env.process (self.operation_thread ())

    def sending (self, request):
        data = self.get_data (request)
        yield self.env.timeout (self.delay)

        request.set_data (data)

        if request.type == Request.EDGE_REQUEST:
            request.type == Request.EDGE_DATA
            for neighbor in self.neighbor_list:
                self.send_edge_data (neighbor, request)
            
            log2.log ("  Request " + "{:02d}".format (request.id) + ":Psan ,Ps " + "{:02d}".format (self.id) + "->Edge" + str (request.edge_origin.id), ",", "{:014.10f}".format (self.env.now), ",d=", self.env.now-request.t_start)
        elif request.type == Request.CLOUD_REQUEST:
            request.type == Request.CLOUD_DATA
            for neighbor in self.neighbor_list:
                self.send_cloud_data (neighbor, request)
            
            log2.log ( "  Request " + "{:02d}".format (request.id) + ":Psan ,Ps " + "{:02d}".format (self.id) + "->Edge" + str (request.edge_origin.id),",", "{:014.10f}".format (self.env.now), ",d=", self.env.now-request.t_start)

        self.results["Messages"] += 1
        self.results["Energy"]["data_r"] += data.bytes
        self.results["Energy"]["data_s"] += data.bytes


    def operation_thread (self):
        while True:
            try:
                yield self.env.timeout (SIMULATION_TIME)
            except simpy.Interrupt as interrupt:
                event = interrupt.cause
                if event.type == Event.ARRIVED_MSG:
                    request = event.obj
                    request.rx_tstamp = self.env.now
                    request.send_delay = request.rx_tstamp - request.tx_tstamp
                    # print "PSAN: ", self.id, ", ", request.id, request.rx_tstamp, request.tx_tstamp


                    log.log ("PSAN_REQ ", request.edge_origin.id, " => ", request, ", t=", self.env.now)

                    self.env.process (self.sending (request))
                elif event.type == Event.ARRIVED_MSG_CLOUD:
                    request = event.obj

                    self.env.process (self.sending (request))
                    
    def send_edge_data (self, neighbor, msg):
        msg.critical_path.append ("Psan")
        msg.tx_tstamp = self.env.now
        ev = Event (Event.ARRIVED_DATA, msg)
        self.interface.send_message (ev, neighbor)

    def send_cloud_data (self, neighbor, msg):
        msg.critical_path.append ("Psan")
        ev = Event (Event.ARRIVED_DATA_CLOUD, msg)
        self.interface.send_message (ev, neighbor)

    def get_results (self):
        return self.results

