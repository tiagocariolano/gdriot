import numpy as np
import util
from request import Request
from util import *
from data_type import DataType

class SourceNode:
    INTERVAL_TYPE = "constant"
    INTERVAL_MEAN = 5.
    INTERVAL_STD = 0.1
    FRESHNESS_MEAN = 0.1
    FRESHNESS_STD = 1.
    N_REQUEST = 1000

    def __init__(self, env, interface,  interval_type, n_request=N_REQUEST):
        self.env = env
        self.interface = interface
        self.interval_type = interval_type
        self.interval_mean = self.INTERVAL_MEAN
        self.interval_std = self.INTERVAL_STD
        self.n_request = n_request

        self.type = "REQUESTER"
        self.neighbor_list = []
        self.request_id = 0
        
        self.freshness_mean = self.FRESHNESS_MEAN
        self.freshness_std = self.FRESHNESS_STD

        self.mem_list = []
        self.cycle_list = []
        self.data_type_list = []

        util.TOT_REQ = self.n_request
        self.process = None

    def add_neighbor (self, neighbor):
        self.neighbor_list.append (neighbor)

    def set_cycle_list (self, cycle_list):
        self.cycle_list = cycle_list

    def set_mem_list (self, mem_list):
        self.mem_list = mem_list

    def set_data_type_list (self, data_type_list):
        self.data_type_list = data_type_list

    def set_interval (self, interval_mean, interval_std=0):
        self.interval_mean = interval_mean
        self.interval_std = interval_std

    def set_freshness (self, freshness_mean, freshness_std=0):
        self.freshness_mean = freshness_mean
        self.freshness_std = freshness_std

    def get_request (self, request_id):
        request = Request ( request_id,
                            util.random.choice (self.cycle_list),
                            util.random.choice (self.mem_list),
                            util.random.choice (self.data_type_list),
                            util.np.random.uniform (0, 2 * self.freshness_mean),
                            self)
        request.set_t_start (self.env.now)

        return request

    def get_neighbor (self):
        return util.np.random.choice (self.neighbor_list)

    def get_interval (self):
        switcher = {
            "constant":     self.interval_mean,
            "normal":       abs (util.np.random.normal (self.interval_mean, self.interval_std)),
            "exponential":  abs (util.np.random.exponential (self.interval_mean))
        }

        return switcher.get (self.interval_type, "Invalid interval type.")

    def run (self):
        self.process = self.env.process (self.operation_thread ())

    def operation_thread (self):

        for i in xrange (self.n_request):
            self.request_id += 1
            request = self.get_request (self.request_id)
            interval = self.get_interval ()
            neighbor = self.get_neighbor ()
            n_neighbor = len (self.neighbor_list)
            # print self.mem_list, request, raw_input ()
            p = 0.05
            q = float (1. - p) / (n_neighbor - 1)
            probs = [p, q, q, q, q, q, q, q]
            probs = [1. / float (n_neighbor)] * n_neighbor
            # probs = [perc] + [(1. - perc) / (n_neighbor - 1)] * (n_neighbor - 1) #2
            # probs = [def_perc, def_perc, perc] + [(1. - perc) / (n_neighbor - 1)] * (n_neighbor - 3) #4
            # neighbor = util.np.random.choice (self.neighbor_list, p=probs)
            self.send_message (neighbor, request)

            yield self.env.timeout(interval)

    def send_message (self, neighbor, msg):
        msg.critical_path.append ("Source")
        ev = Event (Event.ARRIVED_MSG, msg)
        self.interface.send_message (ev, neighbor)
