class Packet:
    
    DEFAULT_INST = 1 * 100
    DEFAULT_BYTES = 1 * 100
    EDGE_REQUEST   = "EDGE_REQUEST"
    EDGE_DATA      = "EDGE_DATA"
    CLOUD_REQUEST  = "CLOUD_REQUEST"
    CLOUD_DATA     = "CLOUD_DATA"

    def __init__ (self):
        self.inst = 0
        self.bytes = 0
        self.source = None
        self.type = None
        self.tx_tstamp = .0
        self.rx_tstamp = .0
        self.total = .0

        