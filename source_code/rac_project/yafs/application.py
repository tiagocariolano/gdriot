class Message:
    """
    A message is set by the following values:

    Args:
        name (str): a name, unique for each application

        src (str): the name of module who send this message

        dst (dst): the nsame of module who recibe this message

        inst (int): the number of instrucctions to be executed ((by default 0), Instead of MIPS, we use IPt since the time is relative to the simulation units.

        bytes (int): the size in bytes (by default 0)

    Internal args used in the **yafs.core** are:
        timestamp (float): simulation time. Instant of time that was created.

        path (list): a list of entities of the topology that has to travel to reach its target module from its source module.

        dst_int (int): an identifier of the intermediate entity in which it is in the process of transmission.

        app_name (str): the name of the application
    """

    def __init__(self, name, src, dst, instructions=0, bytes=0,broadcasting=False):
        self.name = name
        self.src = src
        self.dst = dst
        self.inst = instructions
        self.bytes = bytes

        self.timestamp = 0
        self.path = []
        self.dst_int = None
        self.app_name = None
        self.timestamp_rec = 0

        self.tEdgeModIn = 0
        self.tEdgeModOut = 0

        self.data = None

        self.idDES = None
        self.broadcasting = broadcasting
        self.last_idDes = []

        self.queue_size = 0

    def set_data (self, data):
        self.data = data

    def __str__(self):
        print  "{--"
        print "Name: %s" %self.name
        print "From (src): %s  to (dst): %s" %(self.src,self.dst)
        print " --}"
        return ""


# class AppInterface:
#     QUEUE = "QUEUE"
#     EVENT = "EVENT"
#
#     def __init__ (self, obj, moduleList, typeCommSim, objCommSim, idExt, typeCommExt, objCommExt):
#         self.moduleList = moduleList
#
#     def push_message (self, msg):
#         extMsg = msg.data
#         if typeCommExt == self.QUEUE:
#             self.objCommExt.put (extMsg)
#         elif typeCommExt == self.EVENT:
# 			self.objCommExt.interrupt (extMsg)
#         else:
#             print "Error: Invalid type of communication."
#             exit (1)
#     def send_message (self, msg, dstObj):
#         emitModule = None
#         dstModule = None
#
#         for module in self.moduleList:
#             if self.moduleList[module]["obj"] == obj:
#                 emitModule = module
#             if self.moduleList[module["obj"]] == dstObj:
#                 dstModule = module
#
#         if emitModule and dstModule:
#             appMsg = Message (dstModule, emitModule, dstModule)
#             appMsg.set_data (msg)
#             self.appCommObj.put (appMsg)
#         else:
#             print "Error: There are not source {} or destination {}".format (emitModule, dstModule)


class Application (object):
    """
    An application is defined by a DAG between modules that generate, compute and receive messages.

    Args:
        name (str): The name must be unique within the same topology.

    Returns:
        an application

    """
    TYPE_SOURCE = "SOURCE"  # "SENSOR"
    "A source is like sensor"

    TYPE_MODULE = "MODULE"
    "A module"

    TYPE_SINK = "SINK"
    "A sink is like actuator"

    TYPE_EDGE = "EDGE"
    "A edge node module"

    def __init__(self, name):
        self.name = name
        self.services = {}
        self.messages = {}
        self.modules = []
        self.modules_src = []
        self.modules_sink = []
        self.modules_edge = []
        self.data = {}

        self.upLayerAddresser = {}

    def set_modules(self,data):
        """
        Pure source or sink modules must be typified

        Args:
            data (dict) : a set of characteristic of modules
        """
        for module in data:
            name = module.keys()[0]
            type = module.values()[0]["Type"]
            if type == self.TYPE_SOURCE:
                self.modules_src.append(name)
            elif type == self.TYPE_SINK:
                self.modules_sink.append (name)
            elif type == self.TYPE_EDGE:
                self.modules_edge.append (name)

            self.modules.append(name)

        self.data = data

        # self.modules_sink = modules
    # def set_module(self, modules, type_module):
    #     """
    #     Pure source or sink modules must be typified
    #
    #     Args:
    #         modules (list): a list of modules names
    #         type_module (str): TYPE_SOURCE or TYPE_SINK
    #     """
    #     if type_module == self.TYPE_SOURCE:
    #         self.modules_src = modules
    #     elif type_module == self.TYPE_SINK:
    #         self.modules_sink = modules
    #     elif type_module == self.TYPE_MODULE:
    #         self.modules_pure = modules

    def get_pure_modules(self):
        """
        Returns:
            a list of pure source and sink modules
        """
        return [s for s in self.modules if s not in self.modules_src and s not in self.modules_sink]

    def get_sink_modules(self):
        """
        Returns:
            a list of sink modules
        """
        return self.modules_sink

    def add_source_messages(self, msg):
        """
        Add in the application those messages that come from pure sources (sensors). This distinction allows them to be controlled by the (:mod:`Population`) algorithm
        """
        self.messages[msg.name] = msg


    def get_message(self,name):
        """
        Returns: a message instance from the identifier name
        """
        return self.messages[name]

    """
    ADD SERVICE
    """

    def add_service_source(self, module_name, distribution=None, message=None, module_dest=[], p=[]):
        """
        Link to each non-pure module a management for creating messages

        Args:
            module_name (str): module name

            distribution (function): a function with a distribution function

            message (Message): the message

            module_dest (list): a list of modules who can receive this message. Broadcasting.

            p (list): a list of probabilities to send this message. Broadcasting

        Kwargs:
            param_distribution (dict): the parameters for *distribution* function

        """
        if distribution is not None:
            if module_name not in self.services:
                self.services[module_name] = []
            self.services[module_name].append(
                {"type": Application.TYPE_SOURCE, "dist": distribution,
                 "message_out": message, "module_dest": module_dest, "p": p})

    def add_service_module(self, module_name, message_in, message_out="", distribution="", module_dest=[], p=[],
                           **param):

        """
        Link to each non-pure module a management of transfering of messages

        Args:
            module_name (str): module name

            message_in (Message): input message

            message_out (Message): output message. If Empty the module is a sink

            distribution (function): a function with a distribution function

            module_dest (list): a list of modules who can receive this message. Broadcasting.

            p (list): a list of probabilities to send this message. Broadcasting

        Kwargs:
            param (dict): the parameters for *distribution* function

        """
        if not module_name in self.services:
            self.services[module_name] = []

        self.services[module_name].append({"type": Application.TYPE_MODULE, "dist": distribution, "param": param,
                                           "message_in": message_in, "message_out": message_out,
                                           "module_dest": module_dest, "p": p})

    def add_service_edge_module (self, moduleName, upLayerId, upLayerInterface, **param):

        if not moduleName in self.services:
            self.services[moduleName] = []

        self.services[moduleName] = {   "type" :Application.TYPE_EDGE,
                                        "moduleName": moduleName,
                                        "upLayerInterface": upLayerInterface,
                                        "net_queue": 0}
        self.upLayerAddresser[upLayerId] = moduleName

class UpLayerInterface:
    QUEUE = "QUEUE"
    EVENT = "EVENT"

    def __init__ (self, upLayerAddresser, moduleAssoc): #, upLayerComm, upLayerCommType):
        self.upLayerAddresser = upLayerAddresser
        self.moduleAssoc = moduleAssoc
        self.moduleComm = None
        self.upLayerComm = None
        self.upLayerCommType = None
        self.queue_size = 0

    def set_up_layer_comm (self, upLayerComm, upLayerCommType):
        self.upLayerComm = upLayerComm
        self.upLayerCommType = upLayerCommType

    def set_module_comm (self, moduleComm):
        self.moduleComm = moduleComm

    def push_message (self, msg):
        upLayerMsg = msg.data
        if self.upLayerCommType == self.QUEUE:
            self.upLayerComm.put (upLayerMsg)
        elif self.upLayerCommType == self.EVENT:
            self.upLayerComm.interrupt (upLayerMsg)
        else:
            print "Error: Invalid type of communication '" + self.upLayerCommType + "."
            exit (1)

    def send_message (self, msg, dst):
        moduleDst = self.upLayerAddresser[dst]
        moduleMsg = Message (moduleDst, self.moduleAssoc, moduleDst)
        #moduleMsg.set_data (msg)
        moduleMsg.data = msg
        moduleMsg.inst = msg.obj.inst
        moduleMsg.bytes = msg.obj.bytes
        self.moduleComm.put (moduleMsg)

    def __str__ (self):
        out =   "moduleAssoc    : {}\nmoduleComm      : {}\nupLayerComm     : {}\nupLayerCommType : {}\n".format (\
                self.moduleAssoc,\
                str (self.moduleComm),\
                str (self.upLayerComm),\
                str (self.upLayerCommType)\
                )
        return out
