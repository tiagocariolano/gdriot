import util
from util import *

class VirtualNode():
	### Virtual node states
	NEW = "NEW"
	IDLE = "IDLE"
	BUSY = "BUSY"
	READY = "READY"
	REQUIREMENTLESS = "REQUIREMENTLESS"
	STORED = "STORED"

	NOTFULFILLED = "NOT_FULFILLED"
	FASTFULFILLED = "FAST_FULFILLED"

	RECONF_DELAY 	= 0.100	#seconds
	INST_DELAY 		= 0.100 		#seconds

	def __init__(self, en, cpu_rate, mem, data_type):
		self.id = en.next_vn_id ()
		self.cpu_rate = cpu_rate
		self.mem = mem
		self.data_type = data_type
		self.state = self.NEW
		self.n_runs = 0
		self.request = None
		
		self.en = en
		self.env = en.env
		self.process = None

		self.processing_delay = 0
		self.delay_reconf = self.RECONF_DELAY
		self.delay_inst = self.INST_DELAY

		self.reqState = self.NOTFULFILLED
		self.start = 0
		
	def check_data_type (self, request):
		"""
			Check if data type is equal to request and vn is idle

			Args:
				request: a request

			Return: a boolean indicanting data type is equal to request and state
			is IDLE
		"""
		return self.data_type == request.data_type

	def check_memory (self, request):
		"""
			Check if RAM of vn are sufficient to request.

			Args:
				request: a request

			Return: a boolean indicanting if RAM and storage of vn is greater than
			of the request
		"""
		return 	self.mem >= request.mem

	def check_idle_state (self):
		return self.state == self.IDLE

	def check_busy_state (self):
		return self.state == self.BUSY

	def check_new_state (self):
		return self.state == self.NEW

	def check_ready_state (self):
		return self.state == self.READY

	def check_requirementless_state (self):
		return self.state == self.REQUIREMENTLESS

	def check_stored_state (self):
		return self.state == self.STORED

	def check_freshness (self, request):
		freshness = float (self.env.now) - self.request.data.timestamp
		if freshness <= request.freshness:
			return True
		else:
			return False

	def set_data (self, data):
		self.data = data
		self. state = self.READY

	def provisioning (self, request):
		"""
			Set vn request to input request, reconfigure vn if necessary and
			set state to ALLOCATE

			Args:
				request: request to be allocated
		"""

		if self.request is None or self.request.data is None:
			self.state = self.NEW
		elif not self.check_freshness (request):
			self.state = self.REQUIREMENTLESS
		else:
			self.state = self.READY
		
		data = None if self.request is None else self.request.data

		self.request = request
		self.request.data = data
		self.mem = request.mem
		self.processing_delay = self.compute_proc_delay (request)

	def compute_proc_delay (self, request):
		"""
			Compute tf time of request, it is time when request will end.

			Return: tf, final time of request
		"""
		proc_delay = float (request.cycle) / self.cpu_rate

		if self.state in [self.NEW, self.STORED]:
			proc_delay += self.delay_reconf

		return proc_delay

	def run (self):
		self.state = self.BUSY

		self.process = self.env.process (self.main ())

	def main (self):
		"""
			Simulate execution of vn.
		"""
		
		self.state = self.BUSY
		# print "VN: Request " + str (self.request.id), self.processing_delay, raw_input ()
		yield self.env.timeout (self.processing_delay)

		self.state = self.IDLE

		self.reqState = self.FASTFULFILLED

		self.n_runs += 1

		
		self.en.process.interrupt (Event (Event.FINISHED_VN, self))

	def __str__ (self):
		return "VN     {:02d}: {:3.3s}, {:010.4f}, {:010.4f}".format (
			self.id, self.state, self.mem / MB, self.processing_delay)

