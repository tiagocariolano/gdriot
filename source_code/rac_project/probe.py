from packet import Packet
from util import *

class Probe(Packet):

    def __init__ (self, id, edge_id, queue_size, edge, cycle=500, mem=500):
        self.id = id
        self.edge_id = edge_id
        self.edge = edge
        self.queue_size = queue_size
        self.cycle = cycle
        self.mem = mem
        
        self.inst = cycle
        self.bytes = mem

    def __str__ (self):
		return "PROBE {:04d}: {:4.2f}, {:02d} ".format (self.id, self.utilization, self.edge.id)
    
