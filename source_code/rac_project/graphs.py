import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from resource_allocator_algorithm.utils import VERTICAL, ONLY_CLOUD, HIERARCHY, LINE, LINE_RANDOM

class Statistics:
    TYPE_MEAN = "TYPE_MEAN"
    TYPE_MAX = "TYPE_MAX"

    def __init__ (self, parser,
                        prefix_path="../",
                        extension="pickle",
                        files_names=[]):

        self.parser = parser
        self.prefix_path = prefix_path
        self.extension =  extension
        self.files_names = files_names

    def read (self):
        self.means = []
        self.maxs = []

        for rounds_names in self.files_names:
            paths = []
            for file_name  in rounds_names:
                paths.append (self.prefix_path + file_name + "." + self.extension)

            table_list = self.parser.read (paths, type=self.parser.MULTIPLE_FILE)
            #print table_list
            #print rounds_names
            # print table_list, raw_input ()

            mean_list = []
            max_list = []
            for table in table_list:
                mean_list.append (table.mean (axis=0))
                max_list.append (table.max (axis=0))

            self.means.append (np.array(mean_list))
            self.maxs.append (np.array (max_list))

        # print "means", self.means
        # print "maxs", self.maxs, raw_input ()

    def format_by_column (self, col_slice, type=TYPE_MEAN):
        data = []
        if type == self.TYPE_MEAN:
            for mean_strategy in self.means:
                data.append (np.array (mean_strategy[:, col_slice]))
        elif type == self.TYPE_MAX:
            for max_strategy in self.maxs:
                data.append (np.array (max_strategy[:, col_slice]))

        return np.array (data)

    def format_by_line (self, line, type_stat=TYPE_MEAN):
        data = []
        for mean_strategy in self.means:
            if type == self.TYPE_MEAN:
                data.append (np.array (mean_strategy[line, :]))
            elif type == self.TYPE_MAX:
                data.append (np.array (mean_strategy[line, :]))

    def get_column (self, column):
        return self.means[:, column]

    def get_line (self, line):
        return self.means[line, :]

class Plot:
    DEFAULT_LINE_STYLE = "--"
    DEFAULT_MAKER = "o"
    DEFAULT_COLORS = ["black", "blue", "red", "green"]
    DEFAULT_PREFIX_PATH = "./"
    DEFAULT_FILE_NAME = ""

    def __init__ (self, linestyle   = DEFAULT_LINE_STYLE,
                        marker      = DEFAULT_MAKER,
                        colors      = DEFAULT_COLORS,
                        prefix_path = DEFAULT_PREFIX_PATH):

        self.fig, self.ax = plt.subplots()

        self.linestyle = linestyle
        self.marker = marker
        self.colors = colors
        self.prefix_path =prefix_path

    def _save (self, file_name):
        if file_name == "":
            plt.show ()
        else:
            self.fig.savefig (file_name)
            plt.close (self.fig)

    def _legend (self, legend, legend_location):
        if legend:
            plt.legend(legend, loc=legend_location)

    def _xticks (self, xticks_values, xticks_labels):
        if xticks_values != []:
            plt.xticks (xticks_values, xticks_labels)

    def lines (self , X, Y,
                            file_name="",
                            xlabel="xlabel",
                            ylabel="ylabel",
                            title="title",
                            legend=(),
                            legend_location="upper right",
                            xticks_values=[],
                            xticks_labels=[]):

        color_index = 0
        for x, y in zip (X, Y):
            plt.plot (x, y,
                linestyle   = self.linestyle,
                marker      = self.marker,
                color       = self.colors[color_index])
            color_index += 1

        plt.xlabel (xlabel)
        plt.ylabel (ylabel)
        plt.title (title)

        self._legend (legend, legend_location)

        self._xticks (xticks_values, xticks_labels)

        self._save (file_name)

        # plt.xlabel ("Data Freshness Requeriment")
        # plt.ylabel ("Makespan")
        # plt.title ("Average makespan")
        # plt.legend(('Colab. Vertical', 'Colab. Horizontal', 'Hierarquia'), loc='upper right')

    def bars (self, X, Y,
                        file_name="",
                        xlabel="xlabel",
                        ylabel="ylabel",
                        title="title",
                        legend=(),
                        legend_location="upper right",
                        xticks_values=[],
                        xticks_labels=[],
                        vertical_space=0,
                        normalize=False):

        bottom = np.zeros (len (X[0]))
        color_index = 0

        if normalize:
            for y in Y:
                total = np.sum (y)
                y = y / total

        Y = np.transpose (Y)

        title_index = 0
        for x, y in zip (X, Y):

            plt.xlabel (xlabel)
            plt.ylabel (ylabel)
            plt.title (title[title_index])



            plt.bar (x, y,
                    bottom  = bottom,
                    color   = self.colors[color_index])
            bottom = bottom + y + vertical_space
            color_index += 1
            title_index += 1


        self._legend (legend, legend_location)

        self._xticks (xticks_values, xticks_labels)

        self._save (file_name)

class Parser:
    SINGLE_FILE = "SINGLE_FILE"
    MULTIPLE_FILE = "MULTIPLE_FILE"

    def __init__ (self, rounds=10, n_parameters=10, n_values=10):
        self.content = None
        self.rounds = rounds
        self.n_parameters = n_parameters
        self.n_values = n_values

    def _read_file (self, path):
        content = []
        try:
            with open (path) as f:
                content = pickle.load (f)
        except:
            print "Graph: File " + path + " cannot be opened."
            exit ()

        return content

    def get_dimensions (self):
        return (self.rounds, self.values, self.n_parameters)

    def read (self, paths, type=SINGLE_FILE):
        if type == self.SINGLE_FILE:
            self.raw = self._read_file (paths)
        elif type == self.MULTIPLE_FILE:
            self.raw = {"Stats" : []}
            for path in paths:
                data = self._read_file (path)
                self.raw["Stats"].extend (data["Stats"])

        self.content = np.zeros ((self.n_parameters, self.rounds, self.n_values))


        for i in xrange (self.n_parameters):
            for j in xrange (self.rounds):
                iIndex = (i * self.rounds + j) % self.n_parameters
                jIndex = (i * self.rounds + j) / self.n_parameters
                rawIndex = i * self.rounds + j

                #print rawIndex, iIndex, jIndex
                #self.content[iIndex][jIndex] = self.raw["Stats"][rawIndex][0:self.n_values]
                #print self.raw["Stats"][rawIndex][0:self.n_values], raw_input ()
                self.content[iIndex][jIndex] = self.raw["Stats"][rawIndex][0:self.n_values]

        return self.content

if __name__ == "__main__":

    plot_type           = "makespan"
    var_param_name      = "mean_interval"
    freshness_list      = [1, 0.1, 0.01, 0.001, 0.0001, 0.00001]
    freshness_list      = [0.00001, 0.0001, 0.001, 0.01, 0.1, 1]
    request_rate        = [0.01, 0.008, 0.006, 0.004, 0.002, 0.001, 0.0008, 0.0006, 0.0004, 0.0002, 0.0001]
    n_edge_node_list    = [4, 8, 16, 32]
    links_types         = [VERTICAL, LINE_RANDOM, HIERARCHY]
    min_rounds          = 0
    max_rounds          = 26
    n_values            = 10
    stat_prefix_path    = "./outputfiles/old/"
    stat_extension      = "pickle"
    field_makespan      = 0
    field_network       = 7
    field_energy        = 9
    field_request_perc  = slice (1,4)

    var_param_list = []
    if var_param_name == "mean_interval":
        var_param_list  = request_rate
    elif var_param_name == "freshness":
        var_param_list = freshness_list
    elif var_param_name == "n_edge_node":
        var_param_list = n_edge_node_list

    rounds = max_rounds - min_rounds
    n_parameters = len (var_param_list)

    files_names = []
    for link_type in links_types:
        rounds_names = []
        for i in xrange (min_rounds, max_rounds):
            file_name = "{0:6.6s}-{1:03d}-{2:6.6s}".format (link_type, i, var_param_name).replace (" ", "_")
            rounds_names.append (file_name)
        files_names.append (rounds_names)


    stats = Statistics (Parser (rounds=rounds, n_parameters=n_parameters, n_values=n_values),
                        prefix_path=stat_prefix_path,#"/outputfiles/",
                        extension=stat_extension,
                        files_names=files_names)
    stats.read ()

    if plot_type == "makespan":
        n_x = len (links_types)
        n_y = len (var_param_list)

        print stats.means

        X = np.broadcast_to (np.arange (n_y), (n_x, n_y))
        Y = stats.format_by_column (field_makespan)

        print X
        print Y

        # X = X[:, 0:n_values]
        # Y = Y[:, 0:n_values]

        if var_param_name == "mean_interval":
            legend = ("Colab. Vertical", "Colab. Horizontal", "Hierarquia")
            ylabel = "Tempo de resposta (segundos)"
            legend_location = "upper left"
            xlabel = "Taxa de requisicoes (Requisicoes / segundo)"
            title = "Media do Tempo de Resposta X Taxa de requisicoes"
            xticks_values = np.arange (n_y)
            xticks_labels  = ["{0:5.2f}".format (v) for v in 1 / np.array (var_param_list) / 100]
            #xticks_labels = xticks_labels[:-1]
            #xticks_labels  = np.array (var_param_list)
        elif var_param_name == "n_edge_node":
            legend = ("Colab. Vertical", "Colab. Horizontal", "Hierarquia")
            ylabel = "Tempo de resposta (segundos)"
            legend_location = "upper right"
            xlabel = "Numero de Edge Nodes"
            title = "Media do Tempo de Resposta X Numero de Edge Nodes"
            xticks_values = np.arange (n_y)
            #xticks_labels  = ["{0:5.2f}".format (v) for v in 1 / np.array (var_param_list) / 100]
            xticks_labels  = np.array (var_param_list)

        plot = Plot ()
        plot.lines (X, Y,
                    legend          = legend,
                    legend_location = legend_location,
                    xlabel          = xlabel,
                    ylabel          = ylabel,
                    title           = title,
                    xticks_values   = xticks_values,
                    xticks_labels   = xticks_labels
                    )

    elif plot_type ==  "request_perc" and var_param_name == "freshness":
        n_x = 3
        n_y = len (var_param_list)

        requests_percs = stats.format_by_column (field_request_perc)

        for request_perc in requests_percs:
            X = np.broadcast_to (np.arange (n_y), (n_x, n_y))
            Y = request_perc

            print X
            print Y

            if var_param_name == "freshness":
                legend = ("EdgeNode-Imediato", "EdgeNode-AguardaPSAN", "Nao-Atendido/Cloud")
                legend_location = "lower right"
                title = [
                            "Atendimento das requisicoes X Frescor do dado - Hierarquia",
                            "Atendimento das requisicoes X Frescor do dado - Hierarquia",
                            "Atendimento das requisicoes X Frescor do dado - Hierarquia"
                            ]
                ylabel = "Requisicoes (%)"
                xlabel = "Frescor do dado (s)"
                xticks_values = np.arange (n_y)
                #xticks_labels  = 1 / np.array (var_param_list) / 100
                xticks_labels  = np.array (var_param_list)

            plot = Plot ()
            plot.bars ( X, Y,
                        legend          = legend,
                        legend_location = legend_location,
                        xlabel          = xlabel,
                        ylabel          = ylabel,
                        title           = title,
                        xticks_values   = xticks_values,
                        xticks_labels   = xticks_labels,
                        vertical_space  = 0.005,
                        normalize=False)

    elif plot_type == "avg_network":
        n_x = len (links_types)
        n_y = len (var_param_list)

        X = np.broadcast_to (np.arange (n_y), (n_x, n_y))
        Y = stats.format_by_column (field_network, type=stats.TYPE_MAX)

        print X
        print Y

        X = X[:, 0:n_values]
        Y = Y[:, 0:n_values]

        if var_param_name == "mean_interval":
            legend = ("Colab. Vertical", "Colab. Horizontal", "Hierarquia")
            ylabel = "Numero de Mensagens"
            legend_location = "upper left"
            xlabel = "Taxa de requisicoes (Requisicoes / segundo)"
            title = "Trafego de Rede"
            xticks_values = np.arange (n_y)
            #xticks_labels  = 1 / np.array (var_param_list) / 100
            xticks_labels  = ["{0:5.2f}".format (v) for v in 1 / np.array (var_param_list) / 100]
            xticks_values = np.arange (n_y - 1)
            xticks_labels  = ["{0:5.2f}".format (v) for v in 1 / np.array (var_param_list) / 100]
            xticks_labels = xticks_labels[:-1]


        plot = Plot ()
        plot.lines (X, Y,
                    legend          = legend,
                    legend_location = legend_location,
                    xlabel          = xlabel,
                    ylabel          = ylabel,
                    title           = title,
                    xticks_values   = xticks_values,
                    xticks_labels   = xticks_labels
                    )

    elif plot_type == "energy":
        n_x = len (links_types)
        n_y = len (var_param_list)

        X = np.broadcast_to (np.arange (n_y), (n_x, n_y))
        Y = stats.format_by_column (field_energy, type=stats.TYPE_MEAN)


        print X
        print Y

        X = X[:, 0:n_values]
        Y = Y[:, 0:n_values]

        Y = np.fliplr (Y)

        print Y
        if var_param_name == "freshness":
            legend = ("Cloud", "Colab. Vertical", "Colab. Horizontal", "Hierarquia")
            ylabel = "Energy"
            legend_location = "lower left"
            xlabel = "Data Freshness"
            title = ""
            xticks_labels = var_param_list
            #xticks_labels  = 1 / np.array (var_param_list) / 100
            #xticks_labels  = ["{0:5.2f}".format (v) for v in 1 / np.array (var_param_list) / 100]
            xticks_values = np.arange (n_y)
            #xticks_values = np.arange (n_y)
            #xticks_labels  = ["{0:5.2f}".format (v) for v in 1 / np.array (var_param_list) / 100]
            #xticks_labels = xticks_labels[:-1]


        plot = Plot ()
        plot.lines (X, Y,
                    legend          = legend,
                    legend_location = legend_location,
                    xlabel          = xlabel,
                    ylabel          = ylabel,
                    title           = title,
                    xticks_values   = xticks_values,
                    xticks_labels   = xticks_labels
                    )

    # Y = stats.format_by_column (0, type=stats.TYPE_MEAN)
    #
    # X = []
    # for mean_strategy in stats.means:
    #     # X.append (np.array (freshness_list))
    #     X.append (np.arange (len (freshness_list)))
    #
    # print stats.means
    #
    ### BAR PLOT
    # requests_percs = stats.format_by_column (slice (1,4))
    #
    # print requests_percs
    #
    # n_x = 3
    # n_y = len (var_param_list)
    #
    # for request_perc in requests_percs:
    #     Y = request_perc
    #     X = np.broadcast_to (np.arange (n_y), (n_x, n_y))
    #
    #     print X
    #     print Y, "\n"
    #
    #     plot = Plot ()
    #     plot.bars ( X, Y,
    #                 legend          = ("Imediatamente-EdgeNode", "Aguarda-End-Device", "Nao-Atendida"),
    #                 legend_location = "lower right",
    #                 xticks_values   = X[0],
    #                 xticks_labels   = var_param_list,
    #                 vertical_space  = 0.005,
    #                 normalize=False)

    # plot = Plot ()
    # plot.lines (np.array (X), Y,
    #             legend          = ("Colab. Vertical", "Colab. Horizontal", "Hierarquia"),
    #             legend_location = "upper right",
    #             xticks_values   = X[0],
    #             xticks_labels   = freshness_list)

    # x = range (n_parameters)
    #
    # print "X", np.array (X)
    # print "Y", np.array (Y)
    # print "x", x


    # fig, ax = plt.subplots()
    # plt.plot(x, Y[0], linestyle="--", marker="o", color="black")
    # plt.plot(x, Y[1], linestyle="--", marker="o", color="blue")
    # plt.plot (x, Y[2], linestyle="--", marker="o", color="red")
    #
    # plt.xticks (range (len (freshness_list)), freshness_list)
    # #plt.ticklabel_format (style="scientific", axis="x")
    # plt.xlabel ("Data Freshness Requeriment")
    # plt.ylabel ("Makespan")
    # plt.title ("Average makespan")
    # plt.legend(('Colab. Vertical', 'Colab. Horizontal', 'Hierarquia'), loc='upper right')
    # plt.show ()
    # plt.xlim(self.minBound[0] * 0.85, self.maxBound[0] * 1.1)
    # plt.ylim(self.minBound[1] * 0.85, self.maxBound[1] * 1.1)
    # for i, point in enumerate (self.centers):
    #     plt.annotate (str (i), (point[0], point[1]))

    # print result
    # links_types = [VERTICAL, LINE_RANDOM]
    # times = 1
    # freshness_values = [1.]
    # results = {}
    # stats = {}
    #
    # for link_type in links_types:
    #     makespanSum = fastSum = slowSum = cloudSum = totalSum = 0
    #     n = float (times)
    #     stats[link_type] = []
    #     i = 0
    #     for  t in xrange (times):
    #         j = 0
    #         for freshness in freshness_values:
    #             path = "{0:6.6s}-{1:03d}-{2:010.8}.pickle".format ( link_type.replace (" ", "_"))
    #             with open(path, 'rb') as handle:
    #                 results[link_type] = pickle.load(handle)
    #             print results[link_type], raw_input ()
    #             makespanSum += float (results[link_type][i + i * j][0])
    #             fastSum += float (results[link_type][i + i * j][1])
    #             slowSum += float (results[link_type][i + i * j][2])
    #             cloudSum += float (results[link_type][i + i * j][3])
    #             j += 1
    #         i+= 1
    #         stats[link_type].append ([makespanSum / n, fastSum / n, slowSum / n, cloudSum / n])
    #
    # print "results", results
    # print "stats", stats
    # fig = plt.figure ()


# def Plot3D (xSet, ySet, zSet, path="plot.pdf"):
# 	fig = plt.figure ()
# 	ax = fig.add_subplot (111, projection="3d")
# 	# for x, y, z in zip (xSet, ySet, zSet):
# 	# 	print x
# 	# 	print y
# 	# 	print z
# 		# ax.plot (x, y, z, color="blue")
#
# 	#ax.plot_wireframe(xSet, ySet, zSet)
# 	ax.plot_surface (xSet, ySet, zSet, cmap=cm.coolwarm)
#
# 	ax.set_title ("Cumulative Distribution for all nodes (ordered by root distance)")
# 	ax.set_xlabel ("Syncrhonization error (microseconds)")
# 	ax.set_ylabel ("Nodes (ordered by root distance)")
# 	ax.set_zlabel ("Cumulative distribution")
# 	ax.view_init(azim=240)
#
# 	fig.savefig (path)
# 	#plt.show ()
