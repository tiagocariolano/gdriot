import sys
import os
import argparse
import numpy as np
from subprocess import Popen, PIPE
from configuration import *

class Experiment:
    def __init__ (self, round_min=0, round_max=10):
        self.args = {}
        self.var_args_names = []
        self.var_args_values = []
        self.format_args = []
        self.command_args = []
        self.round_min = round_min
        self.round_max = round_max
        self.format_rounds = "03d"
        self.call = ""

    def execute_command (self, command):
    	p = Popen (command, shell=True, stdout=PIPE)
    	out, err = p.communicate ()
    	return out[:-1]

    def set_call (self, call):
        self.call = call

    def set_default_args (self, args):
        self.args = args

    def set_format_args (self, format_args):
        self.format_args = format_args

    def set_command_args (self, command_args):
        self.command_args = command_args

    def set_var_args_names (self, var_args_names):
        self.var_args_names = var_args_names

    def set_var_args_values (self, var_args_values):
        self.var_args_values = var_args_values

    def assemble_command (self, val0, val1, round):
        self.args[self.var_args_names[0]] = val0
        self.args[self.var_args_names[1]] = val1

        for name in self.command_args:
            print "teste"
            self.args[name] = self.execute_command (self.command_args[name]["command"])
            print "end teste"

        # for name in self.format_args:
        #     values = []
        #     for arg in self.format_args[name]["args"]:
        #         print "arg", arg
        #         if arg[0] == "name":
        #             values.append (self.var_args_names[arg[1]])
        #         elif arg[0] == "value":
        #             values.append (self.args[arg[0]]["values"][arg[1]])
        #         else:
        #             print "error: assemble_command: does not exist name or value"
        #     print values, raw_input ()

        self.args["outpath"] = self.format_args["outpath"]["format"].format (
            self.var_args_names[0], val0,
            self.var_args_names[1], val1,
            "rounds", round
        )

        args_str = ""
        for key in self.args:
            args_str += " --" + key + " " + str (self.args[key])

        command =  self.call + args_str

        return command

    def run (self):
        for val0 in self.var_args_values[0]:
            for val1 in self.var_args_values[1]:
                for round in xrange (self.round_min, self.round_max):
                    command = self.assemble_command (val0, val1, round)
                    print command
                    out = self.execute_command (command)
                    print out


if __name__ == "__main__":
    parser = argparse.ArgumentParser ()
    parser.add_argument('--results_dict',    type=str,	dest='results_dict',     default=PREFIX_PATH)
    parser.add_argument('--param_name0',     type=str,	dest='param_name0',      default="linktype")
    parser.add_argument('--param_name1',     type=str,	dest='param_name1',      default="reqrate")
    parser.add_argument('--param_values0',   type=str,	dest='param_values0',    nargs="*",  default=[])
    parser.add_argument('--param_values1',   type=str,	dest='param_values1',    nargs="*",  default=[])
    parser.add_argument('--round_min',       type=int,	dest='round_min',        default=0)
    parser.add_argument('--round_max',       type=int,	dest='round_max',        default=10)
    parser.add_argument('--linktype',        type=str,	dest='linktype',         default="ONLY_CLOUD")
    parser.add_argument('--freshness',       type=float,dest='freshness',        default=0)
    parser.add_argument('--nedges',          type=int,	dest='nedges',           default=10)
    parser.add_argument('--reqrate',         type=float,	dest='reqrate',      default=0.001)
    parser.add_argument('--nrequest',        type=int,	dest='nrequest',         default=10)

    args = parser.parse_args ()

    if args.param_values0:
        PARAMS[args.param_name0]["values"] = args.params_values0
        PARAMS[args.param_name0]["length"] = len (args.params_values0)
        PARAMS[args.param_name0]["labels"] = tuple (args.params_values0)

    if args.param_values1:
        PARAMS[args.param_name1]["values"] = args.params_values1
        PARAMS[args.param_name1]["length"] = len (args.params_values1)
        PARAMS[args.param_name1]["labels"] = tuple (args.params_values1)

    PARAMS["round"]["values"] = range (args.round_min, args.round_max)
    PARAMS["round"]["length"] = len (PARAMS["round"]["values"])
    PARAMS["round"]["labels"] = tuple (PARAMS["round"]["values"])

    prefix_path         = args.results_dict
    round_min           = args.round_min
    round_max           = args.round_max
    params_names        = [args.param_name0, args.param_name1]
    params_values       = [PARAMS[params_names[0]]["value"], PARAMS[params_names[1]]["value"]]
    params_lengths      = [PARAMS[params_names[0]]["length"], PARAMS[params_names[1]]["length"]]
    params_formats      = [PARAMS[params_names[0]]["format"], PARAMS[params_names[1]]["format"]]
    params_labels       = [PARAMS[params_names[0]]["label"], PARAMS[params_names[1]]["label"]]

    format_args = {
                    "outpath": {
                        "format":   prefix_path + "/" +
                                    "{:6.6s}-{:" + PARAMS[params_names[0]]["format"] + "}-" \
                                    "{:6.6s}-{:" + PARAMS[params_names[1]]["format"] + "}-" \
                                    "{:6.6s}-{:" + PARAMS["round"]["format"] + "}.pickle",
                        "args":     [["name", 0], ["value", 0],
                                     ["name", 1], ["value", 1],
                                     ["round", 0], ["round", 2]]
                    },
    }
    command_args = {
                    "seed": {
                        "command": "echo $(od -An -N4 -i /dev/urandom) | sed 's/\-//'"
                    }
    }

    default_args["linktype"] = args.linktype
    default_args["freshness"] = args.freshness
    default_args["nedges"] = args.nedges
    default_args["reqrate"] = args.reqrate
    default_args["nrequest"] = args.nrequest

    exp = Experiment (round_min=round_min, round_max=round_max)
    exp.set_call ("python -m resource_allocator_algorithm")
    exp.set_default_args (default_args)
    exp.set_command_args (command_args)
    exp.set_var_args_names (params_names)
    exp.set_var_args_values (params_values)
    exp.set_format_args (format_args)
    exp.run ()
