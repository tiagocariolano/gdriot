import random
import numpy as np
import simpy

probe_id = 0

def next_probe_id ():
	global probe_id 
	probe_id += 1
	return probe_id

class Event:
	START_VN = 'START_VN'
	FINISHED_VN = 'FINISHED_VN'
	ARRIVED_MSG = 'ARRIVED_MSG'
	ARRIVED_PROBE = 'ARRIVED_PROBE'
	ARRIVED_DATA = 'ARRIVED_DATA'
	ARRIVED_MSG_CLOUD = 'ARRIVED_MSG_CLOUD'
	ARRIVED_DATA_CLOUD = 'ARRIVED_DATA_CLOUD'
	
	def __init__ (self, type, obj):
		self.type = type
		self.obj = obj

class Log:
	def __init__ (self,log_path="log.txt", debug =True, debug_log=False):
		self._debug = debug
		self._debug_log = debug_log
		self._f = open (log_path, "w")
		self.c = 0
		self.lambda_function = None

	def set_debug (self, debug):
		self._debug = debug

	def log (self, *str_list):
		str_file = ""
		if self._debug or self._debug_log:
			for s in str_list:
				str_file += s.__str__ ()

		if self._debug:
			print str_file
		if self._debug_log:
			self._f.write (str_file + "\n")


	def error (self, str):
		print "Error: " + str + "."
		exit ()

	def count (self):
		self.c = self.c + 1
		if self.c % 100 == 0:
			print "Count: ", self.c

	def __del__ (self):
		self._f.close ()

## enviroment of simpy to control some events
#env = simpy.Environment ()

GB = GHz = 10 ** 9
MB = MHz = 10 ** 6
KB = KHz = 10 ** 3

## time max of simulation
SIMULATION_TIME = 1000000		# Simulation time

## Log output
log = Log ("./results/output.txt", debug=False, debug_log=False)

log2 = Log ("./results/output2.txt", debug=False, debug_log=False)

log3 = Log ("./results/output3.txt", debug=False)

DEBUG = False
STEP_BY_STEP = False


TOT_REQ = 0
FINISHED_REQ = 0
