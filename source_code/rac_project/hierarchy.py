import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import random
import json
import sys
from math import sqrt, log
import util


class Hierarchy:
  INF = sys.maxint

  def __init__ (self, masterList=[], typedist="euclidean"):
    self.masterList = masterList
    self.typedist = typedist
    self.n = -1
    self.adj = []
    self.M = []
    self.links = []
    self.fig = None
    self.parents = []
    self.parents2 = []
    self.parents3 = []
    self.roots = []
    self.pars = []
    self.test = []

  def set_coords (self, coords):
    self.edgesCoord = coords
    self.n = len (self.edgesCoord)

  def set_links (self, links):
    self.links = links
    self.adj = [[0 for x in range(self.n)] for y in range(self.n)]
    for link in links:
      self.adj[link["s"]][link["d"]] = 1
      self.adj[link["d"]][link["s"]] = 1

  def read_coords (self, path="coords.csv"):
    f = open (path, "r")
    data = f.readlines ()

    self.cloudCoord = map(int, data.pop (0)[:-1].split(", "))
    self.eMasterCoord = []

    self.edgesCoord = []
    for l in data:
      self.edgesCoord.append (map (int, l[:-1].split (", "))) # [:-1] to remove newline character, map convert string to int

    f.close ()
    return self.edgesCoord

  def read_links (self, path):
    self.adj = [[0 for x in range(n)] for y in range(n)]
    with open(path, "r") as read_file:
      data = json.load(read_file)

    for link in data:
      self.adj[link["s"]][link["d"]] = 1
      self.adj[link["d"]][link["s"]] = 1 # if problem is assymetric

  def write_hierarchy (self, path):

    f = open (path, "w")
    f.write (str(self.cloudCoord[0]) + ", " + str(self.cloudCoord[1]) + ", 0\n")
    f.write (str(self.eMasterCoord[0]) + ", " + str(self.eMasterCoord[1]) + ", 1\n")
    for c in self.edgesCoord:
      if c[0] != self.eMasterCoord[0] or c[1] != self.eMasterCoord[1]:
        f.write (str(c[0]) + ", " + str(c[1]) + ", 2\n")

    f.close ()

  def distance (self, c1, c2):
    return sqrt ((c1[0] - c2[0]) ** 2 + (c1[1] - c2[1]) ** 2)

  def hierarchy(self):
    n = len (self.edgesCoord)
    inf = self.INF # Does this look like infinity?
    apsp = [[0 for x in range(n)] for y in range(n)]

    #print "Apsp Inicialization..."
    for i in range (n):
      for j in range (n):
        if i == j:
          apsp[i][j] = 0
        elif self.adj[i][j] > 0 :
          apsp[i][j] = self.distance (self.edgesCoord[i], self.edgesCoord[j]) #or self.adj[i][j]
          if self.typedist == "hop":
            apsp[i][j] = 1
          #apsp[i][j] =  self.adj[i][j]
        else:
          apsp[i][j] = inf
    #print_format_matrix (apsp), raw_input ()

    #print "Apsp distances..."
    # self.masterList = [0, 1, 2]

    r = -1
    minmax = inf
    M = [0 for x in range(n)]
    # self.parents2 = [-1 for i in range (n)]
    # self.roots = [-1 for i in range (n)]
    # self.pars = [-1 for i in range (n)]
    for k in range (n):
      for i in range (n):
        max = 0
        min_master = self.INF
        # min = self.INF
        for j in range (n):
          if apsp[i][k] + apsp[k][j] < apsp[i][j]:
            apsp[i][j] = apsp[i][k] + apsp[k][j]
          if apsp[i][j] > max:
            max = apsp[i][j]
          if j in self.masterList and apsp[i][j] < min_master:
              min_master = apsp[i][j]
              # self.roots[i] = j

        # if i in self.masterList:
        #     self.pars[i] = i
        # else:
        #     min = self.INF
        #     for j in range (n):
        #         if self.adj[i][j] > 0 and self.roots[i] == self.roots[j] and apsp[j][self.roots[i]] < min:
        #             min = apsp[j][self.roots[i]]
        #             self.pars[i] = j

        # min_master = self.INF
        # for j in range (n):
        #   if j in self.masterList and apsp[i][j] < min_master:
        #       min_master = apsp[i][j]
        #       self.roots[i] = j
        # min = self.INF
        # for j in range (n):
          # if j in self.masterList and apsp[i][j] < min_master:
          #     min_master = apsp[i][j]
          #     self.roots[i] = j
          # if i in self.masterList:
          #     self.pars[i] = -1
          #     break
          # if self.adj[i][j] > 0 and j in self.masterList:
          #     self.pars[i] = j
          #     break

          # if self.adj[i][j] > 0 and self.roots[i] == self.roots[j] and apsp[j][self.roots[i]] < min:
          #     min = apsp[j][self.roots[i]]
          #     self.pars[i] = j

        # min = self.INF
        # for j in range (n):
        #   if i in self.masterList:
        #       self.pars[i] = -1
        #       break
        #   if self.adj[i][j] > 0 and j in self.masterList:
        #       self.pars[i] = j
        #       break
        #   if self.adj[i][j] > 0 and self.roots[i] == self.roots[j] and apsp[i][j] < min:
        #       min = apsp[i][j]
        #       self.pars[i] = j

        min = self.INF
        for j in self.masterList:
            if apsp[i][j] < min:
                min = apsp[i][j]
                self.parents2[i] = j

        M[i] = max
        if M[i] <  minmax:
          minmax = M[i]
          r = i


    # for i in range (n):
    #     if i in self.masterList:
    #         self.pars[i] = i
    #     else:
    #         min = self.INF
    #         for j in range (n):
    #             if self.adj[i][j] > 0 and self.roots[i] == self.roots[j] and apsp[j][self.roots[i]] < min:
    #                 min = apsp[j][self.roots[i]]
    #                 self.pars[i] = j
    # self.parents3 = [-1 for i in range (n)]
    # for i in range (n):
    #     min = self.INF
    #     #print "i", i
    #     for j in range (n):
    #         #print "j", j, apsp[i][j], self.adj[i][j]
    #         if self.adj[i][j] > 0 and j in self.masterList:
    #             self.parents3[i] = j
    #             break
    #
    #         if self.adj[i][j] > 0 and self.parents2[i] == self.parents2[j] and apsp[i][j] < min:
    #             min = apsp[i][j]
    #             self.parents3[i] = j
    #print "Apsp sorting..."
    S = sorted(range(len(M)), key=lambda k: M[k])
    self.r = S[0]

    # self.parents = [0 for i in xrange (n)]

    if self.masterList == []:
      self.masterList = [self.r]
      self.eMasterCoord = self.edgesCoord[S[0]]
    else:
      for i in range (n):
        if i in self.masterList:
          M[i] = 0
          # self.parents[i] = -1
        else:
          min = self.INF
          for j in self.masterList:
            if apsp[i][j] < min:
              min = apsp[i][j]
              # self.parents[i] = j
          M[i] = min

      for m in self.masterList:
        self.eMasterCoord = self.edgesCoord[m]
    self.M = M
    # self.test = apsp

    return apsp, M

  def plot_setup (self):
    x = [c[0] for c in self.edgesCoord]
    y = [c[1] for c in self.edgesCoord]
    z = [m ** 2 for m in self.M]

    cmap = plt.get_cmap('jet', 20)
    cmap.set_under('gray')

    self.fig, ax = plt.subplots()
    cax = ax.scatter(x, y, c=z, s=100, cmap=cmap, vmin=0, vmax=max(z))
    self.fig.colorbar(cax, extend='min')

    i = 0
    for l in self.links:
      x = [self.edgesCoord[l['s']][0], self.edgesCoord[l['d']][0]]
      y = [self.edgesCoord[l['s']][1], self.edgesCoord[l['d']][1]]
      plt.plot (x, y, color='black')
      # plt.arrow (x[0], y[0], x[1], y[1], color='black')
      # print x, y, raw_input ()

    for i, coord in enumerate (self.edgesCoord):
        posx = coord[0] + 50
        posy = coord[1]
        print i, posx, posy
        t = ""
        if i in self.masterList:
            t  += "M"
        if i == self.r:
            t += "X"
        plt.annotate (t + str (i + 2), (posx, posy))

    plt.ylabel ("Area Y")
    plt.xlabel ("Area X")
    #plt.annotate ("X", (self.edgesCoord[self.r][0], self.edgesCoord[self.r][1] * 1.05), weight="bold")
    #plt.show()

  def plot_save (self, path):
    if self.fig is not None:
      self.fig.savefig (path)
      plt.close (self.fig)

    print "M       : ", self.M
    # print "parents : ", self.parents
    # print "parents2: ", self.parents2
    # print "roots   : ", self.roots
    # print "parents3: ", self.parents3
    # print "pars    : ", self.pars
    # for i in range (self.n):
    #     print "node ", i, ": *********************************"
    #     print "neighs          : ",
    #     for j in range (self.n):
    #         if self.adj[i][j] > 0:
    #             print "{:02d}".format (j), "->", "{:010.4f}".format (self.test[j][self.roots[i]]),
    #     print ""
    #     print "root            : ", "{:02d}".format (self.roots[i])
    #     print "d (", "{:02d}".format (i), ",", "{:02d}".format (self.roots[i]),")   : ", "{:010.4f}".format (self.test[i][self.roots[i]])
    #     print "parents         :"
    #     parent = self.pars[i]
    #     print parent, ", ",
    #     count = 0
    #     while parent not in self.masterList:
    #         parent = self.pars[parent]
    #         print parent, ", ",
    #         count += 1
    #         if count > self.n:
    #             print "error: loop!",
    #             exit (1)
    #     print "\n"

def gen_coords_and_links (type="randomc", n=20, xMax=100000, yMax=100000, customCoords=[], path="./coords.csv"):
  if type == "randomc":
    G=nx.geographical_threshold_graph(n,n + log (n, 10))
    pos=nx.get_node_attributes(G,'pos')
    components = [c for c in sorted (nx.connected_components (G), key=len)]
    while len (components) > 1:
        for c in components:
            isoNode = util.random.choice (list (c))
            minDist = yMax
            minNode = None
            for node in G.nodes ():
                if node not in c and not G.has_edge (isoNode, node):
                    dist = sqrt( (pos[node][0] - pos[isoNode][0]) ** 2 + (pos[node][1] - pos[isoNode][1]) ** 2 )
                    if dist < minDist:
                        minDist = dist
                        minNode = node
        if minNode is not None:
          G.add_edge (isoNode, minNode)
        components = [c for c in sorted (nx.connected_components (G), key=len)]
    exp = log (yMax, 10)

    links = [{'s' : e[0], 'd' : e[1]} for e in G.edges ()]
    coords = [[p[0] * 10 ** exp, p[1] * 10 ** exp] for p in pos.values ()]
  elif type == "Line":
    coords = []
    halfY = yMax / 2
    for i in range (0, n):
      coords.append ([i, halfY])

    links = []
    for i in range (0, n):
      if i > 0 and i < n:
        links.append ({'s' : i - 1, 'd' : i})
  elif type == "Line Random":
    coords = []
    halfY = yMax / 2
    for i in range (0, n):
      y = util.random.uniform (halfY * 0.9, halfY * 1.1)
      coords.append ([i, y])

    links = []
    for i in range (0, n):
      if i > 0 and i < n:
        links.append ({'s' : i - 1, 'd' : i})
  elif type == "grid":
    coords = []
    for i in range (0, xMax):
      for j in range (0, yMax):
        coords.append ([j, i])

    links = []
    for i in range (n):
      if int (i / y) == int ((i + 1) / y ):
        links.append ({'s' : i, 'd' : i + 1})
      # print i, i + 1
      if i + y < n:
        links.append ({'s' : i, 'd' : i + y })
      # print i, i + x + 1

  return coords, links, G

def gen_coords (type="random", n=10, xMax=5, yMax=5, customCoords=[], path="./coords.csv"):
  coords = []
  if type == "random":
    for i in range (0, n):
      coords.append ([util.random.uniform (0, xMax), util.random.uniform (0, yMax)])
  elif type ==  "grid":
    for i in range (0, xMax):
      for j in range (0, yMax):
        coords.append ([j, i])
  elif type == "Line":
    halfY = yMax / 2
    for i in range (0, n):
      coords.append ([i, halfY])
  elif type == "Line Random":
    halfY = yMax / 2
    for i in range (0, n):
      y = util.random.uniform (halfY * 0.9, halfY * 1.1)
      coords.append ([i, y])

  if customCoords:
    coords = [customCoords] + coords

  f = open (path, "w")
  for c in coords:
    f.write (str (c[0]) + ", " + str(c[1]) + "\n")
  f.close ()

  return coords

def gen_coords_links_by_level (nLevel=2, nStart=2, step=2, maxRounds=100, nnodes=None, type="randomc", xMax=100000, yMax=100000):
    maxRoundsGuard = 500
    coords = None
    links = None
    found = False
    count = 0
    n = nStart
    i = 1
    if nnodes:
        nLevel = 1

    while True:
        if nnodes:
            n = nnodes

        coords, links, G = gen_coords_and_links (type="randomc", n=n, xMax=xMax, yMax=yMax)

        h = Hierarchy ()
        h.set_coords(coords)
        h.set_links (links)
        apsp, M = h.hierarchy ()

        lens = nx.single_source_shortest_path_length (G, h.r)


        print "Attempt", i, n, nLevel, "=>", max (lens.values())
        if nnodes:
            if max (lens.values ()) == nLevel and len (G) == nnodes:
                h.plot_setup ()
                h.plot_save ("hiearchy.pdf")
                break
            nLevel += 1
            if nLevel > nnodes / 2:
                nLevel = 0
            continue


        if max (lens.values ()) == nLevel:
            if nnodes and len (G) == nnodes:
                h.plot_setup ()
                h.plot_save ("hiearchy.pdf")
                break
            elif nnodes and len (G) != nnodes:
                found = False
                count = 0
                n = nStart
                i = 1
                continue

        n = n + step
        i = i + 1

        if i > maxRounds:
            i = 1
            n = 2
            maxRounds = maxRounds + 100

        if maxRounds > maxRoundsGuard:
            print "WARNING: gen_coords_links_by_level: Max rounds Guard trigged!!!"
            break

    links = map(lambda x: (x["s"], x["d"]), links)

    return coords, links, n, M

def gen_links (n=4, x=2, y=2, type="Line", path=""):
  links = []
  if type == "Line" or type == "Line Random":
    for i in range (0, n):
      if i > 0 and i < n:
        links.append ({'s' : i - 1, 'd' : i})
  elif type == "grid":
    for i in range (n):
      if int (i / y) == int ((i + 1) / y ):
        links.append ({'s' : i, 'd' : i + 1})
      # print i, i + 1
      if i + y < n:
        links.append ({'s' : i, 'd' : i + y })
      # print i, i + x + 1

  if path != "":
    with open(path, 'w') as outfile:
      json.dump(links, outfile)
  return links

def print_format_matrix (m):
  for l in m:
    for c in l:
      print "%03i," % c,
    print ""

if __name__ == '__main__':
  xMax = 5
  yMax = 5
  n = xMax * yMax

  coords = gen_coords ("grid", n=n, xMax=xMax, yMax=yMax, customCoords=[0,3], path="coords.csv")
  links = gen_topo_links (type="grid", n=n, x=xMax, y=yMax, path="links.json")


  print "Solving apsp..."

  h = Hierarchy ()

  h.read_coords ("coords.csv")
  h.read_links ("links.json")
  apsp, M = h.hierarchy ()
  print M
  h.write_hierarchy ("res.hierarchy.txt")

  coords.pop (0)
  n = len (coords)

  #print M
  x = [c[0] for c in coords]
  y = [c[1] for c in coords]
  z = [m ** 3 for m in M]
  #z = [-1] + [m ** 3 for m in M]
  #print x
  #print y
  #print z
  #print n
  #print coords

  cmap = plt.get_cmap('jet', 20)
  cmap.set_under('gray')

  fig, ax = plt.subplots()
  cax = ax.scatter(x, y, c=z, s=100, cmap=cmap, vmin=0, vmax=max(z))
  fig.colorbar(cax, extend='min')

  i = 0
  #plt.plot ([0,1], [0, 0], color='black')
  for l in links:
    #print l, coords[l['s']], coords[l['d']]
    x = [coords[l['s']][0], coords[l['d']][0]]
    y = [coords[l['s']][1], coords[l['d']][1]]
    #print l, x, y
    #plt.plot (coords[l['s']], coords[l['d']])
    plt.plot (x, y, color='black')
    #if i >= 0:
    # break
    #i = i + 1

  # for i in range (n):
  #   for j in range (n):
  #     if (i != j and h.adj[i][j] == 1):
  #       plt.plot ([coords[i][0], coords[j][0]], [coords[i][1], coords[j][1]], color='black')
  plt.ylabel ("Area Y")
  plt.xlabel ("Area X")
  plt.show()

  #nx.draw(G, with_labels=False, node_color=np.arange (0.0, 5.0, 0.1))
  #plt.show ()
  #~ G = nx.convert_matrix.from_numpy_matrix (np.asmatrix ([[1, 2, 3], [4, 5, 6], [7,8,9]]))
  #~ plt.subplot(121)
  #~ nx.draw(G, with_labels=True, font_weight='bold')
  #~ plt.subplot(122)
  #~ nx.draw_shell(G, nlist=[range(5, 10), range(5)], with_labels=True, font_weight='bold')
  #~ plt.show()

  # Generate some data
  #x, y, z = np.random.random((3, 30))
  #z = z * 20 + 0.1
  # Set some values in z to 0...
  #z[:5] = 0
  #z = np.arange (0, 30, 1)

  # def gen_adj_grid (nrow=3, ncol=3, val=1):
  #   n = nrow*ncol
  #   adj = [[0 for x in range(n)] for y in range(n)]
  #   for r in xrange(nrow):
  #     for c in xrange(ncol):
  #       i = r*ncol + c
  #       # Two inner diagonals
  #       if c > 0: adj[i - 1][i] = adj[i][i - 1] = val
  #       # Two outer diagonals
  #       if r > 0: adj[i - ncol][i] = adj[i][i - ncol] = val
  #
  #   return adj
