import numpy as np
import matplotlib.pyplot as plt
import matplotlib.path as mpltPath
import util
from scipy.spatial import Voronoi

class VoronoiEMCNew:
    def __init__ (self, points, xMin=0, xMax=10000, yMin=0, yMax=10000):
        if len (points) < 3 or self.check_planar (points):
            print "Euclidean"
            self.isPlanar = True
            self.centers = points
            self.vor = None
            self.regions = []
            self.vertices = []
            if len (points) == 2:
                xMin, yMin, xMax, yMax = self.get_min_and_max (points)
            self.minBound = [xMin, yMin]
            self.maxBound = [xMax, yMax]
            self.fig = None
        else:
            # print "Voronoi"
            self.isPlanar = False
            self.centers = points
            self.vor = Voronoi(points)
            self.regions, self.vertices = self.voronoi_finite_polygons_2d ()
            self.minBound = self.vor.min_bound
            self.maxBound = self.vor.max_bound
            self.fig = None

    def check_planar (self, points):
        flag0 = True
        flag1 = True
        for i in xrange (len (points) -  1):
            if points[i][0] != points[i + 1][0]:
                flag0 = False
            if points[i][1] != points[i + 1][1]:
                flag1 = False
        return flag0 or flag1

    def get_min_and_max (self, points):
        xList = []
        yList = []
        for c in self.centers:
            xList.append (c[0])
            yList.append (c[1])

        return min (xList), min (yList), max (xList), max (yList)

    def distance (self, c1, c2):
        # sometimes the value is very high, but not invalidates the code.
        np.seterr (over='ignore')
        np.seterr (invalid='ignore')
        dist = np.sqrt ((c1[0] - c2[0]) ** 2 + (c1[1] - c2[1]) ** 2)
        np.seterr (over='warn')
        np.seterr (invalid='warn')

        return dist

    def search_closest_point (self, newPoint):
        minDist = 99999999
        index = 0
        region = [0]
        for i, point in enumerate (self.centers):
            if self.distance (newPoint, point) < minDist:
                minDist = self.distance (newPoint, point)
                index = i
                region = i
        return index, [region]

    def voronoi_finite_polygons_2d(self, radius=None):
        if self.vor.points.shape[1] != 2:
            raise ValueError("Requires 2D input")

        new_regions = []
        new_vertices = self.vor.vertices.tolist()

        center = self.vor.points.mean(axis=0)
        if radius is None:
            radius = self.vor.points.ptp().max()

        # Construct a map containing all ridges for a given point
        all_ridges = {}
        for (p1, p2), (v1, v2) in zip(self.vor.ridge_points, self.vor.ridge_vertices):
            all_ridges.setdefault(p1, []).append((p2, v1, v2))
            all_ridges.setdefault(p2, []).append((p1, v1, v2))

        # Reconstruct infinite regions
        for p1, region in enumerate(self.vor.point_region):
            vertices = self.vor.regions[region]

            if all(v >= 0 for v in vertices):
                # finite region
                new_regions.append(vertices)
                continue

            # reconstruct a non-finite region
            ridges = all_ridges[p1]
            new_region = [v for v in vertices if v >= 0]

            for p2, v1, v2 in ridges:
                if v2 < 0:
                    v1, v2 = v2, v1
                if v1 >= 0:
                    # finite ridge: already in the region
                    continue

                # Compute the missing endpoint of an infinite ridge

                t = self.vor.points[p2] - self.vor.points[p1] # tangent
                t /= np.linalg.norm(t)
                n = np.array([-t[1], t[0]])  # normal

                midpoint = self.vor.points[[p1, p2]].mean(axis=0)
                direction = np.sign(np.dot(midpoint - center, n)) * n
                far_point = self.vor.vertices[v2] + direction * radius

                new_region.append(len(new_vertices))
                new_vertices.append(far_point.tolist())

            # sort region counterclockwise
            vs = np.asarray([new_vertices[v] for v in new_region])
            c = vs.mean(axis=0)
            angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
            new_region = np.array(new_region)[np.argsort(angles)]

            # finish
            new_regions.append(new_region.tolist())

        return new_regions, np.asarray(new_vertices)

    def get_region_by_point (self, newPoint):
        if len (self.centers) < 3 or self.check_planar (self.centers):
            i, region = self.search_closest_point (newPoint)
            return i, region
        else:
            if self.isPlanar:
                i, region = self.search_closest_point (newPoint)
                return i, region

            for i, region in enumerate (self.regions):
                path = mpltPath.Path(self.vertices[region])
                if path.contains_points ([newPoint])[0]:
                    return i, region
            i, region = self.search_closest_point (newPoint)
            return i, region

        print "Error: Voronoi.get_region_by_point: No region found"
        #exit ()
        #return False, None

    def plot_setup (self):
        self.fig, ax = plt.subplots()
        for region in self.regions:
            polygon = self.vertices[region]
            plt.fill(*zip(*polygon), alpha=0.4)
        plt.plot(self.centers[:,0], self.centers[:,1], 'ko')
        plt.xlim(self.minBound[0] * 0.85, self.maxBound[0] * 1.1)
        plt.ylim(self.minBound[1] * 0.85, self.maxBound[1] * 1.1)
        for i, point in enumerate (self.centers):
            plt.annotate (str (i), (point[0], point[1]))

    def plot_add_point (self, point):
        plt.plot (point[0], point[1], '*')

    def plot_show (self):
        plt.show()

    def plot_save (self, path):
        if self.fig is not None:
            self.fig.savefig (path)

if __name__ == "__main__":
    #util.np.random.seed (6)
    points = util.np.random.uniform(0, 10000, (1,2))
    #points = util.np.random.rand(1, 2)
    #points = util.np.array ([[0, 5000], [1000, 5000], [2000, 5000], [3000, 5000]])
    vor = VoronoiEMCNew (points)

    newPoint = [util.np.random.uniform(low=vor.minBound[0], high=vor.maxBound[0]),
                 util.np.random.uniform(low=vor.minBound[1], high=vor.maxBound[1])]
    # print points
    # print newPoint
    # print vor.minBound[0]
    # print vor.maxBound[0]
    #print vor.vertices
    #print vor.regions

    print vor.get_region_by_point (newPoint)

    vor.plot_setup ()
    vor.plot_add_point (newPoint)
    vor.plot_show ()

# # make up data points
# #util.np.random.seed(1234)
# points = util.np.random.rand(5, 2)
#
# # compute Voronoi tesselation
# vor = Voronoi(points)
#
# # plot
# regions, vertices = voronoi_finite_polygons_2d(vor)
# print "--"
# print "regions", regions
# print "points", points
# print "--"
# print "vertices", vertices
# print "bounds"
# print vor.min_bound
# print vor.max_bound
#
# newPoint = [np.random.uniform(low=vor.min_bound[0], high=vor.max_bound[0]),
#             np.random.uniform(low=vor.min_bound[1], high=vor.max_bound[1])]
#
# #newPoint = np.asarray ([0.5, 0.5])
# #polygon = [[0,0],[0,2],[1,2], [1,0]]
# #path = mpltPath.Path (polygon)
# #print path.contains_points ([[0.5, 0.5]])
# #plt.fill (*zip(*polygon))
# #path.contains_points (np.asarray (newPoint))
# for region in regions:
#     path = mpltPath.Path(vertices[region])
#     print path.contains_points ([newPoint])
#
# # colorize
# for region in regions:
#     polygon = vertices[region]
#     plt.fill(*zip(*polygon), alpha=0.4)
#
# for i, point in enumerate (points):
#     plt.annotate (str (i), (point[0], point[1]))
#
# #for i, vertex in enumerate (vertices):
# #    plt.annotate (str (i), (vertex[0], vertex[1]))
#
# plt.plot(points[:,0], points[:,1], 'ko')
# plt.plot (newPoint[0], newPoint[1], '*')
# plt.xlim(vor.min_bound[0] - 0.1, vor.max_bound[0] + 0.1)
# plt.ylim(vor.min_bound[1] - 0.1, vor.max_bound[1] + 0.1)
# plt.show()
