from sklearn.datasets import load_boston
import matplotlib.pyplot as plt
import pandas as pd

boston = load_boston()

plt.figure(figsize=(5, 4))
plt.hist(boston.target)
plt.title('Histograma dos preços das casas de Boston')
plt.xlabel('Preço ($1000s)')
plt.ylabel('Quantidade')
plt.show()

df = pd.DataFrame (boston.data, columns = boston.feature_names)
df['target'] = boston.target
print (df.head ())
print (boston.DESCR)
# # Imports 
# from sklearn.datasets import load_iris
# import pandas as pd

# # Load Data
# iris = load_iris()

# # Create a dataframe
# df = pd.DataFrame(iris.data, columns = iris.feature_names)
# df['target'] = iris.target
# X = iris.data
# df.sample(4)

# from sklearn.datasets import load_digits
# digits = load_digits()

# from matplotlib import pyplot as plt
# fig = plt.figure(figsize=(6, 6))  # figure size in inches
# fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05)

# for i in range(64):
#     ax = fig.add_subplot(8, 8, i + 1, xticks=[], yticks=[])
#     ax.imshow(digits.images[i], cmap=plt.cm.binary, interpolation='nearest')
#     # label the image with the target value
#     ax.text(0, 7, str(digits.target[i]))

# plt.show ()