#!/bin/bash
export LC_NUMERIC="en_US.UTF-8"
size_seed=4
prefix="./results/"
min_rounds=21
max_rounds=22

declare -A var_name
var_name[reqrate]=0
var_name[freshness]=1
var_name[nedge]=2
var_name[ndatatype]=3
var_name[nrequest]=4
var_name[strategy]=5

# SET VALUE HERE:
index=${var_name[nrequest]}
declare -a default_names=("reqrate" "freshness" "nedge" "ndatatype" "nrequest" "strategy")
declare -a default_formats=("06.3f" "05.2f"     "03d"   "03d"       "04d" "s")
# declare -a default_values=(0.01 1 8 8 1000 'rac')
declare -a default_values=(0.01 1 8 8 1000 'rac')

# declare -a reqrates=(0.01 0.03 0.05 0.07 0.09)
declare -a reqrates=(0.001 0.003 0.005 0.007 0.009)
declare -a freshnesss=(0 1 2 3 4)
declare -a nedges=(4 6 8 10 12)
declare -a ndatatypes=(2 4 8 16)
declare -a nrequests=(200 400 600 800 1000)
declare -a strategies=("vertical" "edge" "rac" )

declare -A var_values

var_values[0]=${reqrates[@]}
var_values[1]=${freshnesss[@]}
var_values[2]=${nedges[@]}
var_values[3]=${ndatatypes[@]}
var_values[4]=${nrequests[@]}
var_values[5]=${strategies[@]}

declare -a x_name=${default_names[$index]}
declare -a x_format=${default_formats[$index]}
x_values=( ${var_values[$index]} )

declare -a param_name="strategy"
declare -a param_format="s"
param_values=( "${strategies[@]}" )

rounds=$(seq $min_rounds 1 $((max_rounds - 1)))

var_name=${vars_names[0]}


for x in ${!x_values[@]}; do

for r in ${rounds[@]}; do

seed=$(od -A n -t u$size_seed -N $size_seed  /dev/urandom)

for p in ${!param_values[@]}; do


s=""
out=""
for i in ${!default_names[@]}; do

  if [ "${default_names[$i]}" = "$x_name" ]; then
   
    s="$s""$(printf "%s%s%s%$x_format" "--" "$x_name" " " "${x_values[$x]}") "
    out="$out""$(printf "%.4s%.4s%.4s%$x_format" "-" "$x_name" "-" "${x_values[$x]}")"
    continue

  fi

  if [ "${default_names[$i]}" = "$param_name" ]; then

    s="$s""$(printf "%s%s%s%$param_format" "--" "$param_name" " " "${param_values[$p]}") "
    out="$out""$(printf "%.4s%.4s%.4s%$param_format" "-" "$param_name" "-" "${param_values[$p]}")"
    continue

  fi
 
  s="$s""$(printf "%s%s%s%${default_formats[$i]}" "--" "${default_names[$i]}" " " "${default_values[$i]}") "
#  out="$out""$(printf "%.4s%.4s%.4s%${default_formats[$i]}" "-" "${default_names[$i]}" "-" "${default_values[$i]}")"

done
out="$out""$(printf "%.4s%.4s%.4s%03d" "-" "round" "-" "$r")"
out=$(echo $(echo $out | sed "s/^\-//"))
out=$prefix$out".pickle"

command=$(echo "python -m resource_allocator_algorithm " " --seed" "$seed " $s " --outpath " $out)
echo $out
echo $command
time $command

done

done

done
#for x  in ${x_values[@]}; do

#  seed=$(od -A n -t u$size_seed -N $size_seed  /dev/urandom)

#  for param in ${param_values[@]}; do

#    format="%s-%"${params_formats[0]}"-%s-%"${params_formats[1]}"-%s-%"${params_formats[2]}
#    args="$x $xs_names[0]$ ${params_names[1]} $param1 ${params_names[2]} $param2"

#    params_str=""

#    for i in ${!default_names[@]}; do

#      printf $i, ${default_names[$i]}
#      if [ "$default_name" = "$x_name" ]; then
#        
#        params_str="$params_str --$default_name $default_values"
#       
#      fi
#
#        echo time python -m resource_allocator_algorithm \
#              --nrequests       $param1 \
#              --opt             $param0 \
#              --seed            $seed \
#              --outpath         $out_path \
#              --ndatatypes      $dt \
#              --kappa           $kappa \
#              --npsan           $npsan \
#              --reqrate         $reqrate \
#              --nedges          $nedge 


#    done
#  done
#done




#for dt in ${ndatatypes[@]}; do
#
#out_folder=""
#out_folder="$out_folder$prefix"
#out_folder="$out_folder"npsan-$(printf "%02d" $npsan)-""
#out_folder="$out_folder"ndt-$(printf "%02d" $dt)-""
#out_folder="$out_folder"kappa-$(printf "%02d" $kappa)-""
#out_folder="$out_folder"nedge-$(printf "%02d" $nedge)-""
#out_folder="$out_folder"reqrate-$(printf "%05.3f" $reqrate)""
#out_folder="$out_folder""/"
#echo $out_folder
#
#mkdir $out_folder
#
#
#for param2 in ${params2[@]}; do
#for param1  in ${params1[@]}; do
#seed=$(od -A n -t u$size_seed -N $size_seed  /dev/urandom)
#for param0 in ${params0[@]}; do
#format="%s-%"${params_formats[0]}"-%s-%"${params_formats[1]}"-%s-%"${params_formats[2]}
#args="${params_names[0]} $param0 ${params_names[1]} $param1 ${params_names[2]} $param2"
#
#out_path=$out_folder$(printf "$format" $args)".pickle"
##echo $out_path
#
#echo time python -m resource_allocator_algorithm \
#              --nrequests       $param1 \
#              --opt             $param0 \
#              --seed            $seed \
#              --outpath         $out_path \
#              --ndatatypes      $dt \
#              --kappa           $kappa \
#              --npsan           $npsan \
#              --reqrate         $reqrate \
#              --nedges          $nedge 
#
#time python -m resource_allocator_algorithm \
#              --nrequests       $param1 \
#              --opt             $param0 \
#              --seed            $seed \
#              --outpath         $out_path \
#              --ndatatypes      $dt \
#              --kappa           $kappa \
#              --npsan           $npsan \
#              --reqrate         $reqrate \
#              --nedges          $nedge 
#
#
##echo $format
##echo $args
##echo $out_path
##              --nrequests       $param0 
##              --seed            $(od -An -N$size_seed -i /dev/random) \
##              --n_edges         4 \
##              --link_type       $param0 \
##              --freshness       $param1 \
##              --req_rate        1 \
##read -p ""
#done
#
#done
#
#done
#
#done
