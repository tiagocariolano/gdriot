class DataType:

	def __init__(self, type, procCycle, fusCycle, upCycle):
		self.type = type
		self.procCycle = procCycle
		self.fusCycle = fusCycle
		self.upCycle = upCycle
		self.collecting_rate = 1

		self.psan = None

	
	def set_psan (self, psan):
		self.psan = psan

	def set_collecting_rate (self, rate):
		self.collecting_rate = rate
