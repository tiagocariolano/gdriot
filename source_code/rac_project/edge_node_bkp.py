from math import sqrt
from virtual_node import VirtualNode
from util import *



class EdgeNode:
	"""
		This class implements a edge node behavior (operation thread).
	"""
	MASTER = 'MASTER'						# Indicates Edge Node is a master
	SLAVE = 'SLAVE'							# Indicates Edge Node is a slave
	PRICE_MAX = 100
	PRICE_INC = PRICE_MAX * 0.1
	CRE_DEFAULT = 20						# Default value of reconfiguration cost
	LAT_EN_CLOUD_DEFAULT = 13				# en -> cloud latency default value
	MAX_DATABASE_SIZE = 100

	def __init__(self, env, interface, id, type, posX, posY, hiPos, cpuRate, totalRAM, totalStorage, do_steal=False, kappa=32):
		"""
			EdgeNode constructor method.
		"""
		self.env = env
		self.interface = interface
		self.id = id
		self.process = None
		self.process2 = None
		self.type = type
		self.posX = posX
		self.posY = posY
		self.hiPos = hiPos
		self.cpuRate = cpuRate
		self.totalRAM = self.RAM = totalRAM
		self.totalStorage = self.storage = totalStorage
		self.vnList = []
		self.dtList = []
		self.psanList = []
		self.neighborList = []
		self.cloud = None
		self.requester = None
		self.database = {"db":[], "avg": 0, "sum": 0, "n": 0, "delay": []}
		self.results = {"Requests" : [], "Energy" : {}, "Network" : [],
						"sumlow": 0, "summiddle": 0, "sumhigh": 0,
						"sumwaittime": 0.0, "nwaittime": 0}
		self.max_cost = 0
		self.busy_cores = 0
		self.request_queue = []
		self.ocioso = 0
		self.do_steal = do_steal
		self.kappa = kappa

		self.priority_list = []
		log.log ( 'Init Edge Node')

	def set_kappa (self, kappa):
		self.kappa = kappa
	def do_stealing (self):
		self.do_steal = True

	def distance (self, c1, c2):
		return sqrt ((c1[0] - c2[0]) ** 2 + (c1[1] - c2[1]) ** 2)

	def get_results (self):
		for psan in self.psanList:
			distance = self.distance ([self.posX, self.posY], [psan.posX, psan.posY])
			self.results["Energy"][psan] = {"data_r" : psan.results["Energy"]["data_r"],
											"data_s" : psan.results["Energy"]["data_s"],
											"distance" : distance}
		return self.results

	def update_database (self, time, value):
		entry = None
		if len (self.database["db"]) >= self.MAX_DATABASE_SIZE:
			entry = self.database["db"].pop ()
			self.database["n"] -= 1

		self.database["db"] = [[time, value]] + self.database["db"]
		self.database["n"] += 1

		if len (self.database["db"]) >= 2:
			intervalDataAdded = self.database["db"][0][0] - self.database["db"][1][0]
			self.database["sum"] += intervalDataAdded

			if entry:
				intervalDataDeleted = self.database["db"][-1][0] - entry[0]
				self.database["sum"] -= intervalDataDeleted

			self.database["avg"] = float (self.database["sum"]) / (self.database["n"] - 1)

	def add_data_type (self, data_type):
		"""
			Add new data type to edge node
		"""
		for dt in self.dtList:
			if dt == data_type:
				return
		self.dtList.append (data_type)

	def add_neighbor (self, neighbor):
		"""
			Add new neighbor to edge node
		"""
		#sanity check
		if not hasattr (neighbor, "type"):
			log.error ("EdgeNode.add_neighbor: Class " + str (neighbor) + "cannot to be added in edge node.")

		if neighbor.type  == "SLAVE" or neighbor.type == "MASTER":
			self.neighborList.append (neighbor)
		elif neighbor.type == "CLOUD":
			self.cloud  = neighbor
		elif neighbor.type == "REQUESTER":
			self.requester = neighbor
		elif neighbor.type == "PSAN":
			self.psanList.append (neighbor)
		else:
			log.error (	"EdgeNode.add_neighbor: Type " + str (neighbor.type) + " from class " + str (neighbor) +
						" is unknown")

	def check_type (self, type):
		"""
			Check type of edge node

			Return: boolean indicating if types match
		"""
		return self.type == type

	def check_resources (self, request):
		"""
			Check if available	resources in edge node are suffient to attend request
			Args:
				request:  a request to be executed

			Return: boolean indicating if edge node is able to attend request
		"""
		return 	self.RAM >= request.RAM and self.storage >= request.storage

	def update_resources (self, vn):
		self.RAM += vn.RAM
		self.storage += vn.storage
		self.busy_cores -= 1

	def allocate (self, vn, request):
		"""
			Provisioning vn resources to request and update edge node available resources

			Args:
				vn: a vn to be allocated

				request: a request
		"""

		if vn not in self.vnList:
			vn.provisioning (request)

			self.RAM -= vn.RAM
			self.storage -= vn.storage

			self.vnList.append (vn)

		else:
			vn.provisioning (request)
			#print "RAM:", self.RAM

			#print "alloc", self.RAM, vn.RAM

			self.RAM -= vn.RAM
			self.storage -= vn.storage
			# if self.RAM < 0:
			# 	print "alloc2", self.RAM
			# 	raw_input ()

			#self.RAM += vn.RAM
			#self.storage += vn.storage
			#print "RAM:", self.RAM, vn.RAM, vn.storage
		self.results["sumwaittime"] += self.env.now - request.tStart
		self.results["nwaittime"] 	+= 1

		self.busy_cores += 1
		vn.run ()

	def run (self):
		"""
			Create a process and start operation thread execution
		"""
		self.process = self.env.process (self.operation_thread ())
		#self.process2 = self.env.process (self.priority_queue_thread ())
		#self.process2 = self.env.process (self.stealing_thread ())

	def steal (neighbor):
		reqList = neighbor.interface.moduleComm.items
		print reqList, raw_input ()

	def stealing (self):
		i = 0
		## stealing
		for neighbor in self.neighborList:
			q_neighbor = len (neighbor.request_queue)
			if q_neighbor > 0:
				for request in neighbor.request_queue:
					if request.dt in self.dtList and i < self.kappa and i < self.busy_cores and self.busy_cores < neighbor.busy_cores:
					# if request.dt in self.dtList and i < 32 and i < self.busy_cores:
						# print neighbor.request_queue
						neighbor.request_queue.remove (request)
						# print neighbor.request_queue
						# print [request], raw_input ()
						# neighbor.request_queue.remove (request)
						self.request_queue.append (request)
						# print request.dt.type, ","
						i += 1

	def priority_queue_thread (self):
		no_allocation = True
		while no_allocation:
			yield self.env.timeout (0.1)
			for i, request in enumerate (self.priority_list):
				bestVn = self.search_possible_vn_locally (request)
				prob = random.random ()
				if bestVn and prob <= bestVn.utilValue:
					self.allocate (bestVn, request)
					self.priority_list.pop (i)
					no_allocation = False
					break

	def operation_thread (self):
		"""
			Implement operation thread of edge node. It controls
			events of arrived request and virtual node finishing. This method
			attempts to allocate a request to local virtual node or to
			slaves nodes (if edge node is master) or to other master node, in
			this order. If request cannot be allocated it turns back to
			higher level.
		"""
		while True:
			try:
				yield self.env.timeout (SIMULATION_TIME)

			except simpy.Interrupt as interrupt:
			 	event = interrupt.cause
				log.log("EDGE NODE " + str (self.id) + " ================ Begin")
				log.log (self)
				# print "{:08.4f}, {:02d},{:04d}".format (self.env.now, self.id, len (self.request_queue)),
				if len (self.request_queue) == 0:
					self.ocioso += 1
					# print ("<<<<<<<<", self.id, self.ocioso)
				if event.type == Event.ARRIVED_MSG:
					request = event.obj
					self.request_queue.append (request)
					# print self.env.now, "Edge S: Req ", request.tStart, self.id
				elif event.type == Event.FINISHED_VN:
					event = interrupt.cause
					vn = event.obj
					# log.log ("VN FINISHED: " + str(self.env.now) + " s")

					# print self.env.now, "Edge F: Req ", vn.request.tStart, self.id, request.dt.type#, [dt.type for dt in self.dtList]

					self.update_resources (vn)

					tStart = vn.request.tStart
					tFinal = self.env.now
					makespan = tFinal - tStart
					self.results["Requests"].append ([	tStart, tFinal,
														makespan, vn.reqState,
														self.env.now, vn.request.hop,
														vn.request.hopedge, vn.request.hopcloud,
														vn.request.price, vn.request.RAM,
														vn.request.cycle, self.cpuRate,
														vn.start])

				if self.do_steal:
					if len (self.request_queue) == 0:
						# print "Before:"
						# for neigh in self.neighborList:
						# 	print neigh.id, " => ", len (neigh.request_queue), neigh.busy_cores
						# print self.id, " => ", len (self.request_queue), self.busy_cores
						self.stealing ()
						# print "After:"
						# for neigh in self.neighborList:
						# 	print neigh.id, " => ", len (neigh.request_queue), neigh.busy_cores
						# print self.id, " => ", len (self.request_queue), self.busy_cores

				if len (self.request_queue) > 0 and self.busy_cores  < 4:
					request = self.request_queue.pop (0)
					# print "teste"
					log.log (request)
					#Search best vn locally
					bestVn  = self.search_possible_vn_locally (request)
					# print bestVn
					log.log ("VN: ", bestVn)
					if bestVn:
						prob = random.random ()
						#print prob, bestVn.utilValue
						#raw_input ()
						#print request.price, bestVn.utilValue, prob
						#raw_input ()
						if prob <= bestVn.utilValue:
							# allocate request to VN
							# print "allocate"
							self.allocate (bestVn, request)
						else:
							#request.price += self.PRICE_INC
							self.priority_list.append (request)
							self.env.process (self.priority_queue_thread ())
							# if request.price == 100:
							# 	print "teste", raw_input ()
							# self.results["sumlow"] = 0
							# self.results["summiddle"] = 0
							# self.results["sumhigh"] = 0
							# for request in self.priority_list:
							# 	if request.price == 1:
							# 		self.results["sumlow"] += 1
							# 	elif request.price == 50:
							# 		self.results["summiddle"] += 1
							# 	elif request.price == 100:
							# 		self.results["sumhigh"] += 1
							# print "low   : ", self.results["sumlow"]
							# print "middle: ", self.results["summiddle"]
							# print "high  : ", self.results["sumhigh"]
							# if self.results["sumhigh"] > 0:
							# 	raw_input ()


						log.log ( "VN ALLOCATE: " + str (self.env.now) + " s")
					else:

						#Search all slaves neighbors
						bestSlave = self.search_possible_vn_slave (request)
						log.log ("bestSlave: ", bestSlave)
						if bestSlave:
							log.log ( "Foward to SLAVE")
							self.foward_request (bestSlave, request)

						else:
							if self.check_type (EdgeNode.MASTER):
								bestMaster = self.search_possible_vn_master (request)

								if bestMaster:
									self.foward_request (bestMaster, request)
								else:
									request.tEdge = self.env.now
									self.foward_request (self.cloud, request)
							else:
								parent = self.search_en_parent ()
								log.log ("parent: ", parent)
								self.foward_request (parent, request)

				# elif event.type == Event.FINISHED_VN:
				# 	event = interrupt.cause
				# 	vn = event.obj
				# 	if DEBUG:
				# 		print "VN FINISHED: " + str(self.env.now) + " s"
				# 		print self
				#
				# 	self.update_resources (vn)
				#
				# 	tStart = vn.request.tStart
				# 	tFinal = self.env.now
				# 	makespan = tFinal - tStart
				# 	self.results["Requests"].append ([	tStart, tFinal,
				# 										makespan, vn.reqState,
				# 										self.env.now, vn.request.hop,
				# 										vn.request.hopedge, vn.request.hopcloud,
				# 										vn.request.price])
				#
				# 	log.count ()
				# 	#print "Edge time: ", self.env.now - request.tEdge
				# 	# print ("EdgeNode" + str (self.id) + ":" +
				# 	# 			"\n Request tStart	: " + str (tStart) +
				# 	#  			"\n tFinal          : " + str (tFinal) +
				# 	# 			"\n Makespan        : " + str (makespan) +
				# 	# 			"\n Request State   : " + str (vn.reqState))
				# 	#raw_input ()
				# 	# if STEP_BY_STEP:
				# 	# 	pass
				# 	# 	raw_input ()
				#
				# elif event.type == Event.ARRIVED_DATA:
				# 	event = interrupt.cause
				# 	data = event.obj
				#
				# 	value = data.value
				# 	time = data.time
				# 	#print self.database
				# 	self.update_database (time, value)
				# 	#print self.database, raw_input ()
				# 	#print "DATA ARRIVED", self.env.now, raw_input ()
				# 	#log.log ("DATA ARRIVED (EdgeNode): " + str (self.env.now) + " s")

				log.log("EDGE NODE " + str (self.id) + " ----------End")

	def search_possible_vn_locally (self, request):
		"""
			Do a local search in list of vns who attend request as algorithm specification.

			Args:
				request: request to be allocated

			Return: best vn finded or empty, if no vn is possible to be allocated
		"""

		# Check if edge node has some data type equal to request data type
		if request.dt not in self.dtList:
			return []

		# Check if edge node resources are sufficient
		#print "check", self.RAM, request.RAM, self.check_resources (request)
		if not self.check_resources (request):
			return []

		# Filter feasible vns who have data type equal to request and state equal IDLE
		feasibleVnList = self.filter_feasible_vn_list (request)

		# No feasible vn, so create a new vn
		if not feasibleVnList:

			log.log ( 'NEW VN     : ' + str (self.env.now) + " s")

			# create new vn with appropriate request requirements and add to feasible list
			newVn = VirtualNode (self, self.cpuRate, request.RAM, request.storage, request.dt, [])
			feasibleVnList.append (newVn)

		# compute utility value for each vn
		for vn in feasibleVnList:

      		## cRe computation
			# Check appropriate resources and set reconfiguration cost (cRe)
			if not vn.check_resources (request):
				cRe = vn.cRe
			else:
				cRe = 0.

			## cOn computation
			# Check if vn was instantiate now and set instantiate cost (cOn)
			if vn not in self.vnList:
				cOn = vn.cOn
			else:
				cOn = 0.

			## cPr computation
			cPr = float (request.cycle) / self.cpuRate

			## cUp computation
			# a dummy value, for while...
			# if request.update:
			# 	cUp = request.dt.upCycle
			# else:
			# 	cUp = 0
			cUp = float (self.database["avg"])

			## cFi computation
			cFi = 0
			if self.results["nwaittime"] > 0:
				cFi = float (self.results["sumwaittime"]) / self.results["nwaittime"]
			# print "sumwaittime : ", self.results["sumwaittime"]
			# print "nwaittime   : ", self.results["nwaittime"]
			# print "cFi         : ", cFi
			#raw_input ()

			## cTr computation
			## This cost is updated in each send message
			cTr = float (request.cTr)

			#if log.c > 9900:
			# if request.t - request.tStart > 0.000001:
			# 	print self.interface.moduleAssoc, request.t, request.tStart, request.t - request.tStart
			#raw_input ()

			cost = cRe + cOn + cPr + cUp + cFi + cTr

			# print "cRe : ", cRe
			# print "cOn : ", cOn
			# print "cPr : ", cPr
			# print "cUp : ", cUp
			# print "cFi : ", cFi
			# print "cTr : ", cTr
			#if self.type == "MASTER":#SLAVE" and self.id == 5:
			#	print "cost: ",  cost
			# raw_input ()
			vn.compute_util_value (cost, request.price)
			#print "util", vn.utilValue

		# Find vn who has  mininum utility function value
		bestVn = feasibleVnList.pop (0)
		for vn in feasibleVnList:
			if bestVn.better_than (vn):
				bestVn = vn

			# prob = random.random ()
			#print prob, bestVn.utilValue
			#raw_input ()
			# if prob >= bestVn.utilValue:
			# 	request.price += self.PRICE_INC
			# 	self.priority_list.append (request)
				#print len (self.priority_list)
				# return []

		# Return best vn
		return bestVn

	def search_possible_vn_slave (self, request):

		# Filter neighbors who are slaves
		slaveNeighborList = self.filter_neighbor_list (self.SLAVE)

		if not slaveNeighborList:
			return []

		bestVnNeighborList = []

		for neighbor in slaveNeighborList:

			# Query each neighbor to your best vn
			bestVn = self.query_neighbor (neighbor, request)

			if bestVn:
				bestVnNeighborList.append ([bestVn, neighbor])

		if not bestVnNeighborList:
			return []

		bestVn, bestNeighbor = bestVnNeighborList.pop (0)

		for vn, neighbor in bestVnNeighborList:
			if bestVn.better_than (vn):
				bestVn = vn
				bestNeighbor = neighbor

		return bestNeighbor

	def search_possible_vn_master (self, request):
		# Filter neighbors who are slaves
		masterNeighborList = self.filter_neighbor_list (self.MASTER)

		if not masterNeighborList:
			return []

		bestVnNeighborList = []
		for master in masterNeighborList:
			bestVn = self.query_neighbor (master, request)

			if bestVn:
				bestVnNeighborList.append ([bestVn, master])

		if not bestVnNeighborList:
			return []

		bestVn, bestNeighbor = bestVnNeighborList.pop (0)

		for vn, neighbor in bestVnNeighborList:
			if bestVn.better_than (vn):
				bestVn = vn
				bestNeighbor = neighbor

		return bestNeighbor

	def search_en_parent (self):
		"""
			Search in neighbors, that vn is your parent in hierarchy

			Return: parent node of en
		"""
		# first, search in masters
		neighborList = self.filter_neighbor_list (self.MASTER)
		if not neighborList:
			#second, search in slaves
			neighborList = self.filter_neighbor_list (self.SLAVE)
			if not neighborList:
				#last, choose cloud
				neighborList = [self.cloud]

		if not neighborList:
			log.error ("EdgeNode.search_en_parent: No edge node or cloud is neighbor from EdgeNode" + str (self.id))

		minHiPos = 999999
		parent = None
		for neighbor in neighborList:
			if neighbor.hiPos < minHiPos:
				minHiPos = neighbor.hiPos
				parent = neighbor

		if parent is None:
			log.error ("EdgeNode.search_en_parent: No parent node found in EdgeNode " + str (self.id))

		return parent

	def filter_feasible_vn_list (self, request):
		"""
			Check availability from each vn to allocate request and filter
			available vns

			Args:
				vnList: list of vns

				request: request used on checking

			Returns:
				feasibleVnList: list of these vns have all requeriments
		"""
		feasibleVnList = []

		for vn in self.vnList:
			#if vn.check_availability (request):
			if vn.check_availability (request) and self.RAM > vn.RAM:
				feasibleVnList.append (vn)

		return feasibleVnList

	def filter_neighbor_list (self, type):
		"""
			Check if each neighbor is a slave or master and filter them to a new list

			Args:
				neighborList: List of neighbors of edge node

			Return: list of slaves neighbors
		"""
		neighborList = []

		for neighbor in self.neighborList:

			# neighbor type is equal type, so add it to slave list
			if neighbor.check_type (type):
				neighborList.append (neighbor)

		return neighborList

	def query_neighbor (self, neighbor, request):
		"""
			Query neighbor to allocate request. This method simulate a
			message exchange between en and neighbor.

			Args:
				neighbor: a edge node neighbor queried

				request: a request
		"""
		bestVn = neighbor.search_possible_vn_locally (request)

		return bestVn

	def foward_request (self, neighbor, request):
		request.hop += 1
		# request.t = self.env.now
		if neighbor.type == "CLOUD":
			request.hopcloud += 1
		elif neighbor.type == "SLAVE" or neighbor.type == "MASTER":
			request.hopedge += 1
		else:
			print neighbor.type, raw_input ()
		self.send_message (neighbor, request)

	def send_message (self, neighbor, msg):
		"""
			Send a messsage to a neighbor edge node. Create event ARRIVED_MSG
			and generate interruption in neighbor edge node.

			Args:
				neighbor: a neighbor edge node

				msg: message to be sent
		"""
		ev = Event (Event.ARRIVED_MSG, msg)
		self.interface.send_message (ev, neighbor)

	def __str__ (self):
		"""
			Print resources info about edge node
		"""
		GB = GHz = 10 ** 9
		MB = MHz = 10 ** 6
		out = "{:5s} {:02d}: {:010.4f}, {:010.4f}, {:08.4f}, {:5s}, ".format ("EN", self.id,
																	float (self.cpuRate) / MHz, float (self.RAM) / MB,
																	float (self.storage) / MB, self.type)
		for dt in self.dtList:
			out = out + "{:5s}-".format (dt.type)
		return out

	# def process_request (self, request):
	# 	#while True:
	# 		# try:
	# 		# 	yield self.env.timeout (SIMULATION_TIME)
	# 		#
	# 		# except simpy.Interrupt as interrupt:
	# 		# 	event = interrupt.cause
	# 		#event = eventQueue.get ()
	#
	# 		#if event.type == Event.ARRIVED_MSG:
	# 	log.log ( "REQUEST ARRIVED ")
	#
	# 	#receive message
	# 	#msg = event.obj
	# 	#request = msg.data
	#
	# 	#self._print ()
	# 	#request._print ()
	#
	# 	#Search best vn locally
	# 	bestVn  = self.search_possible_vn_locally (request)
	#
	# 	if bestVn:
	# 		# allocate request to VN
	# 		self.allocate (bestVn, request)
	# 		#self.update_resources (bestVn)
	# 		log.log ( "Allocate")
	# 		return None, None
	# 	else:
	#
	# 		#Search all slaves neighbors
	# 		bestSlave = self.search_possible_vn_slave (request)
	#
	# 		if bestSlave:
	# 			log.log ( "Foward to SLAVE")
	# 			#self.foward_request (bestSlave, request)
	# 			return bestSlave, request
	# 		else:
	#
	# 			if self.check_type (EdgeNode.MASTER):
	# 				log.log ( "Foward to other MASTER or CLOUD")
	# 				return None, None
	# 				#bestMaster = self.search_possible_vn_master ()
	#
	# 				#if bestMaster:
	# 				#	self.foward_request (bestMaster, request)
	# 				#else:
	# 				#	self.foward_request (self.cloud)
	#
	# 			# edge node is a slave
	# 			else:
	#
	# 				log.log ( "Foward to PARENT")
	# 				parent = self.search_en_parent ()
	# 				#self.foward_request (parent, request)
	# 				return parent, request
	#
	# 	#self._print ()
	# 	#request._print ()
