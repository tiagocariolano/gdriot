import numpy as np
import util
from request import Request
from util import *
from data_type import DataType


class Requester:
    TYPE_INTERVAL = "constant"
    INTERVAL_M = 0.05
    INTERVAL_S = 0.001
    def __init__(self, env, interface,  typeInterval=TYPE_INTERVAL,
                                        intervalM=INTERVAL_M,
                                        intervalS=INTERVAL_S):
        self.env = env
        self.interface = interface
        self.type = "REQUESTER"

        self.typeInterval = typeInterval
        self.intervalM = intervalM
        self.intervalS = intervalS

        self.neighborList = []

        self.cycle = 10000000
        self.RAM = 200000
        self.storage = 0
        self.dt = DataType ("DT1",int (20 * 1000000 / 3), int (20 * 1000000 / 3), int (20 * 1000000 / 3)) #dummy data type
        self.price = 0
        self.update = False
        self.process = None

    def add_neighbor (self, neighbor):
        self.neighborList.append (neighbor)

    def set_neighbor_list (self, neighborList):
        self.neighborList = neighborList

    def set_request_parameters (self, cycle, RAM, storage, dt, price, update):
        self.cycle = cycle
        self.RAM = RAM
        self.storage = storage
        self.dt = dt
        self.price = price
        self.update = update
        self.simTime = simTime

    def generate_request (self):
        request = Request (self.cycle,
                            self.RAM,
                            self.storage,
                            self.dt,
                            self.price,
                            self.update)
        request.set_t_start (self.env.now)

        return request

    def run (self):
        self.process = self.env.process (self.operation_thread ())

    def operation_thread (self):
        while True:
        #for i in range (0, 10):
            #print "operation_thread requester", i, raw_input ()
            request = self.generate_request ()
            for neigh in self.neighborList:
                self.send_message (neigh, request)
            if self.typeInterval == "constant":
                interval = self.intervalM
            elif self.typeInterval ==  "gauss":
                interval = abs (util.np.random.normal (self.intervalM, self.intervalS))
            elif self.typeInterval == "exponential":
                interval = abs (util.np.random.exponential (self.intervalM))
            else:
                log.error ("Requester.operation_thread: Type of interval '" + self.typeInterval + "' does not exist")

            yield self.env.timeout(interval)

    def send_message (self, neighbor, msg):
        ev = Event (Event.ARRIVED_MSG, msg)
        self.interface.send_message (ev, neighbor)
