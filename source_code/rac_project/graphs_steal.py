import matplotlib
matplotlib.use('Agg')
import os
import pickle
import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import ScalarFormatter
from matplotlib.ticker import FormatStrFormatter
from configuration import *
from resource_allocator_algorithm.utils import VERTICAL, ONLY_CLOUD, HIERARCHY, LINE, LINE_RANDOM

class Statistics:
    TYPE_MEAN = "TYPE_MEAN"
    TYPE_MAX = "TYPE_MAX"

    def __init__ (self, parser,
                        prefix_path="../",
                        extension="pickle",
                        files_names=[]):

        self.parser = parser
        self.prefix_path = prefix_path
        self.extension =  extension
        self.files_names = files_names

        self.params_names = []
        self.params_values = []
        self.params_formats = []
        self.data_files_names = []

    def set_params_names (self, params_names):
        self.params_names = params_names

    def set_params_values (self, params_values):
        self.params_values = params_values

    def set_params_formats (self, params_formats):
        self.params_formats = params_formats

    def get_path (self, val0, val1, val2):
        # file_name = "{0:6.6s}-{1:" + self.params_formats[0] + "}-" + \
        #             "{2:6.6s}-{3:" + self.params_formats[1] + "}-" + \
        #             "{4:6.6s}-{5:" + self.params_formats[2] + "}"
        file_name = "{0:s}-{1:" + self.params_formats[0] + "}-" + \
                    "{2:s}-{3:" + self.params_formats[1] + "}-" + \
                    "{4:s}-{5:" + self.params_formats[2] + "}"

        file_name = file_name.format (
                    self.params_names[0], val0,
                    self.params_names[1], val1,
                    self.params_names[2], val2)
        file_name = file_name.replace (" ", "_") + "." + self.extension
        path = os.path.join (self.prefix_path, file_name)
        return path

    def read (self):
        dim0 = len (self.params_values[0])
        dim1 = len (self.params_values[1])
        dim2 = len (self.params_values[2])
        _, dim3 = self.parser.get_dim ()

        self.data = np.zeros ((dim0, dim1, dim2, dim3))
        for i, val0 in enumerate (self.params_values[0]):
            for j, val1 in enumerate (self.params_values[1]):
                for k, val2 in enumerate (self.params_values[2]):
                    path = self.get_path (val0, val1, val2)
                    raw = self.parser.read (path)
                    self.data[i][j][k] = raw

    def format_by_column (self, col_slice, type=TYPE_MEAN):
        data = []
        if type == self.TYPE_MEAN:
            for mean_strategy in self.means:
                data.append (np.array (mean_strategy[:, col_slice]))
        elif type == self.TYPE_MAX:
            for max_strategy in self.maxs:
                data.append (np.array (max_strategy[:, col_slice]))

        return np.array (data)

    def format_by_line (self, line, type_stat=TYPE_MEAN):
        data = []
        for mean_strategy in self.means:
            if type == self.TYPE_MEAN:
                data.append (np.array (mean_strategy[line, :]))
            elif type == self.TYPE_MAX:
                data.append (np.array (mean_strategy[line, :]))

    def get_column (self, column):
        return self.means[:, column]

    def get_line (self, line):
        return self.means[line, :]

class Plot:
    DEFAULT_LINE_STYLE = "--"
    DEFAULT_MAKERS = ["o", "s", "x", "^", "<", ">", "P", "*", "v", "+", "D"]
    DEFAULT_PREFIX_PATH = "./"
    DEFAULT_FILE_NAME = ""

    def __init__ (self, linestyle   = DEFAULT_LINE_STYLE,
                        markers     = DEFAULT_MAKERS,
                        prefix_path = DEFAULT_PREFIX_PATH,
                        color_max   = 1000,
                        color_offset= 600,
                        color_map   = "Blues"):

        self.fig, self.ax = plt.subplots()

        self.linestyle = linestyle
        self.markers = markers
        self.norm = plt.Normalize (0, color_max)
        self.cmap = plt.get_cmap (color_map)
        self.color_max = color_max
        self.color_offset = color_offset
        self.prefix_path =prefix_path


    def _save (self, file_name):
        if file_name == "":
            plt.show ()
        else:
            self.fig.savefig (file_name, dpi=1280, bbox_inches = 'tight', pad_inches = 0.01)
            plt.close (self.fig)

    def _legend (self, legend, legend_location):
        if legend:
            plt.legend(legend, loc=legend_location)

    def _xticks (self, xticks_values, xticks_labels):
        if xticks_values != []:
            plt.xticks (xticks_values, xticks_labels)

    def _yticks (self, yticks_values, yticks_labels):
        if yticks_values != []:
            plt.yticks (yticks_values)

    def lines (self , X, Y,
                            file_name="",
                            xlabel="",
                            ylabel="ylabel",
                            title="",
                            legend=(),
                            legend_location="upper right",
                            xticks_values=[],
                            xticks_labels=[],
                            yticks_values=[],
                            yticks_labels=[],
                            colors={},
                            markerssize={},
                            top_ylim = None,
                            annotates = {}):


        tot_colors = len (Y)
        for i, (x, y) in enumerate (zip (X, Y)):
            cmap_pos = self.color_max/tot_colors * i + self.color_offset
            color = np.array (self.cmap (self.norm (cmap_pos)))
            marker = self.markers[i]
            markersize = 7
            if i in colors:
                color = colors[i]
            if i in markerssize:
                markersize = markerssize[i]
            #print color
            plt.plot (x, y,
                linestyle   = self.linestyle,
                marker      = marker,
                color       = color,
                markersize  = markersize
                )

        #print file_name

        plt.xlabel (xlabel)
        plt.ylabel (ylabel)
        plt.title (title)



        #self.ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        plt.ticklabel_format (axis='y', scilimits=(-2,5), style='sci')
        #self.ax.yaxis.major.formatter.set_powerlimits((0,0))
        #self.ax.yaxis.set_major_formatter(ScalarFormatter(useMathText=True, useOffset=False))
        if top_ylim is not None:
            bottom, top = plt.ylim ()
            plt.ylim (bottom, top_ylim)

            for (text, x, y) in zip (annotates["annote_texts"], annotates["annote_x"], annotates["annote_y"]):
                #print text, x, y
                self.ax.annotate (str (text), xy=(x, y))
        # bottom, top = plt.ylim ()
        # plt.ylim (bottom, 2000)
        #
        # x  = X[0]
        # y  = np.repeat (2000, len (x))
        # plt.plot (x, y, color="green", marker = "v", markersize=20)

        self._legend (legend, legend_location)

        self._xticks (xticks_values, xticks_labels)

        self._yticks (yticks_values, yticks_labels)

        # plt.set_major_formatter(ScalarFormatter())


        self._save (file_name)

    def bars (self, X, Y,
                        file_name="",
                        xlabel="xlabel",
                        ylabel="ylabel",
                        title="title",
                        legend=(),
                        legend_location="upper right",
                        xticks_values=[],
                        xticks_labels=[],
                        vertical_space=0,
                        normalize=False):

        bottom = np.zeros (len (X[0]))
        color_index = 0

        if normalize:
            for y in Y:
                total = np.sum (y)
                y = y / total

        Y = np.transpose (Y)

        title_index = 0
        for x, y in zip (X, Y):

            plt.xlabel (xlabel)
            plt.ylabel (ylabel)
            plt.title (title[title_index])



            plt.bar (x, y,
                    bottom  = bottom,
                    color   = self.colors[color_index])
            bottom = bottom + y + vertical_space
            color_index += 1
            title_index += 1


        self._legend (legend, legend_location)

        self._xticks (xticks_values, xticks_labels)

        self._save (file_name)

class Parser:
    SINGLE_FILE = "SINGLE_FILE"
    MULTIPLE_FILE = "MULTIPLE_FILE"

    def __init__ (self, type=SINGLE_FILE, n=1, m=1):
        self.content = None
        self.type = type
        self.n = n
        self.m = m

    def _read_file (self, path):
        content = []
        try:
            with open (path, "rb") as f:
                content = pickle.load (f, encoding="latin1")
        except Exception as e:
            print (e)
            print ("Graph: File " + path + " cannot be opened.")
            exit ()

        return content

    def get_dim (self):
        return self.n, self.m

    def read (self, paths):
        if self.type == self.SINGLE_FILE:
            self.raw = self._read_file (paths)
            data = self.raw["Stats"]
            if self.m > len (data):
                print ("error: parser.read: dimensions of interval array is larger than size of array.")
                exit (1)

            return np.array (self.raw["Stats"][0:self.m])

def custom_reqrate_labels ( param_labels, nedges):
    return 1 / np.array (param_labels) / nedges

if __name__ == "__main__":
    parser = argparse.ArgumentParser ()

    parser.add_argument('--results',         type=str,	dest='results',          default=PREFIX_PATH)
    parser.add_argument('--var_names',       type=str,	dest='var_names',        nargs="*", default=[])
    parser.add_argument('--var_fields',      type=int,	dest='var_fields',       nargs="*", default=[])
    parser.add_argument('--param_name0',     type=str,	dest='param_name0',      default="nrequest")
    parser.add_argument('--param_name1',     type=str,	dest='param_name1',      default="strategy")
    parser.add_argument('--param_values0',   type=str,	dest='param_values0',    nargs="*",  default=[])
    parser.add_argument('--param_values1',   type=str,	dest='param_values1',    nargs="*",  default=[])
    parser.add_argument('--param_type0',     type=str,	dest='param_type0',      default="normal")
    parser.add_argument('--round_min',       type=int,	dest='round_min',        default=0)
    parser.add_argument('--round_max',       type=int,	dest='round_max',        default=64)

    args = parser.parse_args ()

    if args.param_values0:
        PARAMS[args.param_name0]["values"] = args.params_values0
        PARAMS[args.param_name0]["length"] = len (args.params_values0)
        PARAMS[args.param_name0]["labels"] = tuple (args.params_values0)

    if args.param_values1:
        PARAMS[args.param_name1]["values"] = args.params_values1
        PARAMS[args.param_name1]["length"] = len (args.params_values1)
        PARAMS[args.param_name1]["labels"] = tuple (args.params_values1)

    prefix_path         = args.results
    round_min           = args.round_min
    round_max           = args.round_max
    PARAMS["round"]["value"] = range (round_min, round_max)
    PARAMS["round"]["length"] = len (PARAMS["round"]["value"])
    PARAMS["round"]["label"] = tuple (PARAMS["round"]["value"])
    # print PARAMS["round"]
    args_params_names   = [args.param_name0, args.param_name1, "round"]
    params_names        = [param_name for param_name in args_params_names]
    params_values       = [PARAMS[param_name]["value"] for param_name in args_params_names]
    params_lengths      = [PARAMS[param_name]["length"] for param_name in args_params_names]
    params_formats      = [PARAMS[param_name]["format"] for param_name in args_params_names]
    params_labels       = [PARAMS[param_name]["label"] for param_name in args_params_names]
    params_xlabel       = [PARAMS[param_name]["xlabel"] for param_name in args_params_names]
    formatted_params_names = ["{:.4s}".format (param_name) for param_name in params_names]

    stats = Statistics (Parser (n=DIM_N,m=DIM_M), prefix_path=prefix_path, extension=FILE_EXTENSION)
    stats.set_params_names (formatted_params_names)
    stats.set_params_values (params_values)
    stats.set_params_formats (params_formats)

    print (params_names)
    print (params_values)
    print (params_formats)

    stats.read ()

    #compute rounds stats
    original_mean = stats.data.mean (axis=2)
    original_max =  stats.data.max (axis=2)
    original_std = stats.data.std (axis=2)
    original_min = stats.data.min (axis=2)
    original_max = stats.data.max (axis=2)
    print ("original:\n", stats.data)
    print ("mean:\n", original_mean)
    print ("max:\n", original_max)
    print ("std:\n", original_std)
    print ("min:\n", original_min)
    print ("max:\n", original_max)

    #VARS = {"responsetime_mean" : 0}

    if args.param_name0 == "reqrate":
        params_labels[0] = tuple (custom_reqrate_labels (params_labels[0], 20))
    if args.param_name1 == "reqrate":
        params_labels[1] = tuple (custom_reqrate_labels (params_labels[1], 20))

    if len (args.var_names) > 0:
        # params_values       = [PARAMS[params_names[0]]["value"], PARAMS[params_names[1]]["value"]]
        # params_lengths      = [PARAMS[params_names[0]]["length"], PARAMS[params_names[1]]["length"]]
        # params_formats      = [PARAMS[params_names[0]]["format"], PARAMS[params_names[1]]["format"]]
        # params_labels       = [PARAMS[params_names[0]]["label"], PARAMS[params_names[1]]["label"]]
        # params_xlabel       = PARAMS[params_names[1]]["xlabel"]
        #
        # params_names += ["rounds"]
        # params_values += [PARAMS["round"]["value"]]
        # params_formats += [PARAMS["round"]["format"]]
        #
        # stats = Statistics (Parser (n=DIM_N,m=DIM_M), prefix_path=prefix_path, extension=FILE_EXTENSION)
        # stats.set_params_names (params_names)
        # stats.set_params_values (params_values)
        # stats.set_params_formats (params_formats)
        #
        # stats.read ()
        #
        # mean = stats.data.mean (axis=2)
        # max =  stats.data.max (axis=2)
        # std = stats.data.std (axis=2)
        # print "original:\n", stats.data
        # print "mean:\n", mean
        # print "max:\n", max
        # print "std:\n", std

        var_fields = args.var_fields
        var_names = args.var_names
        var_name = "price"
        mean = original_mean
        max = original_max
        std = original_std

        if len (var_names) != len (var_fields):
            print ("error: var_names and var_fields should be equal")

        for i, _ in enumerate (params_values[0]):
            label = params_labels[0][i]
            print (label)
            # file_name = label + "-" + params_names[1] + "-" + var_name + ".pdf"
            # file_name_inv = label + "-" + var_name + "-" + params_names[1] + ".pdf"
            file_name = label + "-" + params_names[1] + "-" + var_name + ".png"
            file_name_inv = label + "-" + var_name + "-" + params_names[1] + ".png"

            x_len    = len (var_names)
            leg_len  = params_lengths[1]

            x_ticks = var_names
            leg_ticks = params_labels[1]

            print (x_len, leg_len)
            print (x_ticks, leg_ticks)

            print (var_fields)
            X           = np.broadcast_to (np.arange (x_len), (leg_len, x_len))
            Y_mean      = mean[i,:,var_fields].transpose ()
            Y_max       = max[i,:,var_fields].transpose ()

            X_inv       = np.broadcast_to (np.arange (leg_len), (x_len, leg_len))
            Y_mean_inv  = mean[i,:,var_fields]
            Y_max_inv   = max[i,:,var_fields]

            print ("Y_mean_inv:")
            # print Y_mean_inv, raw_input ()

            plot = Plot ()
            plot.lines (X, Y_mean,
                            legend          = leg_ticks,
                            xticks_labels   = x_ticks,
                            xticks_values   = range (x_len),
                            #ylabel          = VARS_LABELS[args.var_name],
                            ylabel          = "RRT-P (ms)",
                            xlabel          = params_xlabel,
                            file_name       = os.path.join (prefix_path, file_name)
                            #,colors          = {0: "red"}
                            )

            plot = Plot ()
            plot.lines (X_inv, Y_mean_inv,
                            legend          = x_ticks,
                            xticks_labels   = leg_ticks,
                            xticks_values   = range (leg_len),
                            #ylabel          = VARS_LABELS[args.var_name],
                            ylabel          = "RRT-P (ms)",
                            xlabel          = params_xlabel,
                            file_name       = os.path.join (prefix_path, file_name_inv)
                            #,colors         = {0: "red"}
                            #,markerssize    = {0: 10}
                            #,annotates      = annotates
                            #,top_ylim       = top_ylim
                            )

    elif len (args.var_names) == 0:
        for var_name in VARS:
            if var_name in ["responsetime_max", "slow_perc", "no_perc", "total", "things_bits", "sumshareedge", "sumsharecloud"]:
               continue
            print (var_name)
            var_field       = VARS[var_name]


            mean = original_mean
            max = original_max
            std = original_std
            min = original_min
            max = original_max
            if var_name in ["responsetime_mean", "responsetime_max", "responsetime_mean_nono", "responsetime_max_nono"]:
                mean = original_mean * 1000
                max = original_max * 1000

            x_len    = params_lengths[0]
            leg_len  = params_lengths[1]

            x_ticks = params_labels[0]
            leg_ticks = params_labels[1]
            leg_ticks = ["VERTICAL", "EDGE", "RAC"]

            

            # file_name = var_name + "-" + params_names[1] + "-" + params_names[0] + ".pdf"
            # file_name_inv = var_name + "-" + params_names[0] + "-" + params_names[1] + ".pdf"
            file_name = var_name + "-" + params_names[1] + "-" + params_names[0] + ".png"
            file_name_inv = var_name + "-" + params_names[0] + "-" + params_names[1] + ".png"

            X           = np.broadcast_to (np.arange (x_len), (leg_len, x_len))
            Y_mean      = mean[:,:,var_field].transpose ()
            Y_max       = max[:,:,var_field].transpose ()
            Y_std       = std[:,:,var_field].transpose ()
            Y_min       = min[:,:,var_field].transpose ()
            Y_max       = max[:,:,var_field].transpose ()

            X_inv       = np.broadcast_to (np.arange (leg_len), (x_len, leg_len))
            Y_mean_inv  = mean[:,:,var_field]
            Y_max_inv   = max[:,:,var_field]
            Y_std_inv   = std[:,:,var_field]
            Y_min_inv   = min[:,:,var_field]
            Y_max_inv   = max[:,:,var_field]

            xticks_values   = range (x_len)
            xticks_labels   = x_ticks
            legend          = leg_ticks
            #ylabel          = VARS_LABELS[args.var_name]
            ylabel          = VARS_LABELS[var_name]
            xlabel          = params_xlabel[0]

            # percs_h = Y_mean_inv / Y_mean_inv[3,:] - 1
            #
            if var_name == "net_avg":
                print ("Original Y_inv:")
                print (Y_mean_inv)
                # print "Percentages:"
                # print percs_h
                #raw_input ()

            top_ylim = None
            annotates = {}

            # if var_name in ["responsetime_mean", "responsetime_max", "responsetime_mean_nono", "responsetime_max_nono"] and \
            #     Y_mean.shape[1] > 1:
            #     cloud = np.array (Y_mean[:, 0])
            #     edges = Y_mean[:,1:x_len]
            #     max_edges = Y_mean[:,1:x_len].max () + 200
            #     cloud_new = np.repeat (max_edges, len (cloud))
            #
            #     Y_mean[:,0] = cloud_new
            #     annotates = {
            #         "annote_texts"  : ["{:04.2f} s".format (v / 1000) for v in cloud],
            #         "annote_x"      : X_inv[0,:],
            #         "annote_y"      : cloud_new * 0.95
            #     }
            #     top_ylim  = cloud_new.max () * 0.999
            #
            #     print "annotates"
            #     print annotates
            #     #print cloud
            #     #print cloud_new
            #     #print edges
            #     #print max_edges

            print ("var_name:", var_name, "=>")
            print ("file_name:", file_name)
            print ("legend: ", leg_ticks)
            print ("X     : \n", x_ticks)
            print ("Y_mean:\n", Y_mean)
            print ("Y_min:\n", Y_min)
            print ("Y_max:\n", Y_max)
            print ("Y_std:\n", Y_std)

            print ("file_name_inv:", file_name_inv)
            print ("legend: ", x_ticks)
            print ("X     : \n", leg_ticks)
            print ("Y_mean_inv:\n", Y_mean_inv)
            # print "X:\n", X
            # print "Y:\n", Y_mean
            # print "X inv:\n", X_inv
            # print "Y inv:\n", Y_mean_inv
            # print x_len, leg_len
            # print xticks_labels
            # print xticks_values
            # print legend
            # print ylabel

            if var_name in ["total"]:
                if "nrequests" in params_names:
                    i = params_names.index ("nrequests")
                    nrequests = params_values[i]
                    Y_mean = np.array ([nrequests]).transpose () - Y_mean
                    Y_mean_inv = np.array ([nrequests]) - Y_mean_inv

            max_y = Y_mean_inv.max ()
            y_ticks = []
            if max_y <= 1:
                y_ticks = np.arange (0, 0.1, 0.01)
            elif max_y <= 100:
                # y_ticks = np.append (np.arange (0, max_y, 10), np.array (max_y))
                y_ticks = np.arange (0, max_y, 10)
            elif max_y <= 1000:
                # y_ticks = np.append (np.arange (0, max_y, 100), np.array (max_y))
                y_ticks = np.arange (0, max_y, 100)
            elif max_y <= 10000:
                # y_ticks = np.append (np.arange (0, max_y, 250), np.array (max_y))
                y_ticks = np.arange (0, max_y, 250)
            elif max_y <= 100000:
                # y_ticks = np.append (np.arange (0, max_y, 1000), np.array (max_y))
                y_ticks = np.arange (0, max_y, 5000)
            else:
                y_ticks = np.append (np.arange (0, max_y, 100000), np.array (max_y))

            if var_name in ["total"]:
                y_ticks = np.arange (0, 200, 50)
            if var_name in ["responsetime_mean", "responsetime_mean_nono"]:
                y_ticks = np.arange (200, 750, 50)
            if var_name in ["responsetime_max", "responsetime_max_nono"]:
                y_ticks = np.arange (200, 4000, 200)
            if var_name in ["load_std"]:
                y_ticks = np.arange (0, 8, 1)
            if var_name in ["cloud_perc", "slow_perc"]:
                y_ticks = np.arange (0, 1, 0.1)

            print ("yticks\n", y_ticks)#, raw_input (

            # data_file_name_inv = os.path.join (prefix_path, file_name_inv).replace (".pdf", ".txt")
            # data_file_name     = os.path.join (prefix_path, file_name).replace (".pdf", ".txt")
            data_file_name_inv = os.path.join (prefix_path, file_name_inv).replace (".png", ".txt")
            data_file_name     = os.path.join (prefix_path, file_name).replace (".png", ".txt")
            print (data_file_name_inv)
            print (data_file_name)
            print (X)
            print (Y_mean)
            print (Y_mean_inv)
            with open(data_file_name, 'wb') as handle:
                pickle.dump({"X": X, "Y":Y_mean}, handle)
            with open(data_file_name_inv, 'wb') as handle:
                pickle.dump({"X": X, "Y":Y_mean_inv}, handle)

            plot = Plot ()
            plot.lines (X, Y_mean
                            ,legend          = leg_ticks
                            ,xticks_labels   = x_ticks
                            ,xticks_values   = range (x_len)
                            # ,title           = prefix_path
                            # ,ylabel          = VARS_LABELS[args.var_name],
                            ,xlabel          = xlabel
                            ,ylabel          = VARS_LABELS[var_name]
                            ,file_name       = os.path.join (prefix_path, file_name)
                            # ,colors          = {0: "red"}
                            )


            plot = Plot ()
            plot.lines (X_inv, Y_mean_inv
                            ,legend          = x_ticks
                            ,xticks_labels   = leg_ticks
                            ,xticks_values   = range (leg_len)
                            # ,title           = prefix_path
                            ,yticks_labels   = y_ticks
                            ,yticks_values   = y_ticks
                            #ylabel          = VARS_LABELS[args.var_name],
                            ,ylabel          = VARS_LABELS[var_name]
                            ,xlabel          = xlabel
                            ,file_name       = os.path.join (prefix_path, file_name_inv)
                            ,colors         = {0: "red"}
                            ,markerssize    = {0: 10}
                            ,annotates      = annotates
                            # ,top_ylim       = top_ylim
                            )

   # def read (self):
    #     self.means = []
    #     self.maxs = []
    #
    #     for rounds_names in self.files_names:
    #         paths = []
    #         for file_name  in rounds_names:
    #             paths.append (self.prefix_path + file_name + "." + self.extension)
    #
    #         table_list = self.parser.read (paths, type=self.parser.MULTIPLE_FILE)
    #         #print table_list
    #         #print rounds_names
    #         # print table_list, raw_input ()
    #
    #         mean_list = []
    #         max_list = []
    #         for table in table_list:
    #             mean_list.append (table.mean (axis=0))
    #             max_list.append (table.max (axis=0))
    #
    #         self.means.append (np.array(mean_list))
    #         self.maxs.append (np.array (max_list))

