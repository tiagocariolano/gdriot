import numpy as np
from collections import OrderedDict

np.set_printoptions (precision=2, linewidth=100, suppress=True)

PREFIX_PATH         = "./results/backup-reqr-2-decimal-points/exp1-reqrate-0.01/"
FILE_EXTENSION      = "pickle"
DIM_N               = 1
DIM_M               = 33
PARAMS_VALUES       = {
                        "linktype":     ["HIERARCHY"],
                        "freshness":    [0.0, 1.0, 2.0, 3.0, 4.0],
                        "nedge":       [4, 6, 8, 10, 12],
                        "reqrate":      [0.009, 0.01, 0.03, 0.05, 0.07],
                        "nrequest":     [200, 400, 600, 800, 1000],
                        "strategy":   ["vertical", "edge", "rac"],
                        "ndatatype":   [2, 4, 8, 16],
                        "round":        range (0, 2)
}
PARAMS_FORMATS      = {
                        "linktype":     "6.6s",
                        "freshness":    "05.2f",
                        "nedge":       "03d",
                        "reqrate":      "06.3f",
                        "nrequest":    "04d",
                        "strategy":   "s",
                        "ndatatype":   "03d",
                        "round":       "03d"
}
PARAMS_LABELS       = {
                        "linktype":     ("Horizontal"),#("Cloud", "Vertical-Colab.", "Horizontal-Colab.", "Hierarchy"),
                        "freshness":    tuple (PARAMS_VALUES["freshness"]),
                        "nedge":       tuple (PARAMS_VALUES["nedge"]),
                        "reqrate":      tuple (PARAMS_VALUES["reqrate"]),
                        "nrequest":    tuple (PARAMS_VALUES["nrequest"]),
                        "strategy":      tuple (PARAMS_VALUES["strategy"]),
                        "ndatatype":      tuple (PARAMS_VALUES["ndatatype"]),
                        "round":        tuple (PARAMS_VALUES["round"])
}
PARAMS              = {
                        "linktype":
                            {   "value":    PARAMS_VALUES["linktype"],
                                "length":   len (PARAMS_VALUES["linktype"]),
                                "format":   PARAMS_FORMATS["linktype"],
                                "label":    PARAMS_LABELS["linktype"],
                                "xlabel":   "Scenario"},
                        "freshness":
                            {   "value":    PARAMS_VALUES["freshness"],
                                "length":   len (PARAMS_VALUES["freshness"]),
                                "format":   PARAMS_FORMATS["freshness"],
                                "label":    PARAMS_LABELS["freshness"],
                                "xlabel":   "VDF (Value of Data Freshness)"},
                        "nedge":
                            {   "value":    PARAMS_VALUES["nedge"],
                                "length":   len (PARAMS_VALUES["nedge"]),
                                "format":   PARAMS_FORMATS["nedge"],
                                "label":    PARAMS_LABELS["nedge"],
                                "xlabel":   "NEN (Number of Edge Nodes)"},
                        "reqrate":
                            {   "value":    PARAMS_VALUES["reqrate"],
                                "length":   len (PARAMS_VALUES["reqrate"]),
                                "format":   PARAMS_FORMATS["reqrate"],
                                "label":    PARAMS_LABELS["reqrate"],
                                "xlabel":   "RAR (Request Arriving Rate)"#"Request Generation Rate (Request per Edge Node / Second)"
                            },
                        "nrequest":
                            {   "value":    PARAMS_VALUES["nrequest"],
                                "length":   len (PARAMS_VALUES["nrequest"]),
                                "format":   PARAMS_FORMATS["nrequest"],
                                "label":    PARAMS_LABELS["nrequest"],
                                "xlabel":   "NAR (Number of Arrived Requets)"
                            },
                        "strategy":
                            {   "value":    PARAMS_VALUES["strategy"],
                                "length":   len (PARAMS_VALUES["strategy"]),
                                "format":   PARAMS_FORMATS["strategy"],
                                "label":    PARAMS_LABELS["strategy"],
                                "xlabel":   "STR (Strategy)"
                            },
                        "ndatatype":
                            {   "value":    PARAMS_VALUES["ndatatype"],
                                "length":   len (PARAMS_VALUES["ndatatype"]),
                                "format":   PARAMS_FORMATS["ndatatype"],
                                "label":    PARAMS_LABELS["ndatatype"],
                                "xlabel":   "NDT (Number of Data Type )"
                            },
                        "round":
                            {   "value":    PARAMS_VALUES["round"],
                                "length":   len (PARAMS_VALUES["round"]),
                                "format":   PARAMS_FORMATS["round"],
                                "label":    PARAMS_LABELS["round"],
                                "xlabel":   "Rounds"}
}
VARS                = {
                        "responsetime_mean"     :   0,
                        "responsetime_max"      :   1,
                        "fast_perc"             :   2,
                        "slow_perc"             :   3,
                        "cloud_perc"            :   4,
                        "no_perc"               :   5,
                        "total"                 :   6,
                        "net_avg"               :   7,
                        "net_peak"              :   8,
                        "net_nosent"            :   9,
                        "things_bits"           :   10,
                        "things_energy"         :   11,
                        "responsetime_mean_nono":   12,
                        "responsetime_max_nono" :   13,
                        "sumshareedge"          :   14,
                        "sumsharecloud"         :   15,
                        "load_std"              :   18,
                        "eff_energy"            :   24,
                        "tot_n_probe"           :   25,
                        "tot_n_forwarding"      :   26,
                        "tot_n_message"         :   29,
                        "tot_message_size"      :   30,
                        "avg_utilization"       :   31,
                        "std_utilization"       :   32
                        }
VARS_LABELS         = {
                        "responsetime_mean" :   "a. RT-AR (ms)",
                        "responsetime_max"  :   " . RT-AR (ms)",
                        "fast_perc"         :   "b. RP-ET (%)",
                        "slow_perc"         :   "PFR-RQ (%) (things)",
                        "cloud_perc"        :   " . RPCN (%)",
                        "no_perc"           :   "Non-Fulfilled (%)",
                        "total"             :   "Total Requests",
                        "net_avg"           :   "c. RT-ET (messages)",
                        "net_peak"          :   "Max Network Queue Size",
                        "net_nosent"        :   "Network Queue Size",
                        "things_bits"       :   "Total Transmitted Data in Things Tier (byte) ",
                        "things_energy"     :   "e. EC-TT (mJ)",
                        "responsetime_mean_nono" :   "Response Time (ms)",
                        "responsetime_max_nono"  :   "Response Time (ms)",
                        "sumshareedge"      :   "Total Edge Tier Sharing",
                        "sumsharecloud"     :   "Total Cloud Offloaded",
                        "load_std"          :   ". RBEN",
                        "eff_energy"        :   "d. EE-ET",
                        "tot_n_probe"       :   "h. PN-ET (probes)",
                        "tot_n_forwarding"  :   "g. FN-ET (forwardings)",
                        "tot_n_message"     :   "a. FPN-ET (messages)",
                        "tot_message_size"  :   "d. FPS-ET (bytes)",
                        "avg_utilization"   :   "f. AU-ET (%)",
                        "std_utilization"   :   " . STDU",

}

default_args = OrderedDict ([
                ("linktype",    "HIERARCHY"),
                ("freshness",    1),
                ("nedges",       8),
                ("reqrate",      0.01),
                ("ndatatypes",   8),
                ("nrequests",     1000)
])
