from packet import Packet

class Request (Packet):

	def __init__(self, id, cycle, mem, data_type, freshness, source):
		self.id = id
		self.cycle =  cycle
		self.mem = mem
		self.data_type = data_type
		self.freshness = freshness
		self.source = source
		self.type = self.EDGE_REQUEST

		self.path = []

		self.t_start = 0
		self.send_delay = .0
		self.recv_delay = .0
		self.hop = 0
		self.hopedge = 0
		self.hopcloud = 0
		self.price = 0
		self.destination = None
		self.data = None
		self.edge_origin = None

		self.inst = self.DEFAULT_INST
		self.bytes = self.DEFAULT_BYTES

		self.critical_path = []

	def set_t_start (self, t_start):
		self.t_start = t_start

	def set_freshness (self, freshness):
		self.freshness = freshness
	
	def set_data_type (self, data_type):
		self.data_type = data_type

	def set_data (self, data):
		self.data = data
		self.bytes = self.DEFAULT_BYTES + self.data.bytes

	def __str__ (self):
		GB = GHz = 10 ** 9
		MB = MHz = 10 ** 6
		KB = KHz = 10 ** 3
		return 	"{:3s} {:05d}: {:010.4f}, {:010.8f}, {:6s}, {:010.6f}".format (
																				"REQ", 
																				self.id, 
																				float (self.cycle) / MHz,
																				float (self.mem) / MB,
																				self.data_type.type,
																				self.t_start)
