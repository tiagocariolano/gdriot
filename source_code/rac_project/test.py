import os
import sys
import pickle
import re

folder = "./results/backup-reqr-2-decimal-points/exp1-reqrate-0.01/"
param = "ndatatype"
fname_list = [
"responsetime_mean-strategy-" + param + ".txt",
"fast_perc-strategy-" + param + ".txt",
"net_avg-strategy-" + param + ".txt",
"eff_energy-strategy-" + param + ".txt",
"things_energy-strategy-" + param + ".txt",
"avg_utilization-strategy-" + param + ".txt",
"tot_n_forwarding-strategy-" + param + ".txt",
"tot_n_probe-strategy-" + param + ".txt"
]
fname_min_improve = [
    "responsetime_mean-strategy-" + param + ".txt",
    "net_avg-strategy-" + param + ".txt",
    "things_energy-strategy-" + param + ".txt",
    "tot_n_forwarding-strategy-" + param + ".txt",
    "tot_n_probe-strategy-" + param + ".txt"
]

fname_max_improve = [
    "fast_perc-strategy-" + param + ".txt",
    "eff_energy-strategy-" + param + ".txt",
    "avg_utilization-strategy-" + param + ".txt",
]

pattern = ".txt$"

for fname in fname_list:
    path = os.path.join (folder, fname)
    f = open (path, "rb")
    data = pickle.load (f, encoding="latin1")

    # print (fname)
    # print (data["Y"])
    if fname in fname_min_improve:
        improve0 = max ((data["Y"][0] - data["Y"][2]) / data["Y"][0])
        improve1 = max ((data["Y"][1] - data["Y"][2]) / data["Y"][1])
    elif fname in fname_max_improve:
        improve0 = max ((data["Y"][2] - data["Y"][0]) / data["Y"][0])
        improve1 = max ((data["Y"][2] - data["Y"][1]) / data["Y"][1])
    
    # print (fname, improve0, improve1)
    print ("{:.10s} -> ".format (fname), end=""), 
    print ("{:8.6f}, {:8.6f}".format (improve0, improve1))
    


