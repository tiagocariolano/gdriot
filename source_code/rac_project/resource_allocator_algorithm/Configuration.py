import math
import numpy as np
import util
from voronoi import VoronoiEMCNew
from hierarchy import gen_coords_links_by_level
from utils import *
from util import DEBUG


class Configuration():
    def __init__(self, parameters, index_to_cloud, index_to_source):
        self.__parameters = parameters
        self._index_to_cloud = index_to_cloud
        self._index_to_source = index_to_source

        if self.__parameters["link_type"] == HIERARCHY:
            levels = self.__parameters["levels"]
            self._entities_coordinates, self._entities_connections, n, M = \
                gen_coords_links_by_level ( levels,
                                            nnodes=self.__parameters["n_entities"],
                                            xMax=self.__parameters["max_x"],
                                            yMax=self.__parameters["max_y"])
            smaller_position = M.index(min(M))
            self._index_to_cloud.append(smaller_position)
            for i, coordinates in enumerate(self._entities_coordinates):
                coordinates.append(M[i])
            self.__parameters["n_entities"] = n
        else:
            self._entities_connections = self.__generate_entities_connections()
            self._entities_coordinates = self.__generate_entities_coordinates()

        if self.__parameters["link_type"] == ONLY_CLOUD:
            self._psans_coordinates = self.__generate_psan_coordinates_for_cloud()
        else:
            self._psans_coordinates = self.__generate_psan_coordinates()

    def __generate_entities_connections(self):
        if self.__parameters["link_type"] == VERTICAL or self.__parameters["link_type"] == ONLY_CLOUD:
            return []
        elif self.__parameters["link_type"] == LINE or self.__parameters["link_type"] == LINE_RANDOM:
            return self.__generate_line_connections ()
        else:
            print "Error: Topology type " + self.__parameters["link_type"] + " is not defined."
            exit (1)

    def __generate_entities_coordinates(self):
        if self.__parameters["link_type"] == ONLY_CLOUD:
            return []
        if self.__parameters["link_type"] == VERTICAL:
            return self.__generate_none_coordinates()
        elif self.__parameters["link_type"] == LINE:
            return self.__generate_line_coorinates ()
        elif self.__parameters["link_type"] == LINE_RANDOM:
            return self.__generate_line_random_coorinates ()
        else:
            print "Error: Topology type " + self.__parameters["link_type"] + " is not defined."
            exit (1)

    def __generate_line_connections (self):
        connections = []
        for i in xrange (self.__parameters["n_entities"] - 1):
            connections.append ((i, i + 1))
        return connections

    def __generate_none_coordinates (self):
        coordinates = []
        for _ in xrange (self.__parameters["n_entities"]):
            c = (util.random.randrange(RANGE_COORDINATES[0], RANGE_COORDINATES[1]), util.random.randrange (RANGE_COORDINATES[0], RANGE_COORDINATES[1]), 1)
            coordinates.append (c)
        return coordinates

    def __generate_line_coorinates(self):
        max_x  = self.__parameters["max_x"]
        y = self.__parameters["max_y"] / 2
        coordinates = [(0, y)]
        for x in xrange (1, self.__parameters["n_entities"]):
            coordinates.append ((max_x / self.__parameters["n_entities"] * x, y, 1))
        return coordinates

    def __generate_line_random_coorinates(self):
        coordinates = []
        max_y = self.__parameters["max_y"]
        for x in xrange (self.__parameters["n_entities"]):
            coordinates.append ([max_y / self.__parameters["n_entities"] * x, util.random.random () * max_y, 1])
        return coordinates

    def __generate_psan_coordinates(self):
        if len(self._entities_coordinates) == 0:
            return {}
        voronoi = VoronoiEMCNew (np.asarray (map(lambda x: (x[0], x[1]), self._entities_coordinates)))

        psans_of_entity = {}

        for i, _ in enumerate(self._entities_coordinates):
            psans_of_entity[i] = []
            while len(psans_of_entity[i]) < self.__parameters["quant_psans"]:
                coordX = util.random.randint (int (voronoi.minBound[0]), int (voronoi.maxBound[0]))
                coordY = util.random.randint (int (voronoi.minBound[1]), int (voronoi.maxBound[1]))

                entity_index, _ = voronoi.get_region_by_point ([coordX, coordY])

                if isinstance(entity_index, int) and not math.isnan(entity_index):
                    psans_of_entity[i].append((coordX, coordY))
        if DEBUG:
            voronoi.plot_setup()
            voronoi.plot_save("outputfiles/voronoi")
        return psans_of_entity

    def __generate_psan_coordinates_for_cloud(self):
        psans_of_entity = {"cloud": []}

        for _ in xrange(self.__parameters["quant_psans"]):
            coordX = util.random.randint (RANGE_COORDINATES[0], RANGE_COORDINATES[1])
            coordY = util.random.randint (RANGE_COORDINATES[0], RANGE_COORDINATES[1])

            psans_of_entity["cloud"].append((coordX, coordY))
        return psans_of_entity

    def __generate_links(self, cloud_entity, source_entity):
        '''links = []

        if self.__parameters["link_type"] == ONLY_CLOUD:
            link = {"s": source_entity["id"],
                "d": cloud_entity["id"],
                "BW": BW,
                "PR": CLOUD_SOURCE_DELAY_PROP
            }
            links.append(link)

            for psan in self._psan_entities["cloud"]:
                link = {"s": cloud_entity["id"],
                    "d": psan["id"],
                    "BW": BW,
                    "PR": CLOUD_PSAN_DELAY_PROP
                }
                links.append(link)
            links.append(link)
        else:
            for i, entity in enumerate(self._entities):
                id_1 = entity["id"]
                if not self._index_to_cloud or i in self._index_to_cloud:
                    link = {"s": id_1,
                        "d": cloud_entity["id"],
                        "BW": BW,
                        "PR": CLOUD_EDGE_DELAY_PROP
                    }
                    links.append(link)
                if not self._index_to_source or i in self._index_to_source:
                    link = {"s": source_entity["id"],
                        "d": id_1,
                        "BW": BW,
                        "PR": EDGE_SOURCE_DELAY_PROP
                    }
                    links.append(link)

            for key, connection in enumerate(self._entities_connections):
                id_1 = self._entities[connection[0]]["id"]
                id_2 = self._entities[connection[1]]["id"]

                c1 = self._entities_coordinates[connection[0]]
                c2 = self._entities_coordinates[connection[1]]
                #print self.distance (c1, c2) / WIRED_WAVE_SPEED, raw_input ()
                link = {"s": id_1,
                    "d": id_2,
                    "BW": BW,
                    "PR": self.distance (c1, c2) / WIRED_WAVE_SPEED
                }
                links.append(link)

            for key, edge_entity in enumerate(self._entities):
                for psan in self._psan_entities[key]:
                    link = {"s": edge_entity["id"],
                        "d": psan["id"],
                        "BW": BW,
                        "PR": EDGE_PSAN_DELAY_PROP
                    }
                    links.append(link)
        return links'''
