__id = -1
def next_id ():
    global __id
    __id += 1
    return __id

GB = GHz = GWz = 1 * 2 ** 30
MB = MHz = MWz = 1 * 2 ** 20
KB = KHz = KWz = 1 * 2 ** 10

GB = GHz = 10 ** 9
MB = MHz = 10 ** 6
KB = KHz = 10 ** 3

_bytes = 100 * MB
STORAGE = 100
PROPAGATION_DELAY = 0
BW = 1 * 10 ** 2

MAX_X_Y = 100000
RANGE_COORDINATES = (0, 100000)          #m
CLOUD_COORDINATES = [100000, 100000]  #m

WIRELESS_WAVE_SPEED = LIGHT_SPEED = 3 * 10 ** 8             #m/s
WIRED_WAVE_SPEED = 0.59 * LIGHT_SPEED                       #http://stason.org/TULARC/networking/lans-ethernet/3-11-What-is-propagation-delay-Ethernet-Physical-Layer.html
CLOUD_PSAN_DISTANCE_DEF = 10 ** 5   #m
CLOUD_EDGE_DISTANCE_DEF = 10 ** 5   #m
EDGE_SOURCE_DISTANCE_DEF = 10 ** 2  #m
EDGE_PSAN_DISTANCE_DEF = 10 * 1     #m
CLOUD_SOURCE_AVG_N_LINKS = CLOUD_EDGE_AVG_N_LINKS = 10

CLOUD_PSAN_DELAY_PROP = CLOUD_PSAN_DISTANCE_DEF / WIRELESS_WAVE_SPEED #s
CLOUD_EDGE_DELAY_PROP = (CLOUD_EDGE_DISTANCE_DEF / WIRED_WAVE_SPEED) * CLOUD_EDGE_AVG_N_LINKS #s
CLOUD_SOURCE_DELAY_PROP = 2 * CLOUD_EDGE_DELAY_PROP

#EDGE_SOURCE_DELAY_PROP = EDGE_SOURCE_DISTANCE_DEF / WIRED_WAVE_SPEED
#EDGE_PSAN_DELAY_PROP = EDGE_PSAN_DISTANCE_DEF / WIRELESS_WAVE_SPEED
##########################################################################

# https://ieeexplore.ieee.org/document/8029252
EDGE_PSAN_DELAY_PROP = 0.0015       # s
CLOUD_EDGE_DELAY_PROP = 0.035     # s
CLOUD_PSAN_DELAY_PROP = EDGE_PSAN_DELAY_PROP + CLOUD_EDGE_DELAY_PROP

CLOUD_SOURCE_DELAY_PROP = CLOUD_PSAN_DELAY_PROP
# EDGE_SOURCE_DELAY_PROP = EDGE_PSAN_DELAY_PROP
EDGE_SOURCE_DELAY_PROP = 0
# EDGE_SOURCE_DELAY_PROP = 0.0015


CLOUD_EDGE_LINK_CHOICES = [1000]# * GB]
PSAN_CLOUD_LINK_CHOICES = [54]# * MB]
EDGE_EDGE_LINK_CHOICES  = [100]# * MB] #https://ieeexplore.ieee.org/document/8029252
EDGE_PSAN_LINK_CHOICES  = [54]# * MB]
CLOUD_SOURCE_LINK = 54
EDGE_SOURCE_LINK = 100  
# EDGE_SOURCE_LINK = 54

default_entity_parameters = {
    "cpuRate": 2 * GHz,
    "MEMORY": 4 * GB,
    "ENERGY": 10 * KWz,
    "COST": 1,
    "WATT": 20.0,

    # "EDGE_MEMORY" : [500 * KB, 2 * GB],
    "EDGE_MEMORY" : [1 * GB, 4 * GB],
    "EDGE_CPU" : [1 * GHz],
    # "SOURCE_REQ_CYCLE" : [  500 * MHz, 600 * MHz, 700 * MHz, 800 * MHz, 900 * MHz,
    #                         1000 * MHz, 1100 * MHz, 1200 * MHz, 1300 * MHz, 1400 * MHz, 1500 * MHz], #https://ieeexplore.ieee.org/document/8029252
    # "SOURCE_REQ_CYCLE" : [10 * MHz],
    # "SOURCE_REQ_RAM"   : range (20, 40), #https://ieeexplore.ieee.org/document/8029252
    # "SOURCE_REQ_PRICE" : [10, 25, 50, 75, 100]
}

ONLY_CLOUD = "ONLY_CLOUD"
VERTICAL = "VERTICAL"
LINE = "LINE"
LINE_RANDOM = "LINE_RANDOM"
HIERARCHY = "HIERARCHY"
