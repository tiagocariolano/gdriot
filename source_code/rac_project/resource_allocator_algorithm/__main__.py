import sys
import os
import argparse
import logging.config
import pickle as pk
import numpy as np
import util
from yafs.core import Sim
from yafs.topology import Topology
from yafs.stats import Stats
from SetupManager import SetupManager
from charts import distribution_function_averages
from utils import *
from util import DEBUG


def main(parameters):
    setup_manager = SetupManager(parameters)

    results, separete_result = setup_manager.get_results()

    # for request in results["Requests"]:
    #     print request[-3], request[-2].type, request[-1], request[2]
    tot_time = 0.
    for en_id in separete_result:
        if separete_result[en_id]["tot_time"] > tot_time:
            tot_time = separete_result[en_id]["tot_time"]

    # Forwarding Energy Consumption is calculated assuming all links have the same bandwidth and
    # receiving and sending powers are equal (p_send =  p_recv) (see where is * below)

    E_forwarding_sum = 0.
    bandwidth = EDGE_EDGE_LINK_CHOICES[0] * 1000000.
    eff_ener_sum = 0
    energy_efficiency_sum = 0.
    count = 0
    tot_req = 0
    tot_n_forwarding = 0
    tot_n_probe = 0
    tot_probe_size = 0.
    tot_forwarding_size = 0.
    utilization_list = []
    for en_id in separete_result:
        if en_id == "CLOUD":
            continue
        forwarding_delay = float (separete_result[en_id]["forwarding_size"]) * 8. / bandwidth #*
        p_send = separete_result[en_id]["p_send"] #*
        E_forwarding = p_send * forwarding_delay #*
        E_forwarding_sum += E_forwarding
        
        energy = separete_result[en_id]['p']
        p_idle = separete_result[en_id]['p_idle']
        p_max = separete_result[en_id]['p_max']
        utilization = separete_result[en_id]["utilization"]
        p_utilization = p_idle + (p_max - p_idle) * utilization
        energy_edge = p_utilization * tot_time

        tot_mem_req = 0.
        tot_cycle_req = 0.
        for req_res in separete_result[en_id]["Requests"]:
            tot_mem_req += req_res[9]
            tot_cycle_req += float (req_res[10]) / float (1e+6)

        tot_req += len (separete_result[en_id]["Requests"])
        eff_ener_sum += float (tot_mem_req) / energy if energy > 0 else 0
        # energy_efficiency_sum += float (tot_cycle_req) / energy_edge if energy_edge > 0 else 0
        energy_efficiency_sum += float (tot_cycle_req) / (energy_edge + E_forwarding) if (energy_edge + E_forwarding) > 0 else 0
        E_forwarding_sum += E_forwarding
        count += 1
        print "Edge ", en_id, " ", len (separete_result[en_id]["Requests"])

        tot_n_forwarding += separete_result[en_id]["n_forwarding"]
        tot_n_probe += separete_result[en_id]["n_probe"]
        tot_forwarding_size += separete_result[en_id]["forwarding_size"]
        tot_probe_size += separete_result[en_id]["probe_size"]
        utilization_list.append (separete_result[en_id]["utilization"])
    
    tot_n_message = tot_n_forwarding + tot_n_probe
    tot_message_size = tot_forwarding_size + tot_probe_size
    std_utilization = np.array (utilization_list).std ()
    avg_utilization = np.array (utilization_list).mean ()
    
    eff_ener_avg = float (eff_ener_sum) / count
    energy_efficiency_avg = float (energy_efficiency_sum) / float (count)
    eff_ener_avg = energy_efficiency_avg
    print "tot_req edge: ", tot_req
    print "eff_ener_avg: ", eff_ener_avg
    print "energy_efficiency_avg: ", energy_efficiency_avg
    print "E_forwarding_sum: ", E_forwarding_sum
    print "tot_time: ", tot_time
    print "tot_n_forwarding: ", tot_n_forwarding
    print "tot_n_probe: ", tot_n_probe
    print "tot_forwarding_size: ", tot_forwarding_size
    print "tot_probe_size: ", tot_probe_size
    print "std_utilization: ", std_utilization
    print "avg_utilization: ", avg_utilization
    
    load_tot_req = []
    load_size_req = []
    load_cycle_req = []
    load_density_req = []
    edge_id_to_request = {}

    for en_id in separete_result:
        sum_tot_req = sum_size_req = sum_cycle_req = sum_density_req = 0
        if (en_id == "CLOUD"):
            continue
        sum_tot_req += len (separete_result[en_id]["Requests"])
        edge_id_to_request[en_id] = 0
        for req_res in separete_result[en_id]["Requests"]:
             sum_size_req += req_res[9]
             sum_cycle_req += float (req_res[10]) / req_res[11]
             sum_density_req += (float (req_res[11] / 1000000000) * (float (req_res[1]) - req_res[12]))
             edge_id_to_request[en_id] += 1
             # print (float (req_res[11]), (float (req_res[1]) - req_res[12])), raw_input ()
        # print en_id, sum_density_req, n_density_req, raw_input ()
        load_tot_req.append (float (sum_tot_req))
        load_size_req.append (float (sum_size_req))
        load_cycle_req.append (float (sum_cycle_req))
        load_density_req.append (float (sum_density_req))

    load_tot_req = np.array (load_tot_req)
    load_size_req = np.array (load_size_req)
    load_cycle_req = np.array (load_cycle_req)
    load_density_req = np.array (load_density_req)

    load_tot_req_std = load_tot_req.std ()
    load_size_req_std = load_size_req.std ()
    load_cycle_req_std = load_cycle_req.std ()
    load_density_req_std = load_density_req.std ()
    load_tot_req_avg = load_tot_req.mean ()
    load_size_req_avg = load_size_req.mean ()
    load_cycle_req_avg = load_cycle_req.mean ()
    load_density_req_avg = load_density_req.mean ()
    load_tot_req_cv = load_tot_req_std / load_tot_req_avg
    load_size_req_cv = load_size_req_std / load_size_req_avg
    load_cycle_req_cv = load_cycle_req_std / load_size_req_avg
    load_density_req_cv = load_density_req_std / load_density_req_avg
    
    print "load_tot_req", load_tot_req
    print "load_tot_req_std", load_tot_req_std
    print "load_tot_req_cv", load_tot_req_cv
    print "load_size_req_std", load_size_req_std
    print "load_size_req_cv", load_size_req_cv
    # print "load_tot_req\n", load_tot_req, load_tot_req.std (), load_tot_req.var (), load_tot_req.sum ()
    # print "load_size_req\n", load_size_req, load_size_req.std (), load_size_req.var (), load_size_req.sum ()
    # print "load_cycle_req\n", load_cycle_req, load_cycle_req.std (), load_cycle_req.var (), load_cycle_req.sum ()
    # print "load_density_req\n", load_density_req, load_density_req.std (), load_density_req.var (), load_density_req.sum ()
    # print sum_load, raw_input ()
    # print separete_result[en_id]["Requests"], raw_input ()

    m = Stats(defaultPath=parameters["output_path"])

    network_results = {"GeneralNetwork": {
                        "avg" : m.average_messages_not_transmitted (),
                        "peak" : m.peak_messages_not_transmitted (),
                        "notsent" : m.messages_not_transmitted ().values[0],
                        "time" : m.time_messages_not_trasmitted ().values,
                        "buffer": m.buffer_messages_not_transmitted ().values
                        }}

    makespans = [r[2] for r in results["Requests"]]
    # print results["Requests"]
    # print len (makespans), makespans
    makespansnono = []
    sumshare = 0
    sumshareedge = 0
    sumsharecloud = 0
    sumreq10 = []
    sumreq50 = []
    sumreq90 = []
    sumreq100 = []
    all_sums = {}
    all_n = {}
    for r in results["Requests"]:
        #print r[3], r[5], r[2]
        if r[3] != "NOT_FULFILLED":
            sumshare += r[5]
            sumshareedge += r[6]
            sumsharecloud += r[7]
            makespansnono.append ( r[2])

    print "sumshare: ", sumshare
    print "sumshareedge: ", sumshareedge
    print "sumsharecloud: ", sumsharecloud


    makespans_mean = float (sum (makespans)) / len (makespans)
    makespans_max = max (makespans)
    makespans_mean_nono = float (sum (makespansnono)) / len (makespansnono) if len (makespansnono) > 0 else 0
    makespans_max_nono = max (makespansnono) if len (makespansnono) > 0 else 0

    fast = slow = cloud = no = 0
    sumfast = sumslow = sumcloud = sumno = 0
    edgems = []
    cloudms = []
    # print results["Requests"]
    for r in results["Requests"]:
        if r[3] == "FAST_FULFILLED":
            fast += 1
            sumfast += r[2]
            edgems.append (r[2])
        elif r[3] == "SLOW_FULFILLED":
            slow += 1
            sumslow += r[2]
            edgems.append (r[2])
        elif r[3] == "CLOUD_FULFILLED":
            cloud += 1
            sumcloud += r[2]
            cloudms.append (r[2])
        elif r[3] == "NOT_FULFILLED":
            no += 1
            sumno += r[2]

    makespan_mean = (sumfast + sumslow + sumcloud) / (fast + slow + cloud)

    #print "EDGE", edgems
    #print "CLOUD", cloudms
    total = fast + slow + cloud + no

    #if parameters["link_type"] != ONLY_CLOUD:
    #    no = no + cloud

    #constants joule
    E_elec = 50 * 1e-9  # J/b/m^2 or 50 nJ/b/m^2
    e_amp = 10 * 1e-12  # J/b/m^2 or 10 pJ/b/m^2
    x = 2
    
    I_sens = 0.001      # A or 1 mA
    V_sup = 1           # V

    E_rx_sum = .0
    E_tx_sum = .0       #
    E_sens_sum = .0     # J
    total_bits = 0
    E_rx_sum_nono = .0
    E_tx_sum_nono = .0
    total_bits_nono = .0

    for edge_energy in results["Energy"]:
        #print edge_energy
        for psan_energy in edge_energy:
            #print edge_energy[psan_energy]
            bits_r = edge_energy[psan_energy]["data_r"] * 8
            bits_s = edge_energy[psan_energy]["data_s"] * 8
            #distance = edge_energy[psan_energy]["distance"] * 0.001
            distance = edge_energy[psan_energy]["distance"]
            delay = edge_energy[psan_energy]["delay"]

            E_tx = E_elec * bits_s + e_amp * bits_s * distance ** x
            E_tx_sum += E_tx

            E_sens_sum += bits_s * V_sup * I_sens * delay

            E_rx = E_elec * bits_r
            E_rx_sum += E_rx
            total_bits += bits_s

    E_things = E_rx_sum + E_tx_sum + E_sens_sum
    print "E_rx_sum: ", E_rx_sum
    print "E_tx_sum: ", E_tx_sum
    print "total_bits: ", total_bits



    print parameters["link_type"]
    print "Makespans-Mean       :{:6.6f}, {:06d}".format (makespans_mean, len (makespans))
    print "Makespans-MeanNONO   :{:6.6f}, {:06d}".format (makespans_mean_nono, len (makespansnono))
    print "Fast                 :{:6.6f}".format (float (fast) / total)
    print "Slow                 :{:6.6f}".format (float (slow) / total)
    print "Cloud                :{:6.6f}".format (float (cloud) / total)
    print "No                   :{:6.6f}".format (float (no) / total)
    if fast + slow > 0:
        print "Makespan Edge        :{:6.6f}".format (float (sumslow + sumfast) / (fast+slow))
    else:
        print "Makespan Edge        : - "
    if cloud > 0:
        print "Makespan Cloud       :{:6.6f}".format (float (sumcloud) / cloud)
    else:
        print "Makespan Cloud       : - "

    results["Stats"] = [makespans_mean,
                        makespans_max,
                        float (fast) / total,
                        float (slow) / total,
                        float (cloud) / total,
                        float (no) / total, #5
                        total,
                        network_results["GeneralNetwork"]["avg"],
                        network_results["GeneralNetwork"]["peak"],
                        network_results["GeneralNetwork"]["notsent"],
                        total_bits, #10
                        E_things,
                        makespans_mean_nono,
                        makespans_max_nono,
                        sumshareedge,
                        sumsharecloud, #15
                        load_tot_req_std,
                        load_size_req_std,
                        load_cycle_req_std,
                        load_density_req_std,
                        load_tot_req_cv, #20
                        load_size_req_cv,
                        load_cycle_req_cv,
                        load_density_req_cv,
                        eff_ener_avg,
                        tot_n_probe, #25
                        tot_n_forwarding,
                        tot_probe_size,
                        tot_forwarding_size,
                        tot_n_message,
                        tot_message_size, #30
                        avg_utilization,
                        std_utilization
                        ]

    del setup_manager._yafs_simulator
    del setup_manager
    return results, separete_result, network_results


if "__main__" == __name__:

    logging.config.fileConfig('./logging.ini')

    parser = argparse.ArgumentParser ()
    parser.add_argument('--strategy'    ,type=str,  dest='strategy',      default="rac")
    parser.add_argument('--seed',       type=int,	dest='seed',          default=3)
    parser.add_argument('--outpath',    type=str,	dest='out_path',      default="./out")
    parser.add_argument('--nrequests',  type=int,	dest='n_requests',    default=1000)
    parser.add_argument('--reqrate',    type=float,	dest='req_rate',      default=1)
    parser.add_argument('--ndatatypes', type=int,	dest='n_datatypes',   default=8)   
    parser.add_argument('--freshness',  type=float,	dest='freshness',     default=00)
    parser.add_argument('--nedges',     type=int,	dest='n_edges',       default=8)
    parser.add_argument('--linktype',   type=str,	dest='link_type',     default="HIERARCHY")
    parser.add_argument('--npsan'       ,type=int,  dest='npsan',         default=4)
    parser.add_argument('--collectrate',type=float,	dest='collect_rate',  default=[0.025, 0.125, 0.5]) #Zeus


    args = parser.parse_args ()

    if args.link_type == "ONLY_CLOUD":
        args.link_type = ONLY_CLOUD
    elif args.link_type == "VERTICAL":
        args.link_type = VERTICAL
    elif args.link_type == "HORIZONTAL":
        args.link_type = LINE_RANDOM
    elif args.link_type == "HIERARCHY":
        args.link_type = HIERARCHY

    util.random.seed(args.seed)
    util.np.random.seed (args.seed)
    
    ## PARAMETERS
    parameters = {
        "simulated_time":       100000,
        "strategy":             args.strategy,
        "RANDOM_SEED":          args.seed,      
        "output_path":          args.out_path,
        "n_request" :           args.n_requests,
        "mean_interval":        args.req_rate,
        "n_datatypes":          args.n_datatypes,
        "freshness":            args.freshness,                        
        "n_entities":           args.n_edges,
        "link_type":            args.link_type,
        "quant_psans":          args.npsan,
        "collect_rate":         args.collect_rate,
        "max_y":                10000,
        "max_x":                10000,
        "type_interval":        "exponential",
        "standard_deviation":   1,
        "levels":               3,
        "EDGE_ENERGY":          [10 * KWz],
        "EDGE_WATT" :           20.0,
        "EDGE_COST":            [1],
        "EDGE_STORAGE":         [500 * GB],
        "EDGE_MEMORY" :         [2 * GB, 2 * GB],
        "EDGE_CPU" :            [1.5 * GHz, 1.5 * GHz],
        "SOURCE_REQ_CYCLE" :    [100 * MHz],#[4 * GHz],#[150 * KHz, 2 * MHz], #AVG 2M #np.repeat (1 * MHz, 99) + [100 * MHz],
        "SOURCE_REQ_RAM"   :    [80 * KB]#[0.000100, 0.080000] #np.arange (0.001, 0.160, 0.001) #https://ieeexplore.ieee.org/document/8029252
        # Relacao REQ_CYCLE <-> EDGE_CPU
        # REQ_CYCLE / EDGE_CPU == 0.002 (requisição de 80 KB, explicacao abaixo)
        # REQ_CYCLE / EDGE_CPU == 0.00015 (requisição de 100 B)
        # Porque em [1]:
        # - tempo de processamento psan = 400 ms (Para requisicao de 80 KB)
        # - tempo de processamento edge = 400 / 200 = 2 ms = 0.002 s
        #   - (tp edge é 200 vezes mais rapido que tp psan para req de 80 KB)
        #[1] https://ieeexplore.ieee.org/document/8029252, Fog Computing: Towards Minimizing Delay in
        #the Internet of Things
    }

    print "************** Experiment Configuration: **************"
    print "strategy:    ",  args.strategy
    print "seed:        ",  args.seed
    print "out_path:    ",  args.out_path
    print "n_requests   ",  args.n_requests
    print "req_rate     ",  args.req_rate
    print "n_datatypes  ",  args.n_datatypes
    print "freshness:   ",  args.freshness   
    print "n_edges:     ",  args.n_edges
    print "link_type:   ",  args.link_type
    print "npsan:       ",  args.npsan
    print "collect_rate:",  args.collect_rate
    print "*******************************************************"

    results, separate_results, network_results = main (parameters)

    print "************** Experiment Configuration: **************"
    print "strategy:    ",  args.strategy
    print "seed:        ",  args.seed
    print "out_path:    ",  args.out_path
    print "n_requests   ",  args.n_requests
    print "req_rate     ",  args.req_rate
    print "n_datatypes  ",  args.n_datatypes
    print "freshness:   ",  args.freshness   
    print "n_edges:     ",  args.n_edges
    print "link_type:   ",  args.link_type
    print "npsan:       ",  args.npsan
    print "collect_rate:",  args.collect_rate
    print "*******************************************************"

    data = {"Stats": results["Stats"], "Parameters": parameters}

    print "************** Run Experiment **************"
    with open(args.out_path, 'wb') as handle:
        pk.dump(data, handle, protocol=pk.HIGHEST_PROTOCOL)

