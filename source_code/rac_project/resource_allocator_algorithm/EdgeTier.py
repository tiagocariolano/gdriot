import util
import collections
from psan import PSAN
from math import sqrt
from yafs.application import Message, UpLayerInterface, Application
from utils import default_entity_parameters, GB, EDGE_SOURCE_DELAY_PROP, WIRED_WAVE_SPEED, ONLY_CLOUD, HIERARCHY
from utils import next_id, PROPAGATION_DELAY, BW, CLOUD_EDGE_DELAY_PROP, CLOUD_SOURCE_DELAY_PROP
from utils import CLOUD_EDGE_LINK_CHOICES, PSAN_CLOUD_LINK_CHOICES, EDGE_EDGE_LINK_CHOICES, EDGE_PSAN_LINK_CHOICES, EDGE_SOURCE_LINK, CLOUD_SOURCE_LINK
from edge_node import EdgeNode


class EdgeTier():
    MODULE_PREFIX = "EdgeService"

    def __init__ (self, env, parameters, configuration,
            # data_type,
            application,
            cloud_node, source_node,
            cloud_entity,
            source_entity,
            index_to_cloud,
            index_to_source,
            nodeCustomParameters={},
        ):
        self.__parameters = parameters
        self.__index_to_cloud = index_to_cloud
        self.__index_to_source = index_to_source
        self.__env = env
        self._configuration = configuration
        # self.__data_type = data_type
        self.__application = application
        self.__cloud_node = cloud_node
        self.__source_node = source_node
        self.__nodeCustomParameters = nodeCustomParameters

        self._edge_nodes = []
        self._entities = []

        if self.__parameters["link_type"] != ONLY_CLOUD:
            self.__generate_nodes ()

        self.__make_edge_connections()

        self._links = self.__make_connections(cloud_entity, source_entity)

    def __generate_node(self, edge_parameters, type, coordinates):
        id = next_id()
        mem = util.random.choice (self.__parameters["EDGE_MEMORY"])
        cpu_rate  = util.random.choice (self.__parameters["EDGE_CPU"])
        energy = util.random.choice (self.__parameters["EDGE_ENERGY"])
        cost = util.random.choice (self.__parameters["EDGE_COST"])

        entity = {"id": id,
            "model": "edge%d" % id,
            "type": "edge%d" % id,
            "CPUR" : cpu_rate,
            "MEMO" : mem,
            "ENER" : energy,
            "COST" : cost}
            
        self._entities.append(entity)

        module_name = "%s%d" % (EdgeTier.MODULE_PREFIX, id)
        interface = UpLayerInterface(self.__application.upLayerAddresser, module_name)

        edge_node = EdgeNode(self.__env, 
                            interface,
                            id, 
                            type,
                            coordinates[1],
                            coordinates[2],
                            cpu_rate,
                            mem)
        edge_node.set_strategy (self.__parameters["strategy"])
        self._edge_nodes.append(edge_node)
        edge_node.run()

        interface.set_up_layer_comm(edge_node.process, UpLayerInterface.EVENT)

        self.__application.set_modules ([{module_name: {"Type": Application.TYPE_EDGE}}])
        self.__application.add_service_edge_module (module_name, edge_node, interface)

        return edge_node

    def __generate_nodes(self):
        n = len (self._configuration._entities_coordinates)
        temp_edge_memory = self.__parameters["EDGE_MEMORY"]

        memory_choice = [0 if i < n/2 else 1 for i in xrange (n)]
        util.random.shuffle (memory_choice)
        
        id_to_edge_node = collections.OrderedDict ()
        for i, _ in enumerate(self._configuration._entities_coordinates):
            type = EdgeNode.EDGE

            self.__parameters["EDGE_MEMORY"] = [temp_edge_memory[memory_choice[i]]]
            edge_node = self.__generate_node(   self.__parameters,
                                                type,
                                                self._configuration._entities_coordinates[i])

        self.__parameters["EDGE_MEMORY"] = temp_edge_memory

    def __make_edge_connections(self):
        if self.__parameters["link_type"] == ONLY_CLOUD:
            self.__source_node.add_neighbor(self.__cloud_node)
            self.__cloud_node.add_neighbor(self.__source_node)
        else:
            for i, edge_node in enumerate(self._edge_nodes):
                
                if self.__parameters["link_type"] == HIERARCHY:
                    edge_node.add_neighbor (self.__cloud_node)
                    self.__cloud_node.add_neighbor (edge_node)
                else:
                    if not self.__index_to_cloud or i in self.__index_to_cloud:
                        edge_node.add_neighbor(self.__cloud_node)
                        self.__cloud_node.add_neighbor(edge_node)

                if not self.__index_to_source or i in self.__index_to_source:
                    self.__source_node.add_neighbor(edge_node)

            for connection in self._configuration._entities_connections:
                edge_node_1 = self._edge_nodes[connection[0]]
                edge_node_2 = self._edge_nodes[connection[1]]

                edge_node_1.add_neighbor(edge_node_2)
                edge_node_2.add_neighbor(edge_node_1)

    def __make_connections(self, cloud_entity, source_entity):
        links = []

        if self.__parameters["link_type"] == ONLY_CLOUD:
            link = {"s": source_entity["id"],
                "d": cloud_entity["id"],
                "BW": CLOUD_SOURCE_LINK,
                "PR": EDGE_SOURCE_DELAY_PROP
            }
            links.append(link)
        else:
            for i, entity in enumerate(self._entities):
                id_1 = entity["id"]
                if not self.__index_to_cloud or i in self.__index_to_cloud:
                    link = {"s": id_1,
                        "d": cloud_entity["id"],
                        "BW": util.random.choice (CLOUD_EDGE_LINK_CHOICES),
                        "PR": CLOUD_EDGE_DELAY_PROP
                    }
                    links.append(link)
                if not self.__index_to_source or i in self.__index_to_source:
                    link = {"s": source_entity["id"],
                        "d": id_1,
                        "BW": EDGE_SOURCE_LINK,
                        "PR": EDGE_SOURCE_DELAY_PROP
                    }
                    links.append(link)

            for connection in self._configuration._entities_connections:
                id_1 = self._entities[connection[0]]["id"]
                id_2 = self._entities[connection[1]]["id"]

                c1 = self._configuration._entities_coordinates[connection[0]]
                c2 = self._configuration._entities_coordinates[connection[1]]

                link = {"s": id_1,
                    "d": id_2,
                    "BW": util.random.choice (EDGE_EDGE_LINK_CHOICES),
                    "PR": self.__distance (c1, c2) / WIRED_WAVE_SPEED
                }
                links.append(link)
        return links

    def __distance (self, c1, c2):
        return sqrt ((c1[0] - c2[0]) ** 2 + (c1[1] - c2[1]) ** 2)

    def update_node (self, id, values):
        try:
            node = filter(lambda x: x.id == id, self._edge_nodes)[0]
            node["CPUR"] = values["cpuRate"]
            node["MEMO"] = values["MEMORY"]
            node["ENER"] = values["ENERGY"]
            node["COST"] = values["COST"]
        except:
            print "no Edge with id %d found." % id

    def show (self):
        for edge_node in self._edge_nodes:
            print edge_node
    '''def update_link (self, oldLinks, newLinks):
        for oldLink in oldLinks:
            for i, link in enumerate (self.__links):
                if link == oldLink:
                    self.__links.remove (i)
                    break
        for newLink in newLinks:
            if newLink not in self.__links:
                self.__links.append (newLink)'''
