import util
import collections
from psan import PSAN
from yafs.application import Message, UpLayerInterface, Application
from utils import *
from edge_node import EdgeNode


class PsanTier():
    MODULE_PREFIX = "PsanService"

    def __init__ (self, env, parameters, configuration, data_type, application,
            cloud_node, source_node,
            edge_tier,
            cloud_entity,
            source_entity,
            index_to_source,
            nodeCustomParameters={},
        ):
        self.__edge_tier = edge_tier
        self.__parameters = parameters
        self.__env = env
        self._configuration = configuration
        self.__data_type = data_type
        self.__application = application
        self.__cloud_node = cloud_node
        self.__source_node = source_node
        self.__nodeCustomParameters = nodeCustomParameters

        self.__psans_entities = {}
        self.__psan_nodes = []
        self.__generate_psans ()

        self._links = self.__make_connections(cloud_entity, source_entity)



    def __generate_psan(self, key, psan_parameters, coordinates):
        id = next_id()
        entity = {"id": id,
                "model": "psan%d" % id,
                "type": "psan%d" % id,
                "CPUR" : psan_parameters["cpuRate"],
                "MEMO" : psan_parameters["MEMORY"],
                "ENER" : psan_parameters["ENERGY"],
                "COST" : psan_parameters["COST"]}
        self.__psans_entities[key].append(entity)

        module_name = "%s%d" % (PsanTier.MODULE_PREFIX, id)
        i_psan_node = UpLayerInterface(self.__application.upLayerAddresser, module_name)
        psan_node = PSAN(self.__env, i_psan_node, id,
                            coordinates[0], coordinates[1],
                            util.random.choice (self.__data_type)) 
        psan_node.set_delay (util.random.choice (self.__parameters["collect_rate"]))
        self.__psan_nodes.append(psan_node)
        psan_node.run()

        i_psan_node.set_up_layer_comm(psan_node.process, UpLayerInterface.EVENT)

        self.__application.set_modules ([{module_name: {"Type": Application.TYPE_EDGE}}])
        self.__application.add_service_edge_module (module_name, psan_node, i_psan_node)

        return psan_node

    def __generate_psans(self):
        psan_parameters = default_entity_parameters
        
        if len (self.__data_type) > self._configuration._psans_coordinates:
            print "Error: PsanTier.__generate_psans: Number of data types should be greater than number of psans."
            exit (1)

        data_type_psans = collections.OrderedDict ()
        for data_type in self.__data_type:
            data_type_psans[data_type] = collections.OrderedDict ()

        for key in self._configuration._psans_coordinates:
            self.__psans_entities[key] = []
            for i, _ in enumerate(self._configuration._psans_coordinates[key]):
                psan = self.__generate_psan(
                    key,
                    psan_parameters,
                    self._configuration._psans_coordinates[key][i]
                )
                # print "psan.data_type", psan.data_type.type, psan.id 
                data_type_psans[psan.data_type][psan] = None

                if key == "cloud":
                    psan.add_neighbor(self.__cloud_node)
                    self.__cloud_node.add_neighbor (psan)
                else:
                    psan.add_neighbor(self.__edge_tier._edge_nodes[key])
                    self.__edge_tier._edge_nodes[key].add_neighbor (psan)
                    # self.__edge_tier._edge_nodes[key].add_data_type (psan.data_type)

        for data_type in data_type_psans:
            while len (data_type_psans[data_type]) == 0:
                for data_type2 in data_type_psans:
                    if len (data_type_psans[data_type2]) > 1 and data_type != data_type2:
                        psan = util.random.choice (list (data_type_psans[data_type2]))
                        new_data_type = util.random.choice ([data_type, data_type2])
                        del data_type_psans[data_type2][psan]
                        data_type_psans[new_data_type][psan] = None
                        psan.data_type = new_data_type
        for edge_node in self.__edge_tier._edge_nodes:
            for psan in edge_node.psan_list:
                edge_node.add_data_type (psan.data_type)

        # for data_type in data_type_psans:
        #     print data_type.type, ": ", data_type_psans[data_type]
        
        # for edge_node in self.__edge_tier._edge_nodes:
        #     print edge_node 
            
        # raw_input ()

    def _get_psans(self):
        psans = []
        for key in self.__psans_entities:
            psans = psans + self.__psans_entities[key]
        return psans

    def _get_psan_nodes (self):
        return self.__psan_nodes

    def __make_connections(self, cloud_entity, source_entity):
        links = []

        if self.__parameters["link_type"] == ONLY_CLOUD:
            for psan in self.__psans_entities["cloud"]:
                link = {"s": psan["id"],
                    "d": cloud_entity["id"],
                    "BW": BW,
                    "PR": CLOUD_PSAN_DELAY_PROP
                }
                links.append(link)
            links.append(link)
        else:
            for key, edge_entity in enumerate(self.__edge_tier._entities):
                for psan in self.__psans_entities[key]:
                    link = {"s": edge_entity["id"],
                        "d": psan["id"],
                        "BW": BW,
                        "PR": EDGE_PSAN_DELAY_PROP
                    }
                    links.append(link)
        return links
