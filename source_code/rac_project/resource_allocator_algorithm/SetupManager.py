import os
import numpy as np
import util
from yafs.population import TestPopulation
from yafs.application import Application, UpLayerInterface
from yafs.topology import Topology
from yafs.stats import Stats
from yafs.core import Sim

from EdgeTier import EdgeTier
from PsanTier import PsanTier
from Configuration import Configuration
from cloud_node import CloudNode
from SourceNode import SourceNode
from data_type import DataType
from utils import STORAGE, MB, GB, _bytes, next_id, ONLY_CLOUD
from utils import default_entity_parameters, CLOUD_SOURCE_DELAY_PROP, CLOUD_SOURCE_LINK
from simplePlacement import TestPlacement
from simpleSelection import MinimunPath
import simpy


class SetupManager():
    CLOUD_MODULE_PREFIX = "CloudService"
    SOURCE_MODULE_PREFIX = "SourceService"

    def __init__ (self, parameters):
        self.__parameters = parameters
        self._yafs_topology = {}
        self.alg_topology = None

        self.__source_module = None
        self.__source_yafs = None
        self.__source_node = None
        self.__cloudModule = None
        self.__cloud_yafs = None
        self._cloud_node = None
        

        self.__cloudEntity = {}
        self.__sourceEntity = {}

        self._env = simpy.Environment()

        self._application = Application(name="AppTeste")

        self.__data_type_list = []

        for i in xrange (self.__parameters["n_datatypes"]):
            dt = DataType ("DT" + str (i), 1, 1, 1)
            dt.set_collecting_rate (util.np.random.choice (self.__parameters["collect_rate"]))
            self.__data_type_list.append (dt)

        self.__generate_yafs_cloud()
        self.__generate_yafs_source()

        index_to_cloud = []
        index_to_source = []

        self._configuration = Configuration(
            self.__parameters,
            index_to_cloud,
            index_to_source)

        self._edge_tier = EdgeTier(self._env,
            self.__parameters,
            self._configuration,
            self._application,
            self._cloud_node,
            self.__source_node,
            self.__cloudEntity,
            self.__sourceEntity,
            index_to_cloud,
            index_to_source)

        self._psan_tier = PsanTier(self._env,
            self.__parameters,
            self._configuration,
            # self.__data_type,
            self.__data_type_list,
            self._application,
            self._cloud_node,
            self.__source_node,
            self._edge_tier,
            self.__cloudEntity,
            self.__sourceEntity,
            index_to_source)

        self._edge_tier.show ()

        self._yafs_topology = self.__configure_yafs_topology ()
        self._placement = TestPlacement("Teste")
        self._pop = TestPopulation("Teste")
        self._selectorPath = MinimunPath()

        self._yafs_simulator = self.__configure_yafs_simulator ()
        self._yafs_simulator.run (self.__parameters["simulated_time"], show_progress_monitor=False)

    def __configure_yafs_topology (self):
        yafsTopoConf = {}
        yafsTopoConf["entity"] = [self.__cloudEntity] +        \
                                  [self.__sourceEntity] +       \
                                  self._edge_tier._entities +   \
                                  self._psan_tier._get_psans()  \

        links = []
        if self.__parameters["link_type"] == ONLY_CLOUD:
            # if ONLY_CLOUD, link between source and cloud

            link = {"s": self.__sourceEntity["id"],
                    "d": self.__cloudEntity["id"],
                    "BW": CLOUD_SOURCE_LINK,
                    "PR": CLOUD_SOURCE_DELAY_PROP
            }
            links.append(link)

        yafsTopoConf["link"] = self._edge_tier._links +    \
                                self._psan_tier._links +    \
                                links

        yafsTopo = Topology()
        yafsTopo.load(yafsTopoConf)
        #yafsTopo.write("outputfiles/network.gexf")

        return yafsTopo

    def __configure_yafs_simulator (self):
        simulator = Sim(self._yafs_topology,
                        env=self._env,
                        default_results_path=self.__parameters["output_path"])

        simulator.deploy_app(   self._application,
                                self._placement,
                                self._pop,
                                self._selectorPath)

        return simulator

    def __generate_yafs_cloud(self):
        id = next_id()

        self.__cloudEntity = {"id": id,
            "model": "cloud",
            "type": "cloud",
            "IPT": self.__parameters["EDGE_CPU"],
            "RAM": self.__parameters["EDGE_MEMORY"],
            "COST": self.__parameters["EDGE_COST"],
            "WATT": self.__parameters["EDGE_WATT"]
        }

        self.__cloudModule = "{0:s}{1:d}".format (self.CLOUD_MODULE_PREFIX, id)
        iCloudNode = UpLayerInterface(self._application.upLayerAddresser, self.__cloudModule)

        self._cloud_node = CloudNode(self._env, iCloudNode)
        # self._cloud_node.set_all_data_type_list([self.__data_type])
        self._cloud_node.set_all_data_type_list(self.__data_type_list)

        self._cloud_node.run()

        iCloudNode.set_up_layer_comm (self._cloud_node.process, UpLayerInterface.EVENT)

        self._application.set_modules ([{self.__cloudModule: {"Type": Application.TYPE_EDGE}}])

        self._application.add_service_edge_module (self.__cloudModule, self._cloud_node, iCloudNode)

    def __generate_yafs_source(self):
        id = next_id()

        self.__sourceEntity = {"id": id, "model": "source",
            "type": "source",
            "IPT": default_entity_parameters["cpuRate"],
            "RAM": default_entity_parameters["MEMORY"],
            "COST": default_entity_parameters["COST"],
            "WATT": default_entity_parameters["WATT"]
        }

        self.__source_module = "%s%d" % (SetupManager.SOURCE_MODULE_PREFIX, id)
        i_source_node = UpLayerInterface(self._application.upLayerAddresser, self.__source_module)

        interval_type = self.__parameters["type_interval"]
        interval_mean = self.__parameters["mean_interval"]
        interval_std = self.__parameters["standard_deviation"]
        n_request = self.__parameters["n_request"]

        self.__source_node = SourceNode(self._env, i_source_node, interval_type, n_request)
                                    
        self.__source_node.set_interval (interval_mean, interval_std)
        self.__source_node.set_cycle_list (self.__parameters["SOURCE_REQ_CYCLE"])
        self.__source_node.set_mem_list (self.__parameters["SOURCE_REQ_RAM"])
        self.__source_node.set_data_type_list (self.__data_type_list)
        self.__source_node.set_freshness (self.__parameters["freshness"])

        self.__source_node.run()

        i_source_node.set_up_layer_comm (self.__source_node.process, UpLayerInterface.EVENT)

        self._application.set_modules ([{self.__source_module: {"Type": Application.TYPE_EDGE}}])

        self._application.add_service_edge_module (self.__source_module, self.__source_node, i_source_node)

    def get_results(self):
        separete_result = {}
        results = {"Requests": [], "Energy": [], "Network": [], "General": [], "PsanTier" : []}
        for edgeNode in self._edge_tier._edge_nodes:
            edgeNodeResults = edgeNode.get_results ()
            results["Requests"] += edgeNodeResults["Requests"]
            results["Energy"] += [edgeNodeResults["Energy"]]
            results["Network"] += edgeNodeResults["Network"]

            separete_result[edgeNode.id] = edgeNodeResults

        results["Requests"] += self._cloud_node.get_results()["Requests"]
        results["Energy"] += [self._cloud_node.get_results()["Energy"]]
        results["Network"] += self._cloud_node.get_results()["Network"]
        results["General"] = [  self.__parameters["n_entities"],
                                self.__parameters["max_x"],
                                self.__parameters["max_y"],
                                self.__parameters["quant_psans"],
                                self.__parameters["type_interval"],
                                self.__parameters["mean_interval"],
                                self.__parameters["freshness"]]

        for psan in self._psan_tier._get_psan_nodes ():
            results["PsanTier"].append ([psan, psan.data_type.type, psan.get_results ()["Messages"]])

        # m = Stats(defaultPath="CSVs/Results1")
        #
        # network_results = {"GeneralNetwork": {
        #                     "avg" : m.average_messages_not_transmitted (),
        #                     "peak" : m.peak_messages_not_transmitted (),
        #                     "notsent" : m.messages_not_transmitted ().values[0],
        #                     "time" : m.time_messages_not_trasmitted ().values,
        #                     "buffer": m.buffer_messages_not_transmitted ().values
        #                     }}
        #
        # makespan = [r[2] for r in results["Requests"]]
        #
        # fast = slow = cloud = 0
        # for r in results["Requests"]:
        #     if r[3] == "FAST_FULFILLED":
        #         fast += 1
        #     elif r[3] == "SLOW_FULFILLED":
        #         slow += 1
        #     elif r[3] == "CLOUD_FULFILLED":
        #         cloud += 1
        #
        # total = fast + slow + cloud
        # results["Stats"] = [float (sum (makespan)) / len (makespan), float (fast) / total, float (slow) / total, float (cloud) / total, total]



        separete_result[self._cloud_node.type] = self._cloud_node.get_results()

        return results, separete_result
