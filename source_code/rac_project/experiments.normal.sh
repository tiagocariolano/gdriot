##!/bin/bash
export LC_NUMERIC="en_US.UTF-8"
size_seed=4
out_folder="./results/"
min_rounds=16
max_rounds=32
declare -a vars_names=("makespan")
declare -a links_types=("ONLY_CLOUD" "VERTICAL" "HORIZONTAL" "HIERARCHY")
declare -a freshnesss=(0.0001 0.001 0.01 0.1 1 10)
declare -a nedges=(10 20 30 40 50 60 70 80 90 100)
declare -a reqrates=(0.0001 0.0001 0.001 0.01 0.1 1 10)
declare -a nrequests=(100 200 400 800)
declare -a ndatatypes=(16)
declare -a kappas=(2 4 8)
declare -a strategies=("none" "stealing")
rounds=$(seq $min_rounds 1 $((max_rounds - 1)))


declare -a params_names=("strategies" "nrequests" "rounds" )
declare -a params_formats=("s" "04d" "03d" )
var_name=${vars_names[0]}
params0=${strategies[@]}
params1=${nrequests[@]}
params2=${rounds[@]}

for param2 in ${params2[@]}; do
for param1  in ${params1[@]}; do
seed=$(od -A n -t u$size_seed -N $size_seed  /dev/urandom)
for param0 in ${params0[@]}; do
format="%s-%"${params_formats[0]}"-%s-%"${params_formats[1]}"-%s-%"${params_formats[2]}
args="${params_names[0]} $param0 ${params_names[1]} $param1 ${params_names[2]} $param2"

out_path=$out_folder$(printf "$format" $args)".pickle"

echo time python -m resource_allocator_algorithm \
              --nrequests       $param1 \
              --opt             $param0 \
              --seed            $seed \
              --outpath         $out_path \
              --ndatatypes      04 \
              --kappa           32

time python -m resource_allocator_algorithm \
              --nrequests       $param1 \
              --opt             $param0 \
              --seed            $seed \
              --outpath         $out_path \
              --ndatatypes      04 \
              --kappa           32


#echo $format
#echo $args
#echo $out_path
#              --nrequests       $param0 
#              --seed            $(od -An -N$size_seed -i /dev/random) \
#              --n_edges         4 \
#              --link_type       $param0 \
#              --freshness       $param1 \
#              --req_rate        1 \
#read -p ""
done

done

done
