import util
from util import *
from math import sqrt, fmod
from decimal import Decimal
from probe import Probe
from virtual_node import VirtualNode


class EdgeNode:
	"""
		This class implements a edge node behavior (operation thread).
	"""
	EDGE = "EDGE"
	CLOUD = "CLOUD"
	PSAN = "PSAN"
	REQUESTER = "REQUESTER"

	TOTAL_CORES = 1

	def __init__(self, env, interface, id, type, x, y, cpu_rate, total_mem):
		"""
			EdgeNode constructor method.
		"""
		self.env = env
		self.interface = interface
		self.id = id
		self.type = type
		self.x  = x
		self.y = y
		self.cpu_rate = cpu_rate
		self.total_mem = total_mem
		self.mem = total_mem
		self.active_cores = 0
		self.strategy = "rac"

		self.utilization = .0
		self.p = 0.
		# self.p_idle = 0.6
		# self.p_max = 1.
		self.p_idle = 297.
		self.p_max = 495.
		self.p_send = 8.4
		self.p_recv = 8.4
		self.tot_time = 0.
		self.efficiency = .0

		self.probe_queue = []
		self.queue = []
		self.dt_list = []
		self.vn_list = []
		self.psan_list = []
		self.neighbor_list = []
		self.parent = None
		self.cloud = None
		self.requester = None
		self.rac_process = None
		self.process = None
		self.alive_process = None
		self.neighbor_to_probe = {}
		self.vn_id = 0
		
		self.last_blocked_time = .0
		self.last_unblocked_time = .0
		self.total_blocked_time = .0
		self.last_total_blocked_time = .0
		self.last_compute_time = .0

		self.total_blocked_time2 = .0
		self.blocked_time2 = .0
		self.last_blocked_time2 = .0
		self.utilization_avg = .0
		self.moving_utilization = .0
		self.blocked_time_list = []

		self.sum_blocked_time = .0
		self.sum_time_slot = .0

		self.results = {"Requests" : [], "Energy" : {}, "Network" : [], "Debug" : [],
										"sumlow": 0, "summiddle": 0, "sumhigh": 0,
										"sumwaittime": 0.0, "nwaittime": 0,
										"utilization": .0,
										"p_idle" : self.p_idle,
										"p_max"  : self.p_max,
										"p_send"  : self.p_send,
										"p-recv"  : self.p_recv,
										"tot_time": 0.,
										"p": 0.0, 
										 "n_probe":0,
										"probe_size": 0, "n_forwarding": 0,
										"forwarding_size": 0}

		self.rac_count = 0
		self.n_probe = 0
		self.probe_size = 0.
		self.n_forwarding = 0
		self.forwarding_size = 0.
		self.local_log = log
		self.debug_function = lambda id: id == 3

		self.log ( 'Init Edge Node')

	def run (self):
		self.rac_process = self.env.process (self.rac ())
		self.process = self.env.process (self.manager ())
		self.alive_process = self.env.process (self.alive ())

	def set_strategy (self, strategy):
		self.strategy = strategy

	def set_parent (self, parent):
		self.parent = parent

	def add_data_type (self, data_type):
		"""
			Add new data type to edge node
		"""
		for dt in self.dt_list:
			if dt == data_type:
				return
		self.dt_list.append (data_type)

	def add_neighbor (self, neighbor):
		"""
			Add new neighbor to edge node
		"""
		#sanity check
		if not hasattr (neighbor, "type"):
			log.error ("EdgeNode.add_neighbor: Class " + str (neighbor) + "cannot to be added in edge node.")

		if neighbor.type  == self.EDGE:
			self.neighbor_list.append (neighbor)
			self.neighbor_to_probe[neighbor] = None
		elif neighbor.type == self.CLOUD:
			self.cloud  = neighbor
		elif neighbor.type == self.PSAN:
			self.psan_list.append (neighbor)
		elif neighbor.type == self.REQUESTER:
			self.requester = neighbor
		else:
			log.error (	"EdgeNode.add_neighbor: Type " + str (neighbor.type) + " from class " + str (neighbor) +
						" is unknown")

	def check_data_type (self, request):
		"""
			Check type of edge node

			Return: boolean indicating if types match
		"""
		return request.data_type in self.dt_list

	def check_memory (self, request):
		"""
			Check if available	resources in edge node are suffient to attend request
			Args:
				request:  a request to be executed

			Return: boolean indicating if edge node is able to attend request
		"""

		return 	self.mem >= request.mem

	def choose_psan (self, request):

		psan = None
		psan_index_list = []

		for i, psan in enumerate (self.psan_list):
			if psan.data_type == request.data_type:
				psan_index_list.append (i)

		if not psan_index_list:
			log.error ("EdgeNode.choose_psan: No psan found")
		
		psan_index = util.random.choice (psan_index_list)
		psan = self.psan_list[psan_index]

		return psan

	def next_vn_id (self):
		self.vn_id += 1
		return self.vn_id

	def compute_blocked_time (self):
		blocked_time = .0
		
		if self.active_cores > 0:
			if self.last_blocked_time > self.last_compute_time:
				blocked_time += self.env.now - self.last_blocked_time
			else:
				blocked_time += self.env.now - self.last_compute_time
		
		blocked_time += max (0, self.total_blocked_time - self.last_total_blocked_time)
		
		self.last_total_blocked_time = self.last_total_blocked_time + blocked_time
		self.last_compute_time = self.env.now

		# self.log ("\n")
		# self.log ("test compute => {:08.5f}, {:08.5f}, {:08.5f}, {:08.5f}, {:08.5f}, t={:08.5f}".format (
		# 	self.last_blocked_time,
		# 	self.last_unblocked_time,
		# 	self.total_blocked_time,
		# 	self.last_total_blocked_time,
		# 	blocked_time,
		# 	self.env.now
		# ))
		# self.log ("\n")

		return blocked_time

	def block_core (self):
		self.active_cores += 1
		self.last_blocked_time = self.env.now
		
		self.last_blocked_time2 = self.env.now

		# self.log ("\n")
		# self.log ("test block   => {:08.5f}, {:08.5f}, {:08.5f}, {:08.5f}, {:8s}, t={:08.5f}".format (
		# 	self.last_blocked_time, 
		# 	self.last_unblocked_time,
		# 	self.total_blocked_time,
		# 	self.last_total_blocked_time, 
		# 	 "", self.env.now ))
		# self.log ("\n")

	def unblock_core (self):
		self.active_cores -= 1
		self.last_unblocked_time = self.env.now
		self.total_blocked_time += self.env.now - self.last_blocked_time

		self.total_blocked_time2 += self.env.now - self.last_blocked_time2
		
		# self.log ("\n")
		# self.log ("test unblock => {:08.5f}, {:08.5f}, {:08.5f}, {:08.5f}, {:8s}, t={:08.5f}".format (
		# 	self.last_blocked_time, 
		# 	self.last_unblocked_time,
		# 	self.total_blocked_time,
		# 	self.last_total_blocked_time, 
		# 	"", self.env.now ))
		# self.log ("\n")

	def compute_utilization (self, blocked_time):
		utilization = float (blocked_time) / self.time_slot

		assert utilization >= 0 and utilization <= 1 or utilization - 1 < 0.0000000001, \
			"utilization should be between 0 and 1. Value=" + repr (utilization)
		
		return utilization
	
	def compute_utilization2 (self, blocked_time):
		self.blocked_time_list.append (blocked_time)
		self.sum_blocked_time += blocked_time
		self.sum_time_slot += self.time_slot

		if len (self.blocked_time_list) > self.periods:
			first = self.blocked_time_list.pop (0)
			self.sum_blocked_time -= first
			self.sum_time_slot -= self.time_slot

		moving_utilization = float (self.sum_blocked_time) / self.sum_time_slot

		assert moving_utilization >= 0 and \
				moving_utilization <= 1 or \
				moving_utilization - 1 < 0.0000000001, \
				"moving utilization should be between 0 and 1. Value=" + repr (moving_utilization)

		return moving_utilization

	def compute_utilization_avg (self):
		utilization_avg = float (self.total_blocked_time) / self.env.now
		assert self.utilization_avg >= 0 and self.utilization_avg <= 1 or self.utilization_avg - 1 < 0.0000000001, \
				"utilization_avg should be between 0 and 1. Value=" + repr (self.utilization_avg)

		return utilization_avg

	def get_percentages_from_probes (self):
		percentages = {}
		utilization_sum = 0

		for i, probe in enumerate (self.probe_queue):
			utilization_sum += probe.utilization
		
		for i, probe in enumerate (self.probe_queue):
			percentages[probe.edge_id] = 1 - float (probe.utilization) / utilization_sum if utilization_sum > 0 else 0
		
		return percentages

	def alloc_mem (self, mem):
		self.mem -= mem

	def desalloc_mem (self, mem):
		self.mem += mem

		assert (self.mem >= 0 and self.mem <= self.total_mem)
	
	def desalloc_vn (self, vn):
		self.mem -= vn.mem
		self.vn_list.remove (vn)
		
		assert (self.mem >= 0 and self.mem <= self.total_mem)

	def allocate (self, vn, request):
		"""
			Allocating vn  to request and update edge node available resources

			Args:
				vn: a vn to be allocated

				request: a request
		"""

		if vn.check_new_state ():
			
			vn.provisioning (request)
			self.alloc_mem (vn.mem)

			self.vn_list.append (vn)
		else:
			self.desalloc_mem (vn.mem)
			vn.provisioning (request)
			self.alloc_mem (vn.mem)

			for vn_instance in list (self.vn_list):
				if vn == vn_instance:
					self.vn_list.remove (vn)
					self.vn_list.append (vn)
					break
		
		#self.alloc_cpu_time (vn.processing_delay)
		self.process.interrupt (Event (Event.START_VN, vn))

	def cloud_forwarding (self):
		for request in list (self.queue):
			has_data_type = False
			for neighbor in self.neighbor_list:
				if neighbor.check_data_type (request):
					has_data_type = True
					break
			if not self.check_data_type (request):
				self.queue.remove (request)
				self.send_cloud_message (self.cloud, request)

	def vertical_offloading (self):
		for request in list (self.queue):
			if not self.check_data_type (request):
				self.queue.remove (request)
				self.send_cloud_message (self.cloud, request)

				log2.log ("C Request " + "{:02d}".format (request.id) + ":Edge ,Edge" + str (self.id) + "->Cloud,", "{:014.10f}".format (self.env.now), ",d=", self.env.now - request.t_start)

	def stealing_collaboration (self):
		# if self.id in [8, 2]: print self.id, " STEALING"
		for request in list (self.queue):
			# if self.id in [8,2]: print request
			for probe in self.probe_queue:
				# if self.id in [8,2]: print self.id, " ", probe, [edge.id for edge in request.path]
				if probe.edge.check_data_type (request) and probe.edge not in request.path:
					# if self.id in [8, 2]: print self.id, " send request"
					self.queue.remove (request)
					self.send_message (probe.edge, request)
					self.stealing_count += 1
					log2.log ("PRRequest " + "{:02d}".format (request.id) + ":Edge ,Edge" + str (self.id) + "->Edge" + str (probe.edge.id) + ",", "{:014.10f}".format (self.env.now))
					break
		
		for probe in list (self.probe_queue):
			self.probe_queue.remove (probe)
	
	def probing (self):
		if len (self.queue) <= 0:
			probe = Probe (next_probe_id (), self.id, len (self.queue), self)
			for neighbor in self.neighbor_list:
				self.send_probe (neighbor, probe)
				self.n_probe += 1
				self.probe_size += probe.bytes

	def edge_collaboration (self):
		
		for request in list (self.queue):
			neighbor_list_2 = list (self.neighbor_list)
			util.random.shuffle (neighbor_list_2)
			for neighbor in neighbor_list_2:
				if neighbor not in request.path:
					self.queue.remove (request)
					request.path.append (self)
					self.send_message (neighbor, request)
					self.n_forwarding += 1
					self.forwarding_size += request.bytes
					log2.log ("PRRequest " + "{:02d}".format (request.id) + ":Edge ,Edge" + str (self.id) + "->Edge" + str (neighbor.id) + ",", "{:014.10f}".format (self.env.now))
					break
	
	def rac_collaboration (self):
		for request in list (self.queue):
			neighbor_list_2 = list (self.neighbor_list)
			util.random.shuffle (neighbor_list_2)
			for neighbor in neighbor_list_2:
				probe = self.neighbor_to_probe[neighbor]
				if probe is not None:
					if 	(not self.check_data_type (request) and neighbor.check_data_type (request) and neighbor not in request.path) or \
						(self.check_data_type (request) and neighbor.check_data_type (request) and neighbor not in request.path and len(self.queue) > probe.queue_size):
						
						self.queue.remove (request)
						request.path.append (self)
						probe.queue_size += 1
						self.send_message (neighbor, request)
						self.n_forwarding += 1
						self.forwarding_size += request.bytes
						log2.log ("PRRequest " + "{:02d}".format (request.id) + ":Edge ,Edge" + str (self.id) + "->Edge" + str (neighbor.id) + ",", "{:014.10f}".format (self.env.now))
						break

	def alive (self):
		while util.FINISHED_REQ < util.TOT_REQ:
			yield self.env.timeout (0.1)
			self.utilization = self.compute_utilization_avg ()
			self.p += self.p_idle + (self.p_max - self.p_idle) * self.utilization

	def rac (self):

		while util.FINISHED_REQ < util.TOT_REQ:
			try:	
				yield self.env.timeout (SIMULATION_TIME)
			except simpy.Interrupt as interrupt:
				event = interrupt.cause
				self.rac_count += 1

				self.log ("\n" + "=" * 20)
				for vn in self.vn_list:
					self.log (vn)
				for probe in self.probe_queue:
					self.log (probe)
				self.log ("-" * 20)
				self.log (self)
				self.log ("\n" + "=" * 20)

				for request in list(self.queue):
					self.log (request)
					if self.active_cores > 0:
						break

					if self.check_data_type (request):

						min_vn = None

						for vn in self.vn_list:
							if vn.check_data_type (request) and vn.check_idle_state () and vn.check_freshness (request) and vn.check_memory (request):
								min_vn = vn
								break
						
						if min_vn is None and self.check_memory (request):
							min_vn = VirtualNode (self, self.cpu_rate, 0, request.data_type)

						if min_vn is None and not self.check_memory (request):
							for vn in reversed (self.vn_list):
								if vn.check_data_type (request) and vn.check_idle_state ():
									self.desalloc_vn (vn)

									if self.check_memory (request):
										min_vn = VirtualNode (self, self.cpu_rate, 0, request.data_type)
										break

						if min_vn is not None:
							self.queue.remove (request)
							self.allocate (min_vn, request)

							self.log ("ALLOCATE => ", self, ", Q: ", len (self.queue))
							self.log ("         => ", request)
							self.log ("         => ", min_vn)
							
							break

					self.log ("---------------------------------------------------------")

				if self.strategy == "vertical":
					self.vertical_offloading ()
				elif self.strategy == "stealing":
					if event is None:
						self.probing ()
					self.stealing_collaboration ()
					self.cloud_forwarding ()
				elif self.strategy == "edge":
					self.edge_collaboration ()
					self.cloud_forwarding ()
				elif self.strategy == "rac":
					if event is None:
						self.probing ()
					self.rac_collaboration ()
					self.cloud_forwarding ()

	def manager (self):
		while True:
			try:
				yield self.env.timeout (SIMULATION_TIME)

			except simpy.Interrupt as interrupt:
			 	event = interrupt.cause

				if event.type == Event.ARRIVED_MSG:
					request = event.obj

 					self.queue.append (request)
					self.rac_process.interrupt ()

					log2.log ("->Request " + "{:02d}".format (request.id) + ":Edge ,Edge" + str (self.id) + "<-     ,", "{:014.10f}".format (self.env.now), ",d=", self.env.now - request.t_start, ",", request.data_type.type)
					self.log ("REQ_ARR  ", self.id, " => ", request, self.active_cores, ", t=", self.env.now)
				elif event.type == Event.ARRIVED_PROBE:
					probe = event.obj

					self.probe_queue.append (probe)
					self.neighbor_to_probe[probe.edge] = probe
					self.rac_process.interrupt (event)

					# if self.id in [8, 2]: print self.id, " ARRIVED PROBE ", probe
					self.log ("PROBE_AR ", self.id, " => ", probe, self.active_cores, ", t=", self.env.now)
				elif event.type == Event.ARRIVED_DATA:
					request = event.obj
					request.rx_tstamp = self.env.now
					request.recv_delay = request.rx_tstamp - request.tx_tstamp
					# print "EDGE: ", self.id, ", ", request.send_delay, request.recv_delay, 
					# raw_input ()
					
					self.log ("DATA_ARR ", self.id, " => ", request, ", ", self.active_cores, ", t=", self.env.now)
					log2.log ("D Request " + "{:02d}".format (request.id) + ":Edge ,Edge" + str (self.id) + "<-     ,", len (self.queue), self.env.now, ",d=", self.env.now-request.t_start)
					
					for vn in self.vn_list:
						if vn.request == request:
							vn.set_data (request.data)
							break

					for vn in self.vn_list:
						if vn.check_ready_state ():
							if self.active_cores < self.TOTAL_CORES:
								self.block_core ()
								vn.run ()
					
					self.log ("         ", self.id, " => ", self.active_cores, ", t=", self.env.now)
				elif event.type == Event.ARRIVED_MSG_CLOUD:
					request = event.obj
					if not self.check_data_type (request) or request.dst is not self:
						log.error ("EdgeNode.event_handling: request from cloud does not match the data type")

					psan = self.choose_psan (request)
					self.send_cloud_message (psan, request)
					log2.log ("D Request " + "{:02d}".format (request.id) + ":Edge ,Edge" + str (self.id) + "->Ps " + "{:02d}".format (psan.id) + ",", "{:014.10f}".format (self.env.now), ",d=", self.env.now-request.t_start)
				elif event.type == Event.ARRIVED_DATA_CLOUD:
					request = event.obj
					self.send_cloud_data (self.cloud, request)

					log2.log ( "D Request " + "{:02d}".format (request.id) + ":Edge ,Edge" + str (self.id) + "->Cloud,", "{:014.10f}".format (self.env.now), ",d=", self.env.now-request.t_start)
				elif event.type == Event.FINISHED_VN:
					finished_vn = event.obj

					finished_vn.request.critical_path.append ("Edge" + str (self.id))
					tStart = finished_vn.request.t_start
					tFinal = self.env.now
					makespan = tFinal - tStart
					log2.log ( "F Request " + "{:02d}".format (finished_vn.request.id) + ":Edge ,Edge" + str (self.id) + "<-     ,", "{:014.10f}".format (self.env.now), ",", makespan)
					
					self.results["Requests"].append ([	tStart, tFinal,
														makespan, finished_vn.reqState,
														self.env.now, finished_vn.request.hop,
														finished_vn.request.hopedge, finished_vn.request.hopcloud,
														finished_vn.request.price, finished_vn.request.mem,
														finished_vn.request.cycle, self.cpu_rate,
														finished_vn.start, self.p, 
														finished_vn.request.id, 
														finished_vn.request.data_type,
														finished_vn.request.critical_path])

					self.unblock_core ()
					self.update_resources (finished_vn)

					util.FINISHED_REQ += 1
					self.tot_time = self.env.now

					self.log ("VN_FINIS ", self.id, " => ", finished_vn, ", ", self.active_cores, ", ", makespan, ", ", util.FINISHED_REQ, ", t=", self.env.now)

					for vn in self.vn_list:
						if vn.check_ready_state ():
							if self.active_cores < self.TOTAL_CORES:
								self.block_core ()
								vn.run ()				
						self.log ("         ", self.id, " => ", vn, ", ", self.active_cores, ", t=", self.env.now)	
					# if self.id in [8, 2]: print self.id, " FINISHED VN ", request
					if self.active_cores <= 0:
						self.rac_process.interrupt ()
				elif event.type == Event.START_VN:
					vn = event.obj

					self.log ("VN_START ", self.id, " => ", vn, ", t=", self.env.now)
					if vn.check_ready_state ():
						if self.active_cores < self.TOTAL_CORES:
							self.block_core ()
							vn.run ()
					elif vn.check_new_state () or vn.check_requirementless_state ():
						psan = self.choose_psan (vn.request)
						self.send_message (psan, vn.request)
						log2.log ("D Request " + "{:02d}".format (vn.request.id) + ":Edge ,Edge" + str (self.id) + "->Ps " + "{:02d}".format (psan.id) + ",", "{:014.10f}".format (self.env.now))
					elif vn.check_busy_state ():
						self.log ("EdgeNode.event_handling: state 'BUSY' for 'START_VN' event")
					else:
						log.error ("EdgeNode.event_handling: wrong vn state")
					self.log ("         ", self.id, " => ", vn, ", t=", self.env.now)
				else:
					log.error ("EdgeNode.manager: Event type did not expect. ")

	def update_resources (self, vn):
		vn.state = vn.IDLE

	def get_results (self):
		self.results["p"] = self.p
		self.results["utilization"] = self.utilization
		self.results["tot_time"] = self.tot_time
		self.results["n_probe"] = self.n_probe
		self.results["probe_size"] = self.probe_size
		self.results["n_forwarding"] = self.n_forwarding
		self.results["forwarding_size"] = self.forwarding_size
		for psan in self.psan_list:
			distance = self.distance ([self.x, self.y], [psan.x, psan.y])
			self.results["Energy"][psan] = {"data_r" : psan.results["Energy"]["data_r"],
											"data_s" : psan.results["Energy"]["data_s"],
											"distance" : distance,
											"delay"	: psan.results["Energy"]["delay"]}
		return self.results

	def distance (self, c1, c2):
		return sqrt ((c1[0] - c2[0]) ** 2 + (c1[1] - c2[1]) ** 2)

	def foward_request (self, neighbor, request):
		request.hop += 1
		# request.t = self.env.now
		if neighbor.type == "CLOUD":
			request.hopcloud += 1
		elif neighbor.type == "SLAVE" or neighbor.type == "MASTER":
			request.hopedge += 1
		else:
			print neighbor.type
		self.send_message (neighbor, request)

	def send_cloud_data (self, neighbor, msg):
		msg.critical_path.append ("Edge" + str (self.id))
		msg.edge_origin = self
		ev = Event (Event.ARRIVED_DATA_CLOUD, msg)
		self.interface.send_message (ev, neighbor)

	def send_cloud_message (self, neighbor, msg):
		msg.critical_path.append ("Edge" + str (self.id))
		msg.edge_origin = self
		ev = Event (Event.ARRIVED_MSG_CLOUD, msg)
		self.interface.send_message (ev, neighbor)

	def send_message (self, neighbor, msg):
		"""
			Send a messsage to a neighbor edge node. Create event ARRIVED_MSG
			and generate interruption in neighbor edge node.

			Args:
				neighbor: a neighbor edge node

				msg: message to be sent
		"""
		# print "Request", msg.id, "Edge", self.id, "->", self.env.now
		msg.critical_path.append ("Edge" + str (self.id))
		msg.tx_tstamp = self.env.now
		ev = Event (Event.ARRIVED_MSG, msg)
		msg.edge_origin = self
		self.interface.send_message (ev, neighbor)

	def send_probe (self, neighbor, msg):
		ev = Event (Event.ARRIVED_PROBE, msg)
		msg.edge_origin = self
		self.interface.send_message (ev, neighbor)

	def log (self, *str_list):
		if self.debug_function (self.id):
			self.local_log.log (*str_list)

	def pause (self):
		if self.debug_function (self.id):
			raw_input ()

	def __str__ (self):
		"""
			Print resources info about edge node
		"""
		GB = GHz = 10 ** 9
		MB = MHz = 10 ** 6
		out = "{:6s} {:02d}: {:010.4f}, {:010.4f}, {:5s}, {:02d}".format (
				"EN", self.id,
				float (self.cpu_rate) / MHz, 
				float (self.mem) / MB,
				self.type, 
				self.parent.id if self.parent is not None else 0)
		for dt in self.dt_list:
			out = out + "{:5s}-".format (dt.type)
		return out


